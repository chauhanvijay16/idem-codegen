CloudTrail_CloudWatchLogs_Role:
  aws.iam.role.present:
  - resource_id: CloudTrail_CloudWatchLogs_Role
  - name: CloudTrail_CloudWatchLogs_Role
  - arn: arn:aws:iam::123456789012:role/CloudTrail_CloudWatchLogs_Role
  - id: AROAITPA5S7NF4PREISDM
  - path: /
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "cloudtrail.amazonaws.com"}, "Sid": ""}],
      "Version": "2012-10-17"}'


CloudTrail_CloudWatchLogs_Role-oneClick_CloudTrail_CloudWatchLogs_Role_1522065148468:
  aws.iam.role_policy.present:
  - resource_id: CloudTrail_CloudWatchLogs_Role-oneClick_CloudTrail_CloudWatchLogs_Role_1522065148468
  - role_name: CloudTrail_CloudWatchLogs_Role
  - name: oneClick_CloudTrail_CloudWatchLogs_Role_1522065148468
  - policy_document: '{"Statement": [{"Action": ["logs:CreateLogStream"], "Effect":
      "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:CloudTrail/DefaultLogGroup:log-stream:123456789012_CloudTrail_us-east-1*"],
      "Sid": "AWSCloudTrailCreateLogStream20141101"}, {"Action": ["logs:PutLogEvents"],
      "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:CloudTrail/DefaultLogGroup:log-stream:123456789012_CloudTrail_us-east-1*"],
      "Sid": "AWSCloudTrailPutLogEvents20141101"}], "Version": "2012-10-17"}'
