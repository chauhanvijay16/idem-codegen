Z03616682JNY9P3D3T2IR:vpc-0738f2a523f4735bd:eu-west-3:
  aws.route53.hosted_zone_association.present:
  - resource_id: Z03616682JNY9P3D3T2IR:vpc-0738f2a523f4735bd:eu-west-3
  - name: Z03616682JNY9P3D3T2IR:vpc-0738f2a523f4735bd:eu-west-3
  - zone_id: Z03616682JNY9P3D3T2IR
  - vpc_id: vpc-0738f2a523f4735bd
  - vpc_region: eu-west-3
  - comment: null


Z0721725PI001CQXUGYY:vpc-0738f2a523f4735bd:eu-west-3:
  aws.route53.hosted_zone_association.present:
  - resource_id: Z0721725PI001CQXUGYY:vpc-0738f2a523f4735bd:eu-west-3
  - name: Z0721725PI001CQXUGYY:vpc-0738f2a523f4735bd:eu-west-3
  - zone_id: Z0721725PI001CQXUGYY
  - vpc_id: vpc-0738f2a523f4735bd
  - vpc_region: eu-west-3
  - comment: null


arn:aws:iam::123456789012:policy/service-role/ProtonRolePolicy-test:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["cloudformation:CancelUpdateStack",
      "cloudformation:ContinueUpdateRollback", "cloudformation:CreateChangeSet", "cloudformation:CreateStack",
      "cloudformation:DeleteChangeSet", "cloudformation:DeleteStack", "cloudformation:DescribeChangeSet",
      "cloudformation:DescribeStackDriftDetectionStatus", "cloudformation:DescribeStackEvents",
      "cloudformation:DescribeStackResourceDrifts", "cloudformation:DescribeStacks",
      "cloudformation:DetectStackResourceDrift", "cloudformation:ExecuteChangeSet",
      "cloudformation:ListChangeSets", "cloudformation:ListStackResources", "cloudformation:UpdateStack"],
      "Effect": "Allow", "Resource": "arn:aws:cloudformation:*:123456789012:stack/AWSProton-*"},
      {"Condition": {"ForAnyValue:StringEquals": {"aws:CalledVia": ["cloudformation.amazonaws.com"]}},
      "Effect": "Allow", "NotAction": ["organizations:*", "account:*"], "Resource":
      "*"}, {"Action": ["organizations:DescribeOrganization", "account:ListRegions"],
      "Condition": {"ForAnyValue:StringEquals": {"aws:CalledVia": ["cloudformation.amazonaws.com"]}},
      "Effect": "Allow", "Resource": "*"}], "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: ProtonRolePolicy-test
  - resource_id: arn:aws:iam::123456789012:policy/service-role/ProtonRolePolicy-test
  - id: ANPAX2FJ77DCQ6NJ3BXDI
  - path: /service-role/


arn:aws:iam::123456789012:policy/xyz-idem-test-admin:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["xyz:*"], "Effect": "Allow", "Resource":
      "*"}, {"Action": ["iam:PassRole"], "Effect": "Allow", "Resource": ["arn:aws:iam::123456789012:role/idem-test-temp-xyz-cluster"]},
      {"Action": ["kms:Create*", "kms:Describe*", "kms:Enable*", "kms:List*", "kms:Put*",
      "kms:Update*", "kms:Revoke*", "kms:Disable*", "kms:Get*", "kms:Delete*", "kms:TagResource",
      "kms:UntagResource", "kms:ScheduleKeyDeletion", "kms:CancelKeyDeletion"], "Effect":
      "Allow", "Resource": "arn:aws:kms:eu-west-3:123456789012:key/8ac5f341-fd1c-4e9d-9596-8f844dba5cc8"}],
      "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: xyz-idem-test-admin
  - resource_id: arn:aws:iam::123456789012:policy/xyz-idem-test-admin
  - id: ANPAX2FJ77DCY4DVSXP5E
  - path: /


arn:aws:iam::123456789012:policy/xyz-idem-test-jenkins:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": "xyz:DescribeCluster", "Effect": "Allow",
      "Resource": "arn:aws:xyz:eu-west-3:123456789012:cluster/idem-test"}], "Version":
      "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: xyz-idem-test-jenkins
  - resource_id: arn:aws:iam::123456789012:policy/xyz-idem-test-jenkins
  - id: ANPAX2FJ77DCWXVXTSXMQ
  - path: /


db-subnet-group-idem-test:
  aws.rds.db_subnet_group.present:
  - db_subnet_group_arn: arn:aws:rds:eu-west-3:123456789012:subgrp:db-subnet-group-idem-test
    db_subnet_group_description: For Aurora rds
    name: db-subnet-group-idem-test
    resource_id: db-subnet-group-idem-test
    subnets:
    - subnet-039e53122e038d38c
    - subnet-05dfaa0d01a337199
    - subnet-050732fa4616470d9
    tags:
    - Key: Automation
      Value: 'true'
    - Key: Owner
      Value: org1
    - Key: COGS
      Value: OPEX
    - Key: Environment
      Value: test-dev
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Name
      Value: idem-test-db-subnet-group


elasticache-subnet-group-idem-test:
  aws.elasticache.cache_subnet_group.present:
  - name: elasticache-subnet-group-idem-test
  - resource_id: elasticache-subnet-group-idem-test
  - cache_subnet_group_description: For elastcache redis cluster
  - arn: arn:aws:elasticache:eu-west-3:123456789012:subnetgroup:elasticache-subnet-group-idem-test
  - subnet_ids:
    - subnet-039e53122e038d38c
    - subnet-05dfaa0d01a337199
    - subnet-050732fa4616470d9
  - tags: []


extension-jenkins-idem-test:
  aws.iam.user.present:
  - name: extension-jenkins-idem-test
  - resource_id: extension-jenkins-idem-test
  - arn: arn:aws:iam::123456789012:user/xyz/idem-test/extension-jenkins-idem-test
  - path: /xyz/idem-test/
  - tags:
    - Key: COGS
      Value: OPEX
    - Key: Owner
      Value: org1
    - Key: Automation
      Value: 'true'
    - Key: Environment
      Value: test-dev
    - Key: KubernetesCluster
      Value: idem-test
  - user_name: extension-jenkins-idem-test


extension-jenkins-idem-test-extension-jenkins-idem-test:
  aws.iam.user_policy.present:
  - resource_id: extension-jenkins-idem-test-extension-jenkins-idem-test
  - user_name: extension-jenkins-idem-test
  - name: extension-jenkins-idem-test
  - policy_document: '{"Statement": [{"Action": ["xyz:DescribeCluster"], "Effect":
      "Allow", "Resource": "arn:aws:xyz:eu-west-3:123456789012:cluster/idem-test"},
      {"Action": ["sts:AssumeRole"], "Effect": "Allow", "Resource": ["arn:aws:iam::123456789012:role/xyz-idem-test-jenkins"],
      "Sid": ""}, {"Action": "s3:*", "Effect": "Allow", "Resource": ["arn:aws:s3:::ssm-ansible-test-dev",
      "arn:aws:s3:::ssm-ansible-test-dev/*"], "Sid": ""}, {"Action": ["ssm:StartSession"],
      "Condition": {"StringLike": {"ssm:resourceTag/KubernetesCluster": ["idem-test"]}},
      "Effect": "Allow", "Resource": "*"}, {"Action": ["ssm:TerminateSession"], "Effect":
      "Allow", "Resource": "*"}], "Version": "2012-10-17"}'


extension-jenkins-idem-test-extension-jenkins-rolling-upgrade-idem-test:
  aws.iam.user_policy.present:
  - resource_id: extension-jenkins-idem-test-extension-jenkins-rolling-upgrade-idem-test
  - user_name: extension-jenkins-idem-test
  - name: extension-jenkins-rolling-upgrade-idem-test
  - policy_document: '{"Statement": [{"Action": "ec2:Describe*", "Effect": "Allow",
      "Resource": "*"}, {"Action": ["sts:AssumeRole"], "Effect": "Allow", "Resource":
      ["arn:aws:iam::123456789012:role/xyz-idem-test-jenkins"], "Sid": ""}, {"Action":
      ["autoscaling:DeleteTags", "autoscaling:ResumeProcesses", "autoscaling:CreateOrUpdateTags",
      "autoscaling:UpdateAutoScalingGroup", "autoscaling:SuspendProcesses", "autoscaling:TerminateInstanceInAutoScalingGroup"],
      "Condition": {"StringEquals": {"autoscaling:ResourceTag/KubernetesCluster":
      "idem-test"}}, "Effect": "Allow", "Resource": "*", "Sid": ""}, {"Action": ["xyz:UpdateClusterVersion",
      "ec2:DescribeInstances", "ec2:RebootInstances", "autoscaling:DescribeAutoScalingGroups",
      "xyz:DescribeUpdate", "xyz:DescribeCluster", "xyz:ListClusters", "xyz:CreateCluster"],
      "Effect": "Allow", "Resource": "*", "Sid": ""}, {"Action": "ec2:DescribeInstances",
      "Effect": "Allow", "Resource": "*", "Sid": ""}], "Version": "2012-10-17"}'


fl-02e639031d2f37592:
  aws.ec2.flow_log.present:
  - resource_ids:
    - vpc-02e366ce9fa15dd56
  - resource_type: VPC
  - resource_id: fl-02e639031d2f37592
  - iam_role: arn:aws:iam::123456789012:role/xyz-idem-test_redlock_flow_role
  - log_group_name: xyz-idem-test_redlock_flow_log_group
  - traffic_type: ALL
  - log_destination_type: cloud-watch-logs
  - log_destination: arn:aws:logs:eu-west-3:123456789012:log-group:xyz-idem-test_redlock_flow_log_group:*
  - log_format: ${version} ${account-id} ${interface-id} ${srcaddr} ${dstaddr} ${srcport}
      ${dstport} ${protocol} ${packets} ${bytes} ${start} ${end} ${action} ${log-status}
  - max_aggregation_interval: 600
  - tags: []


fl-0604c326e4d398406:
  aws.ec2.flow_log.present:
  - resource_ids:
    - vpc-0c06512aed6737787
  - resource_type: VPC
  - resource_id: fl-0604c326e4d398406
  - iam_role: arn:aws:iam::123456789012:role/xyz-idem-test_redlock_flow_role
  - log_group_name: xyz-idem-test_redlock_flow_log_group
  - traffic_type: ALL
  - log_destination_type: cloud-watch-logs
  - log_destination: arn:aws:logs:eu-west-3:123456789012:log-group:xyz-idem-test_redlock_flow_log_group:*
  - log_format: ${version} ${account-id} ${interface-id} ${srcaddr} ${dstaddr} ${srcport}
      ${dstport} ${protocol} ${packets} ${bytes} ${start} ${end} ${action} ${log-status}
  - max_aggregation_interval: 600
  - tags: []


fl-0761ee74ec872ab13:
  aws.ec2.flow_log.present:
  - resource_ids:
    - vpc-079e00e8877b676f8
  - resource_type: VPC
  - resource_id: fl-0761ee74ec872ab13
  - iam_role: arn:aws:iam::123456789012:role/xyz-idem-test_redlock_flow_role
  - log_group_name: xyz-idem-test_redlock_flow_log_group
  - traffic_type: ALL
  - log_destination_type: cloud-watch-logs
  - log_destination: arn:aws:logs:eu-west-3:123456789012:log-group:xyz-idem-test_redlock_flow_log_group:*
  - log_format: ${version} ${account-id} ${interface-id} ${srcaddr} ${dstaddr} ${srcport}
      ${dstport} ${protocol} ${packets} ${bytes} ${start} ${end} ${action} ${log-status}
  - max_aggregation_interval: 600
  - tags: []


fl-0d8347dbe88a0027a:
  aws.ec2.flow_log.present:
  - resource_ids:
    - vpc-0738f2a523f4735bd
  - resource_type: VPC
  - resource_id: fl-0d8347dbe88a0027a
  - iam_role: arn:aws:iam::123456789012:role/xyz-idem-test_redlock_flow_role
  - log_group_name: xyz-idem-test_redlock_flow_log_group
  - traffic_type: ALL
  - log_destination_type: cloud-watch-logs
  - log_destination: arn:aws:logs:eu-west-3:123456789012:log-group:xyz-idem-test_redlock_flow_log_group
  - log_format: ${version} ${account-id} ${interface-id} ${srcaddr} ${dstaddr} ${srcport}
      ${dstport} ${protocol} ${packets} ${bytes} ${start} ${end} ${action} ${log-status}
  - max_aggregation_interval: 600
  - tags: []


idem-test:
  aws.eks.cluster.present:
  - name: idem-test
  - resource_id: idem-test
  - role_arn: arn:aws:iam::123456789012:role/idem-test-temp-xyz-cluster
  - arn: arn:aws:xyz:eu-west-3:123456789012:cluster/idem-test
  - status: ACTIVE
  - version: '1.20'
  - resources_vpc_config:
      clusterSecurityGroupId: sg-0f9910c81ca733164
      endpointPrivateAccess: false
      endpointPublicAccess: true
      publicAccessCidrs:
      - 0.0.0.0/0
      securityGroupIds:
      - sg-070a797a4b433814b
      subnetIds:
      - subnet-05dfaa0d01a337199
      - subnet-0094b72dfb7ce6131
      - subnet-050732fa4616470d9
      - subnet-0d68d61b1ab708d42
      - subnet-039e53122e038d38c
      - subnet-09cecc8c853637d3b
      vpcId: vpc-0738f2a523f4735bd
  - kubernetes_network_config:
      ipFamily: ipv4
      serviceIpv4Cidr: 172.20.0.0/16
  - logging:
      clusterLogging:
      - enabled: true
        types:
        - api
        - audit
        - authenticator
        - controllerManager
        - scheduler
  - tags:
      Automation: 'true'
      COGS: OPEX
      Environment: test-dev
      KubernetesCluster: idem-test
      Owner: org1


idem-test-temp-xyz-cluster:
  aws.iam.role.present:
  - resource_id: idem-test-temp-xyz-cluster
  - name: idem-test-temp-xyz-cluster
  - arn: arn:aws:iam::123456789012:role/idem-test-temp-xyz-cluster
  - id: AROAX2FJ77DC2JM67OZSY
  - test_output_variable: test
  - path: /
  - max_session_duration: 3600
  - tags:
    - Key: Owner
      Value: org1
    - Key: COGS
      Value: OPEX
    - Key: Environment
      Value: test-dev
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Automation
      Value: 'true'
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "xyz.amazonaws.com"}}], "Version": "2012-10-17"}'


idem-test-temp-xyz-cluster-arn:aws:iam::aws:policy/AmazonxyzClusterPolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: idem-test-temp-xyz-cluster
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzClusterPolicy


idem-test-temp-xyz-cluster-arn:aws:iam::aws:policy/AmazonxyzServicePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: idem-test-temp-xyz-cluster
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzServicePolicy


igw-0eee9bba485b312a8:
  aws.ec2.internet_gateway.present:
  - attachments:
    - State: available
      VpcId: vpc-0738f2a523f4735bd
    name: igw-0eee9bba485b312a8
    resource_id: igw-0eee9bba485b312a8
    tags:
    - Key: Name
      Value: idem-test-temp-xyz
    - Key: COGS
      Value: OPEX
    - Key: Automation
      Value: 'true'
    - Key: Owner
      Value: org1
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Environment
      Value: test-dev
    vpc_id:
    - vpc-0738f2a523f4735bd


nat-076cd14a28acd21b4:
  aws.ec2.nat_gateway.present:
  - name: nat-076cd14a28acd21b4
  - resource_id: nat-076cd14a28acd21b4
  - subnet_id: subnet-0d68d61b1ab708d42
  - connectivity_type: public
  - tags:
    - Key: Environment
      Value: test-dev
    - Key: Automation
      Value: 'true'
    - Key: Owner
      Value: org1
    - Key: KubernetesCluster
      Value: idem-test
    - Key: COGS
      Value: OPEX
    - Key: Name
      Value: idem-test-natgw-2
  - state: available
  - allocation_id: eipalloc-01319ee06efe14298


nat-0a49a65a4bb87370a:
  aws.ec2.nat_gateway.present:
  - name: nat-0a49a65a4bb87370a
  - resource_id: nat-0a49a65a4bb87370a
  - subnet_id: subnet-09cecc8c853637d3b
  - connectivity_type: public
  - tags:
    - Key: Environment
      Value: test-dev
    - Key: Automation
      Value: 'true'
    - Key: Owner
      Value: org1
    - Key: KubernetesCluster
      Value: idem-test
    - Key: COGS
      Value: OPEX
    - Key: Name
      Value: idem-test-natgw-0
  - state: available
  - allocation_id: eipalloc-0134ceb9112c887fd


nat-0c02a1f1d590b5534:
  aws.ec2.nat_gateway.present:
  - name: nat-0c02a1f1d590b5534
  - resource_id: nat-0c02a1f1d590b5534
  - subnet_id: subnet-0094b72dfb7ce6131
  - connectivity_type: public
  - tags:
    - Key: Environment
      Value: test-dev
    - Key: Automation
      Value: 'true'
    - Key: Owner
      Value: org1
    - Key: KubernetesCluster
      Value: idem-test
    - Key: COGS
      Value: OPEX
    - Key: Name
      Value: idem-test-natgw-1
  - state: available
  - allocation_id: eipalloc-001d4219447c325ca


rtb-01e542a8c56c9511f:
  aws.ec2.route_table.present:
  - associations:
    - AssociationState:
        State: associated
      Main: false
      RouteTableAssociationId: rtbassoc-0f2b91e5f78d7af47
      RouteTableId: rtb-01e542a8c56c9511f
      SubnetId: subnet-09cecc8c853637d3b
    name: rtb-01e542a8c56c9511f
    propagating_vgws: []
    resource_id: rtb-01e542a8c56c9511f
    routes:
    - DestinationCidrBlock: 10.170.0.0/16
      GatewayId: local
      Origin: CreateRouteTable
      State: active
    - DestinationCidrBlock: 0.0.0.0/0
      GatewayId: igw-0eee9bba485b312a8
      Origin: CreateRoute
      State: active
    tags:
    - Key: Environment
      Value: test-dev
    - Key: Name
      Value: idem-test-xyz-public-0
    - Key: Automation
      Value: 'true'
    - Key: COGS
      Value: OPEX
    - Key: Owner
      Value: org1
    - Key: KubernetesCluster
      Value: idem-test
    vpc_id: vpc-0738f2a523f4735bd


rtb-0445912793473da66:
  aws.ec2.route_table.present:
  - associations:
    - AssociationState:
        State: associated
      Main: false
      RouteTableAssociationId: rtbassoc-05b12a843fea97f87
      RouteTableId: rtb-0445912793473da66
      SubnetId: subnet-0094b72dfb7ce6131
    name: rtb-0445912793473da66
    propagating_vgws: []
    resource_id: rtb-0445912793473da66
    routes:
    - DestinationCidrBlock: 10.170.0.0/16
      GatewayId: local
      Origin: CreateRouteTable
      State: active
    - DestinationCidrBlock: 0.0.0.0/0
      GatewayId: igw-0eee9bba485b312a8
      Origin: CreateRoute
      State: active
    tags:
    - Key: Name
      Value: idem-test-xyz-public-1
    - Key: COGS
      Value: OPEX
    - Key: Automation
      Value: 'true'
    - Key: Environment
      Value: test-dev
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Owner
      Value: org1
    vpc_id: vpc-0738f2a523f4735bd


rtb-0516e0e06d933d9f4:
  aws.ec2.route_table.present:
  - associations:
    - AssociationState:
        State: associated
      Main: false
      RouteTableAssociationId: rtbassoc-080a96c3caa4d67bc
      RouteTableId: rtb-0516e0e06d933d9f4
      SubnetId: subnet-050732fa4616470d9
    name: rtb-0516e0e06d933d9f4
    propagating_vgws: []
    resource_id: rtb-0516e0e06d933d9f4
    routes:
    - DestinationCidrBlock: 10.170.0.0/16
      GatewayId: local
      Origin: CreateRouteTable
      State: active
    - DestinationCidrBlock: 0.0.0.0/0
      NatGatewayId: nat-0a49a65a4bb87370a
      Origin: CreateRoute
      State: active
    tags:
    - Key: COGS
      Value: OPEX
    - Key: Environment
      Value: test-dev
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Name
      Value: idem-test-xyz-private-0
    - Key: Owner
      Value: org1
    - Key: Automation
      Value: 'true'
    vpc_id: vpc-0738f2a523f4735bd


rtb-05bd8f7251c25d82c:
  aws.ec2.route_table.present:
  - associations:
    - AssociationState:
        State: associated
      Main: false
      RouteTableAssociationId: rtbassoc-0e7f80eb1863284da
      RouteTableId: rtb-05bd8f7251c25d82c
      SubnetId: subnet-0d68d61b1ab708d42
    name: rtb-05bd8f7251c25d82c
    propagating_vgws: []
    resource_id: rtb-05bd8f7251c25d82c
    routes:
    - DestinationCidrBlock: 10.170.0.0/16
      GatewayId: local
      Origin: CreateRouteTable
      State: active
    - DestinationCidrBlock: 0.0.0.0/0
      GatewayId: igw-0eee9bba485b312a8
      Origin: CreateRoute
      State: active
    tags:
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Owner
      Value: org1
    - Key: COGS
      Value: OPEX
    - Key: Automation
      Value: 'true'
    - Key: Name
      Value: idem-test-xyz-public-2
    - Key: Environment
      Value: test-dev
    vpc_id: vpc-0738f2a523f4735bd


rtb-0b0a3c628c59ac049:
  aws.ec2.route_table.present:
  - associations:
    - AssociationState:
        State: associated
      Main: false
      RouteTableAssociationId: rtbassoc-0ef8af8654a7c1fb3
      RouteTableId: rtb-0b0a3c628c59ac049
      SubnetId: subnet-039e53122e038d38c
    name: rtb-0b0a3c628c59ac049
    propagating_vgws: []
    resource_id: rtb-0b0a3c628c59ac049
    routes:
    - DestinationCidrBlock: 10.170.0.0/16
      GatewayId: local
      Origin: CreateRouteTable
      State: active
    - DestinationCidrBlock: 0.0.0.0/0
      NatGatewayId: nat-076cd14a28acd21b4
      Origin: CreateRoute
      State: active
    tags:
    - Key: COGS
      Value: OPEX
    - Key: Owner
      Value: org1
    - Key: Name
      Value: idem-test-xyz-private-2
    - Key: Environment
      Value: test-dev
    - Key: Automation
      Value: 'true'
    - Key: KubernetesCluster
      Value: idem-test
    vpc_id: vpc-0738f2a523f4735bd


rtb-0e8c5df719166603e:
  aws.ec2.route_table.present:
  - associations: []
    name: rtb-0e8c5df719166603e
    propagating_vgws: []
    resource_id: rtb-0e8c5df719166603e
    routes:
    - DestinationCidrBlock: 10.170.0.0/16
      GatewayId: local
      Origin: CreateRouteTable
      State: active
    tags: []
    vpc_id: vpc-0738f2a523f4735bd


rtb-0ee77d1deb1a1d86a:
  aws.ec2.route_table.present:
  - associations:
    - AssociationState:
        State: associated
      Main: false
      RouteTableAssociationId: rtbassoc-0832ce3bb146df047
      RouteTableId: rtb-0ee77d1deb1a1d86a
      SubnetId: subnet-05dfaa0d01a337199
    name: rtb-0ee77d1deb1a1d86a
    propagating_vgws: []
    resource_id: rtb-0ee77d1deb1a1d86a
    routes:
    - DestinationCidrBlock: 10.170.0.0/16
      GatewayId: local
      Origin: CreateRouteTable
      State: active
    - DestinationCidrBlock: 0.0.0.0/0
      NatGatewayId: nat-0c02a1f1d590b5534
      Origin: CreateRoute
      State: active
    tags:
    - Key: Owner
      Value: org1
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Name
      Value: idem-test-xyz-private-1
    - Key: Environment
      Value: test-dev
    - Key: COGS
      Value: OPEX
    - Key: Automation
      Value: 'true'
    vpc_id: vpc-0738f2a523f4735bd


sg-02b4fa0698eaa06cf:
  aws.ec2.security_group.present:
  - resource_id: sg-02b4fa0698eaa06cf
  - name: idem-test-temp-xyz-cluster-node
  - vpc_id: vpc-0738f2a523f4735bd
  - tags:
    - Key: cluster
      Value: xyz
    - Key: Environment
      Value: test-dev
    - Key: Name
      Value: idem-test-temp-xyz-cluster-node
    - Key: KubernetesCluster
      Value: idem-test
    - Key: role
      Value: xyz-worker
    - Key: kubernetes.io/cluster/idem-test
      Value: owned
    - Key: Owner
      Value: org1
    - Key: Automation
      Value: 'true'
    - Key: COGS
      Value: OPEX
  - description: Security group for all nodes in the cluster


sg-04fa08f7b284cd1e9:
  aws.ec2.security_group.present:
  - resource_id: sg-04fa08f7b284cd1e9
  - name: default
  - vpc_id: vpc-0738f2a523f4735bd
  - description: default VPC security group


sg-06552329287f9b206:
  aws.ec2.security_group.present:
  - resource_id: sg-06552329287f9b206
  - name: temp-20220505111435890800000003
  - vpc_id: vpc-0738f2a523f4735bd
  - output_variable_test: test
  - tags:
    - Key: Name
      Value: idem-test-nessus_vuln_scanner
  - description: nessus scanner


sg-070a797a4b433814b:
  aws.ec2.security_group.present:
  - resource_id: sg-070a797a4b433814b
  - name: idem-test-temp-xyz-cluster
  - vpc_id: vpc-0738f2a523f4735bd
  - tags:
    - Key: Environment
      Value: test-dev
    - Key: Automation
      Value: 'true'
    - Key: KubernetesCluster
      Value: idem-test
    - Key: COGS
      Value: OPEX
    - Key: Name
      Value: idem-test-temp-xyz
    - Key: Owner
      Value: org1
  - description: Cluster communication with worker nodes


sg-0f9910c81ca733164:
  aws.ec2.security_group.present:
  - resource_id: sg-0f9910c81ca733164
  - name: xyz-cluster-sg-idem-test-7532260
  - vpc_id: vpc-0738f2a523f4735bd
  - tags:
    - Key: kubernetes.io/cluster/idem-test
      Value: owned
    - Key: aws:xyz:cluster-name
      Value: idem-test
    - Key: Name
      Value: xyz-cluster-sg-idem-test-7532260
  - description: xyz created security group applied to ENI that is attached to xyz
      Control Plane master nodes, as well as any managed workloads.


sgr-00140d3e51aad2e43:
  aws.ec2.security_group_rule.present:
  - name: sgr-00140d3e51aad2e43
  - resource_id: sgr-00140d3e51aad2e43
  - group_id: sg-04fa08f7b284cd1e9
  - is_egress: false
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - tags: []
  - referenced_group_info:
      GroupId: sg-04fa08f7b284cd1e9
      UserId: '123456789012'


sgr-00ed93ac23eed55f6:
  aws.ec2.security_group_rule.present:
  - name: sgr-00ed93ac23eed55f6
  - resource_id: sgr-00ed93ac23eed55f6
  - group_id: sg-04fa08f7b284cd1e9
  - is_egress: true
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


sgr-06960cb41fe34b17f:
  aws.ec2.security_group_rule.present:
  - name: sgr-06960cb41fe34b17f
  - resource_id: sgr-06960cb41fe34b17f
  - group_id: sg-0f9910c81ca733164
  - is_egress: true
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


sgr-06fa8980bf9bffea7:
  aws.ec2.security_group_rule.present:
  - name: sgr-06fa8980bf9bffea7
  - resource_id: sgr-06fa8980bf9bffea7
  - group_id: sg-02b4fa0698eaa06cf
  - is_egress: false
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - tags: []
  - description: Allow worker Kubelets and pods to receive communication from the
      nessus scanner
  - referenced_group_info:
      GroupId: sg-06552329287f9b206
      UserId: '123456789012'


sgr-0746837a711b2f632:
  aws.ec2.security_group_rule.present:
  - name: sgr-0746837a711b2f632
  - resource_id: sgr-0746837a711b2f632
  - group_id: sg-02b4fa0698eaa06cf
  - is_egress: false
  - ip_protocol: tcp
  - from_port: 1025
  - to_port: 65535
  - tags: []
  - description: Allow worker Kubelets and pods to receive communication from the
      cluster control plane
  - referenced_group_info:
      GroupId: sg-070a797a4b433814b
      UserId: '123456789012'


sgr-0748e2625a1c6b9b1:
  aws.ec2.security_group_rule.present:
  - name: sgr-0748e2625a1c6b9b1
  - resource_id: sgr-0748e2625a1c6b9b1
  - group_id: sg-0f9910c81ca733164
  - is_egress: false
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - tags: []
  - referenced_group_info:
      GroupId: sg-0f9910c81ca733164
      UserId: '123456789012'


sgr-077c2651cc2eb9b1f:
  aws.ec2.security_group_rule.present:
  - name: sgr-077c2651cc2eb9b1f
  - resource_id: sgr-077c2651cc2eb9b1f
  - group_id: sg-070a797a4b433814b
  - is_egress: false
  - ip_protocol: tcp
  - from_port: 443
  - to_port: 443
  - tags: []
  - description: Allow pods to communicate with the cluster API Server
  - referenced_group_info:
      GroupId: sg-02b4fa0698eaa06cf
      UserId: '123456789012'


sgr-0895dc3282b1068e5:
  aws.ec2.security_group_rule.present:
  - name: sgr-0895dc3282b1068e5
  - resource_id: sgr-0895dc3282b1068e5
  - group_id: sg-06552329287f9b206
  - is_egress: true
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


sgr-0a410f396195a1a29:
  aws.ec2.security_group_rule.present:
  - name: sgr-0a410f396195a1a29
  - resource_id: sgr-0a410f396195a1a29
  - group_id: sg-02b4fa0698eaa06cf
  - is_egress: true
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


sgr-0a4ef3a78cdce5042:
  aws.ec2.security_group_rule.present:
  - name: sgr-0a4ef3a78cdce5042
  - resource_id: sgr-0a4ef3a78cdce5042
  - group_id: sg-070a797a4b433814b
  - is_egress: true
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


sgr-0b6d618ed84f17b83:
  aws.ec2.security_group_rule.present:
  - name: sgr-0b6d618ed84f17b83
  - resource_id: sgr-0b6d618ed84f17b83
  - group_id: sg-02b4fa0698eaa06cf
  - is_egress: false
  - ip_protocol: tcp
  - from_port: 22
  - to_port: 22
  - cidr_ipv4: 10.170.0.0/16
  - tags: []
  - description: Allow bastion to communicate with worker nodes


sgr-0d08ef367d62e82f3:
  aws.ec2.security_group_rule.present:
  - name: sgr-0d08ef367d62e82f3
  - resource_id: sgr-0d08ef367d62e82f3
  - group_id: sg-02b4fa0698eaa06cf
  - is_egress: false
  - ip_protocol: tcp
  - from_port: 443
  - to_port: 443
  - tags: []
  - description: Allow worker Kubelets and pods to receive communication from the
      cluster control plane
  - referenced_group_info:
      GroupId: sg-070a797a4b433814b
      UserId: '123456789012'


sgr-0ffb22f7ca7a0f16c:
  aws.ec2.security_group_rule.present:
  - name: sgr-0ffb22f7ca7a0f16c
  - resource_id: sgr-0ffb22f7ca7a0f16c
  - group_id: sg-02b4fa0698eaa06cf
  - is_egress: false
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - tags: []
  - description: Allow node to communicate with each other
  - referenced_group_info:
      GroupId: sg-02b4fa0698eaa06cf
      UserId: '123456789012'


subnet-0094b72dfb7ce6131:
  aws.ec2.subnet.present:
  - name: subnet-0094b72dfb7ce6131
  - resource_id: subnet-0094b72dfb7ce6131
  - vpc_id: vpc-0738f2a523f4735bd
  - cidr_block: 10.170.208.0/20
  - availability_zone: eu-west-3b
  - tags:
    - Key: KubernetesCluster
      Value: idem-test
    - Key: kubernetes.io/cluster/idem-test
      Value: shared
    - Key: kubernetes.io/role/elb
      Value: '1'
    - Key: Automation
      Value: 'true'
    - Key: Owner
      Value: org1
    - Key: COGS
      Value: OPEX
    - Key: Environment
      Value: test-dev
    - Key: Name
      Value: idem-test-temp-xyz-control-public


subnet-039e53122e038d38c:
  aws.ec2.subnet.present:
  - name: subnet-039e53122e038d38c
  - resource_id: subnet-039e53122e038d38c
  - vpc_id: vpc-0738f2a523f4735bd
  - cidr_block: 10.170.128.0/18
  - availability_zone: eu-west-3c
  - tags:
    - Key: Automation
      Value: 'true'
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Environment
      Value: test-dev
    - Key: kubernetes.io/cluster/idem-test
      Value: shared
    - Key: kubernetes.io/role/internal-elb
      Value: '1'
    - Key: COGS
      Value: OPEX
    - Key: Name
      Value: idem-test-temp-xyz-node-private
    - Key: Owner
      Value: org1


subnet-050732fa4616470d9:
  aws.ec2.subnet.present:
  - name: subnet-050732fa4616470d9
  - resource_id: subnet-050732fa4616470d9
  - vpc_id: vpc-0738f2a523f4735bd
  - cidr_block: 10.170.0.0/18
  - availability_zone: eu-west-3a
  - tags:
    - Key: COGS
      Value: OPEX
    - Key: Name
      Value: idem-test-temp-xyz-node-private
    - Key: kubernetes.io/role/internal-elb
      Value: '1'
    - Key: Environment
      Value: test-dev
    - Key: Automation
      Value: 'true'
    - Key: KubernetesCluster
      Value: idem-test
    - Key: kubernetes.io/cluster/idem-test
      Value: shared
    - Key: Owner
      Value: org1


subnet-05dfaa0d01a337199:
  aws.ec2.subnet.present:
  - name: subnet-05dfaa0d01a337199
  - resource_id: subnet-05dfaa0d01a337199
  - vpc_id: vpc-0738f2a523f4735bd
  - cidr_block: 10.170.64.0/18
  - availability_zone: eu-west-3b
  - tags:
    - Key: Automation
      Value: 'true'
    - Key: kubernetes.io/cluster/idem-test
      Value: shared
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Environment
      Value: test-dev
    - Key: COGS
      Value: OPEX
    - Key: kubernetes.io/role/internal-elb
      Value: '1'
    - Key: Owner
      Value: org1
    - Key: Name
      Value: idem-test-temp-xyz-node-private


subnet-09cecc8c853637d3b:
  aws.ec2.subnet.present:
  - name: subnet-09cecc8c853637d3b
  - resource_id: subnet-09cecc8c853637d3b
  - vpc_id: vpc-0738f2a523f4735bd
  - cidr_block: 10.170.192.0/20
  - availability_zone: eu-west-3a
  - tags:
    - Key: kubernetes.io/cluster/idem-test
      Value: shared
    - Key: Name
      Value: idem-test-temp-xyz-control-public
    - Key: Environment
      Value: test-dev
    - Key: kubernetes.io/role/elb
      Value: '1'
    - Key: Automation
      Value: 'true'
    - Key: KubernetesCluster
      Value: idem-test
    - Key: COGS
      Value: OPEX
    - Key: Owner
      Value: org1


subnet-0d68d61b1ab708d42:
  aws.ec2.subnet.present:
  - name: subnet-0d68d61b1ab708d42
  - resource_id: subnet-0d68d61b1ab708d42
  - vpc_id: vpc-0738f2a523f4735bd
  - cidr_block: 10.170.224.0/20
  - availability_zone: eu-west-3c
  - tags:
    - Key: Owner
      Value: org1
    - Key: kubernetes.io/role/elb
      Value: '1'
    - Key: COGS
      Value: OPEX
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Automation
      Value: 'true'
    - Key: Environment
      Value: test-dev
    - Key: Name
      Value: idem-test-temp-xyz-control-public
    - Key: kubernetes.io/cluster/idem-test
      Value: shared


test:
  aws.iam.role.present:
  - resource_id: test
  - name: test
  - arn: arn:aws:iam::123456789012:role/service-role/test
  - id: AROAX2FJ77DCZ632NHVVI
  - path: /service-role/
  - description: Proton pipeline service role
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "proton.amazonaws.com"}}], "Version": "2012-10-17"}'


test-arn:aws:iam::123456789012:policy/service-role/ProtonRolePolicy-test:
  aws.iam.role_policy_attachment.present:
  - role_name: test
  - policy_arn: arn:aws:iam::123456789012:policy/service-role/ProtonRolePolicy-test


vpc-0738f2a523f4735bd:
  aws.ec2.vpc.present:
  - name: vpc-0738f2a523f4735bd
  - resource_id: vpc-0738f2a523f4735bd
  - instance_tenancy: default
  - tags:
    - Key: COGS
      Value: OPEX
    - Key: Automation
      Value: 'true'
    - Key: Owner
      Value: org1
    - Key: kubernetes.io/cluster/idem-test
      Value: shared
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Environment
      Value: test-dev
    - Key: Name
      Value: idem-test-temp-xyz-cluster-node
  - cidr_block_association_set:
    - AssociationId: vpc-cidr-assoc-02ab4e7064a606d2b
      CidrBlock: 10.170.0.0/16
      CidrBlockState:
        State: associated
  - enable_dns_hostnames: true
  - enable_dns_support: true


xyz-idem-test-admin:
  aws.iam.role.present:
  - resource_id: xyz-idem-test-admin
  - name: xyz-idem-test-admin
  - arn: arn:aws:iam::123456789012:role/xyz-idem-test-admin
  - id: AROAX2FJ77DC33OWDECR6
  - path: /
  - max_session_duration: 3600
  - tags:
    - Key: Environment
      Value: test-dev
    - Key: Owner
      Value: org1
    - Key: Automation
      Value: 'true'
    - Key: COGS
      Value: OPEX
    - Key: KubernetesCluster
      Value: idem-test
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {"ForAnyValue:StringEquals": {"aws:username": ["user1", "user2", "user3", "user4",
      "user5"]}}, "Effect": "Allow", "Principal": {"AWS": "arn:aws:iam::123456789012:root"}},
      {"Action": "sts:AssumeRole", "Effect": "Allow", "Principal": {"Service": "xyz.amazonaws.com"}}],
      "Version": "2012-10-17"}'


xyz-idem-test-admin-arn:aws:iam::123456789012:policy/xyz-idem-test-admin:
  aws.iam.role_policy_attachment.present:
  - role_name: xyz-idem-test-admin
  - policy_arn: arn:aws:iam::123456789012:policy/xyz-idem-test-admin


xyz-idem-test-jenkins:
  aws.iam.role.present:
  - resource_id: xyz-idem-test-jenkins
  - name: xyz-idem-test-jenkins
  - arn: arn:aws:iam::123456789012:role/xyz-idem-test-jenkins
  - id: AROAX2FJ77DCQHIULDKIS
  - path: /
  - max_session_duration: 3600
  - tags:
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Automation
      Value: 'true'
    - Key: Environment
      Value: test-dev
    - Key: Owner
      Value: org1
    - Key: COGS
      Value: OPEX
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {"ForAnyValue:StringEquals": {"aws:username": ["user1", "user2", "user3", "user4",
      "user5"]}}, "Effect": "Allow", "Principal": {"AWS": "arn:aws:iam::123456789012:root"}},
      {"Action": "sts:AssumeRole", "Effect": "Allow", "Principal": {"AWS": "arn:aws:iam::123456789012:user/xyz/idem-test/extension-jenkins-idem-test"}}],
      "Version": "2012-10-17"}'


xyz-idem-test-jenkins-arn:aws:iam::123456789012:policy/xyz-idem-test-jenkins:
  aws.iam.role_policy_attachment.present:
  - role_name: xyz-idem-test-jenkins
  - policy_arn: arn:aws:iam::123456789012:policy/xyz-idem-test-jenkins


xyz-idem-test_redlock_flow_log_group:
  aws.cloudwatch.log_group.present:
  - name: xyz-idem-test_redlock_flow_log_group
  - resource_id: xyz-idem-test_redlock_flow_log_group
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:xyz-idem-test_redlock_flow_log_group:*
  - tags:
      Automation: 'true'
      COGS: OPEX
      Environment: test-dev
      KubernetesCluster: idem-test
      Owner: org1
