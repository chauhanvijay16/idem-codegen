resource "aws_eks_cluster" "cluster" {
  name     = var.clusterName
  role_arn = aws_iam_role.cluster.arn
  version  = var.clusterVersion
  tags     = local.tags

  vpc_config {
    security_group_ids = [aws_security_group.cluster.id]
    subnet_ids         = concat(aws_subnet.cluster.*.id, aws_subnet.xyz_public_subnet.*.id)
  }
  enabled_cluster_log_types = ["controllerManager", "scheduler", "authenticator", "audit", "api"]
  lifecycle {
    ignore_changes = [vpc_config.0.subnet_ids]
  }
}
