variable "profile" {
}

variable "region" {
  default = "us-west-2"
}

variable "environment" {
  type = map(string)

  default = {
    dev4    = "development"
    staging = "staging"
    prod    = "production"
    scaleperf   = "scaleperf"
  }
}

variable "admin_users" {
  type    = list(string)
  default = [""]
}

variable "cogs" {
}

variable "automation" {
  default = true
}

variable "singleAz" {
  default = false
}

variable "acl" {
  default = "private"
}

variable "clusterName" {
}

variable "product" {
}

variable "service" {
}

variable "owner" {
}
