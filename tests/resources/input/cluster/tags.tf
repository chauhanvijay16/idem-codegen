locals {
  tags = {
    Environment       = var.profile
    Owner             = var.owner
    KubernetesCluster = var.clusterName
    COGS              = var.cogs
    Automation        = var.automation
  }
}
