region                                = "eu-west-3"
profile                               = "test-dev"
owner                                 = "org1"
cogs                                  = "OPEX"
default_domain                        = "potato-dev.com"
home                                  = ""
msk_kafka_domain                      = true
clusterName                           = "idem-test"
VpcSuperNet                           = "10.170."
wavefrontProxy                        = "test-dev-mgmt-2-proxy.internal.potato.beachops.io"
create_sym_nodegroup-1                = true
create_sym_nodegroup_opssolr          = false
clusterVersion                        = "1.20" # please match controller version (important if upgrading xyz version)
asgEnabled                            = true
instanceType                          = "m5a.xlarge"
desired_capacity_cluster              = "3"
instanceTypeArm                       = "m6g.xlarge"
desired_capacity_cluster_arm          = "1"
sym_nodegroup_cluster-sym-1_capacity  = "3"
sym_cluster_monitoring                = false
singleAz                              = true
pam_cluster_access                    = true
singleAzIndex                         = 0 # Represents 1st zone from the aws describe availibility zone command
enable-jenkins-rolling-upgrade-policy = true
admin_users = [
  "user1",
  "user2",
  "user3",
  "user4",
  "user5"
]
# can create/destroy namespaces
cluster_admin = [
  "user1"
]
# can read/write to namespace resources
cluster_edit = [
  "user1"
]
# can read namespaces
cluster_read = [
  "user1"
]
