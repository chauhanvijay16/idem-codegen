import json
from collections import ChainMap

import pytest
import yaml


@pytest.mark.asyncio
def test_parameterize_resource(hub):
    sls_data = yaml.safe_load(
        open(f"{hub.test.idem_codegen.current_path}/files/parameterize.sls")
    )

    hub.tf_idem.RUNS["TF_RESOURCE_MAP"] = json.load(
        open(f"{hub.test.idem_codegen.current_path}/files/terraform_resource_map.json")
    )
    hub.tf_idem.RUNS["TF_VARIABLES"] = {}
    hub.tf_idem.RUNS["MODULE_VARIABLES"] = {}
    hub.tf_idem.RUNS["OUTPUT_VARIABLES"] = {}

    parameterized_sls_data = hub.tf_idem.generator.parameterizer.default.parameterize(
        sls_data
    )

    resource_attributes = list(
        parameterized_sls_data["aws_subnet.xyz_public_subnet-0"].values()
    )[0]
    resource_map = dict(ChainMap(*resource_attributes))
    assert '{{ params.get("aws_subnet.xyz_public_subnet-0")}}' == resource_map.get(
        "resource_id"
    )
    statement = resource_map.get("vpc_id")
    expression = statement.split("?")[0]
    data_arg_bind = statement.split("?")[1].split(" : ")[1]
    # parameterizing the if condition inside ternery expression
    assert '{{ params["create_vpc"] }}' == expression.strip()
    # Data arg binded value
    assert "${aws.ec2.vpc:data.aws_vpc.vpc:[0]:resource_id}" == data_arg_bind.strip()

    resource_attributes = list(parameterized_sls_data["data.aws_vpc.vpc"].values())[0]
    resource_map = dict(ChainMap(*resource_attributes))
    # parameterized data filter value
    assert '{{ params["VpcName"] }}' == resource_map.get("filters")[0]["value"]
