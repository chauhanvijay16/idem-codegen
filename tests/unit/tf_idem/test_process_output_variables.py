def test_process_output_variables(hub):
    hub.tf_idem.RUNS["MODULE_VARIABLES"] = {
        "aws_iam_role-eks-admin": "${module.iam.aws_iam_role-eks-admin}",
        "aws_kms_key-credstash_key-arn": "${module.iam.aws_kms_key-credstash_key-arn}",
        "aws_kms_key-credstash_key-key-id": "${module.iam.aws_kms_key-credstash_key-key-id}",
        "aws_iam_role-cluster-node-arn": "${module.iam.aws_iam_role-cluster-node-arn}",
    }
    hub.tf_idem.RUNS["OUTPUT_VARIABLES"] = {
        "${module.iam.aws_kms_key-credstash_key-key-id}": "${aws_kms_key.credstash_key.key_id}",
        "${module.iam.aws_kms_key-credstash_key-arn}": "${aws_kms_key.credstash_key.arn}",
        "${module.iam.aws_iam_role-eks-admin}": "${aws_iam_role.eks-admin.arn}",
        "${module.iam.aws_iam_role-cluster-node-arn}": "${aws_iam_role.cluster-node.arn}",
    }
    var_input = "${var.aws_iam_role-eks-admin}"
    value = hub.tf_idem.generator.parameterizer.default.process_output_variables(
        var_input
    )
    assert "${aws.iam.role:eks-admin-search:arn}" == value
    assert "aws_iam_role-eks-admin-search" in hub.tf_idem.RUNS.get("SEARCH_BLOCK")
