AWS-QuickSetup-StackSet-Local-AdministrationRole:
  aws.iam.role.present:
  - resource_id: AWS-QuickSetup-StackSet-Local-AdministrationRole
  - name: AWS-QuickSetup-StackSet-Local-AdministrationRole
  - arn: arn:aws:iam::123456789012:role/AWS-QuickSetup-StackSet-Local-AdministrationRole
  - id: AROAX2FJ77DCYHZJS2UUV
  - path: /
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "cloudformation.amazonaws.com"}}], "Version":
      "2012-10-17"}'


AWS-QuickSetup-StackSet-Local-AdministrationRole-AssumeRole-AWS-QuickSetup-StackSet-Local-ExecutionRole:
  aws.iam.role_policy.present:
  - resource_id: AWS-QuickSetup-StackSet-Local-AdministrationRole-AssumeRole-AWS-QuickSetup-StackSet-Local-ExecutionRole
  - role_name: AWS-QuickSetup-StackSet-Local-AdministrationRole
  - name: AssumeRole-AWS-QuickSetup-StackSet-Local-ExecutionRole
  - policy_document: '{"Statement": [{"Action": ["sts:AssumeRole"], "Effect": "Allow",
      "Resource": ["arn:*:iam::*:role/AWS-QuickSetup-StackSet-Local-ExecutionRole"]}],
      "Version": "2012-10-17"}'


AWS-QuickSetup-StackSet-Local-ExecutionRole:
  aws.iam.role.present:
  - resource_id: AWS-QuickSetup-StackSet-Local-ExecutionRole
  - name: AWS-QuickSetup-StackSet-Local-ExecutionRole
  - arn: arn:aws:iam::123456789012:role/AWS-QuickSetup-StackSet-Local-ExecutionRole
  - id: AROAX2FJ77DCTRT7XN4BI
  - path: /
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"AWS": "arn:aws:iam::123456789012:role/AWS-QuickSetup-StackSet-Local-AdministrationRole"}}],
      "Version": "2012-10-17"}'


AWS-QuickSetup-StackSet-Local-ExecutionRole-arn:aws:iam::aws:policy/AdministratorAccess:
  aws.iam.role_policy_attachment.present:
  - role_name: AWS-QuickSetup-StackSet-Local-ExecutionRole
  - policy_arn: arn:aws:iam::aws:policy/AdministratorAccess


AWSServiceRoleForAWSCloud9:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForAWSCloud9
  - name: AWSServiceRoleForAWSCloud9
  - arn: arn:aws:iam::123456789012:role/aws-service-role/cloud9.amazonaws.com/AWSServiceRoleForAWSCloud9
  - id: AROAX2FJ77DCWO6CED7WB
  - path: /aws-service-role/cloud9.amazonaws.com/
  - description: Service linked role for AWS Cloud9
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "cloud9.amazonaws.com"}}], "Version": "2012-10-17"}'


AWSServiceRoleForAWSCloud9-arn:aws:iam::aws:policy/aws-service-role/AWSCloud9ServiceRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForAWSCloud9
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AWSCloud9ServiceRolePolicy


AWSServiceRoleForAmazonBraket:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForAmazonBraket
  - name: AWSServiceRoleForAmazonBraket
  - arn: arn:aws:iam::123456789012:role/aws-service-role/braket.amazonaws.com/AWSServiceRoleForAmazonBraket
  - id: AROAX2FJ77DC3NDCJUSOE
  - path: /aws-service-role/braket.amazonaws.com/
  - description: Service role created by Amazon Braket Console
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "braket.amazonaws.com"}}], "Version": "2012-10-17"}'


AWSServiceRoleForAmazonBraket-arn:aws:iam::aws:policy/aws-service-role/AmazonBraketServiceRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForAmazonBraket
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AmazonBraketServiceRolePolicy


AWSServiceRoleForAmazonElasticFileSystem:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForAmazonElasticFileSystem
  - name: AWSServiceRoleForAmazonElasticFileSystem
  - arn: arn:aws:iam::123456789012:role/aws-service-role/elasticfilesystem.amazonaws.com/AWSServiceRoleForAmazonElasticFileSystem
  - id: AROAX2FJ77DCZH5XYF76X
  - path: /aws-service-role/elasticfilesystem.amazonaws.com/
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "elasticfilesystem.amazonaws.com"}}], "Version":
      "2012-10-17"}'


AWSServiceRoleForAmazonElasticFileSystem-arn:aws:iam::aws:policy/aws-service-role/AmazonElasticFileSystemServiceRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForAmazonElasticFileSystem
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AmazonElasticFileSystemServiceRolePolicy


AWSServiceRoleForAmazonGuardDuty:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForAmazonGuardDuty
  - name: AWSServiceRoleForAmazonGuardDuty
  - arn: arn:aws:iam::123456789012:role/aws-service-role/guardduty.amazonaws.com/AWSServiceRoleForAmazonGuardDuty
  - id: AROAX2FJ77DCZCOCKFNFT
  - path: /aws-service-role/guardduty.amazonaws.com/
  - description: 'A service-linked role required for Amazon GuardDuty to access your
      resources. '
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "guardduty.amazonaws.com"}}], "Version": "2012-10-17"}'


AWSServiceRoleForAmazonGuardDuty-arn:aws:iam::aws:policy/aws-service-role/AmazonGuardDutyServiceRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForAmazonGuardDuty
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AmazonGuardDutyServiceRolePolicy


AWSServiceRoleForAmazonMQ:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForAmazonMQ
  - name: AWSServiceRoleForAmazonMQ
  - arn: arn:aws:iam::123456789012:role/aws-service-role/mq.amazonaws.com/AWSServiceRoleForAmazonMQ
  - id: AROAX2FJ77DCRRO6Y53RR
  - path: /aws-service-role/mq.amazonaws.com/
  - description: Allows Amazon MQ to call AWS services on your behalf
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "mq.amazonaws.com"}}], "Version": "2012-10-17"}'


AWSServiceRoleForAmazonMQ-arn:aws:iam::aws:policy/aws-service-role/AmazonMQServiceRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForAmazonMQ
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AmazonMQServiceRolePolicy


AWSServiceRoleForAmazonOpenSearchService:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForAmazonOpenSearchService
  - name: AWSServiceRoleForAmazonOpenSearchService
  - arn: arn:aws:iam::123456789012:role/aws-service-role/opensearchservice.amazonaws.com/AWSServiceRoleForAmazonOpenSearchService
  - id: AROAX2FJ77DCQWW7SGRAB
  - path: /aws-service-role/opensearchservice.amazonaws.com/
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "opensearchservice.amazonaws.com"}}], "Version":
      "2012-10-17"}'


AWSServiceRoleForAmazonOpenSearchService-arn:aws:iam::aws:policy/aws-service-role/AmazonOpenSearchServiceRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForAmazonOpenSearchService
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AmazonOpenSearchServiceRolePolicy


AWSServiceRoleForAmazonSSM:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForAmazonSSM
  - name: AWSServiceRoleForAmazonSSM
  - arn: arn:aws:iam::123456789012:role/aws-service-role/ssm.amazonaws.com/AWSServiceRoleForAmazonSSM
  - id: AROAX2FJ77DCU3AAWCVYJ
  - path: /aws-service-role/ssm.amazonaws.com/
  - description: Provides access to AWS Resources managed or used by Amazon SSM.
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "ssm.amazonaws.com"}}], "Version": "2012-10-17"}'


AWSServiceRoleForAmazonSSM-arn:aws:iam::aws:policy/aws-service-role/AmazonSSMServiceRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForAmazonSSM
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AmazonSSMServiceRolePolicy


AWSServiceRoleForAmazonxyz:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForAmazonxyz
  - name: AWSServiceRoleForAmazonxyz
  - arn: arn:aws:iam::123456789012:role/aws-service-role/xyz.amazonaws.com/AWSServiceRoleForAmazonxyz
  - id: AROAX2FJ77DC75XAPS2YS
  - path: /aws-service-role/xyz.amazonaws.com/
  - description: Allows Amazon xyz to call AWS services on your behalf.
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "xyz.amazonaws.com"}}], "Version": "2012-10-17"}'


AWSServiceRoleForAmazonxyz-arn:aws:iam::aws:policy/aws-service-role/AmazonxyzServiceRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForAmazonxyz
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AmazonxyzServiceRolePolicy


AWSServiceRoleForAmazonxyzForFargate:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForAmazonxyzForFargate
  - name: AWSServiceRoleForAmazonxyzForFargate
  - arn: arn:aws:iam::123456789012:role/aws-service-role/xyz-fargate.amazonaws.com/AWSServiceRoleForAmazonxyzForFargate
  - id: AROAX2FJ77DCRGULKS6PV
  - path: /aws-service-role/xyz-fargate.amazonaws.com/
  - description: This policy grants necessary permissions to Amazon xyz to run fargate
      tasks
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "xyz-fargate.amazonaws.com"}}], "Version":
      "2012-10-17"}'


AWSServiceRoleForAmazonxyzForFargate-arn:aws:iam::aws:policy/aws-service-role/AmazonxyzForFargateServiceRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForAmazonxyzForFargate
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AmazonxyzForFargateServiceRolePolicy


AWSServiceRoleForAmazonxyzNodegroup:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForAmazonxyzNodegroup
  - name: AWSServiceRoleForAmazonxyzNodegroup
  - arn: arn:aws:iam::123456789012:role/aws-service-role/xyz-nodegroup.amazonaws.com/AWSServiceRoleForAmazonxyzNodegroup
  - id: AROAX2FJ77DCWMKF5DRTO
  - path: /aws-service-role/xyz-nodegroup.amazonaws.com/
  - description: This policy allows Amazon xyz to create and manage Nodegroups
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "xyz-nodegroup.amazonaws.com"}}], "Version":
      "2012-10-17"}'


AWSServiceRoleForAmazonxyzNodegroup-arn:aws:iam::aws:policy/aws-service-role/AWSServiceRoleForAmazonxyzNodegroup:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForAmazonxyzNodegroup
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AWSServiceRoleForAmazonxyzNodegroup


AWSServiceRoleForApplicationAutoScaling_DynamoDBTable:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForApplicationAutoScaling_DynamoDBTable
  - name: AWSServiceRoleForApplicationAutoScaling_DynamoDBTable
  - arn: arn:aws:iam::123456789012:role/aws-service-role/dynamodb.application-autoscaling.amazonaws.com/AWSServiceRoleForApplicationAutoScaling_DynamoDBTable
  - id: AROAX2FJ77DCWYM4GYO65
  - path: /aws-service-role/dynamodb.application-autoscaling.amazonaws.com/
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "dynamodb.application-autoscaling.amazonaws.com"}}],
      "Version": "2012-10-17"}'


? AWSServiceRoleForApplicationAutoScaling_DynamoDBTable-arn:aws:iam::aws:policy/aws-service-role/AWSApplicationAutoscalingDynamoDBTablePolicy


: aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForApplicationAutoScaling_DynamoDBTable
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AWSApplicationAutoscalingDynamoDBTablePolicy


AWSServiceRoleForAutoScaling:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForAutoScaling
  - name: AWSServiceRoleForAutoScaling
  - arn: arn:aws:iam::123456789012:role/aws-service-role/autoscaling.amazonaws.com/AWSServiceRoleForAutoScaling
  - id: AROAIXAVZRCZRYFDGCIME
  - path: /aws-service-role/autoscaling.amazonaws.com/
  - description: Default Service-Linked Role enables access to AWS Services and Resources
      used or managed by Auto Scaling
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "autoscaling.amazonaws.com"}}], "Version":
      "2012-10-17"}'


AWSServiceRoleForAutoScaling-arn:aws:iam::aws:policy/aws-service-role/AutoScalingServiceRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForAutoScaling
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AutoScalingServiceRolePolicy


AWSServiceRoleForBackup:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForBackup
  - name: AWSServiceRoleForBackup
  - arn: arn:aws:iam::123456789012:role/aws-service-role/backup.amazonaws.com/AWSServiceRoleForBackup
  - id: AROAX2FJ77DCXHBSDTACQ
  - path: /aws-service-role/backup.amazonaws.com/
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "backup.amazonaws.com"}}], "Version": "2012-10-17"}'


AWSServiceRoleForBackup-arn:aws:iam::aws:policy/aws-service-role/AWSBackupServiceLinkedRolePolicyForBackup:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForBackup
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AWSBackupServiceLinkedRolePolicyForBackup


AWSServiceRoleForCloudWatchEvents:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForCloudWatchEvents
  - name: AWSServiceRoleForCloudWatchEvents
  - arn: arn:aws:iam::123456789012:role/aws-service-role/events.amazonaws.com/AWSServiceRoleForCloudWatchEvents
  - id: AROAX2FJ77DCWZCEVKZS4
  - path: /aws-service-role/events.amazonaws.com/
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "events.amazonaws.com"}}], "Version": "2012-10-17"}'


AWSServiceRoleForCloudWatchEvents-arn:aws:iam::aws:policy/aws-service-role/CloudWatchEventsServiceRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForCloudWatchEvents
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/CloudWatchEventsServiceRolePolicy


AWSServiceRoleForComputeOptimizer:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForComputeOptimizer
  - name: AWSServiceRoleForComputeOptimizer
  - arn: arn:aws:iam::123456789012:role/aws-service-role/compute-optimizer.amazonaws.com/AWSServiceRoleForComputeOptimizer
  - id: AROAX2FJ77DCRUVCZE4XO
  - path: /aws-service-role/compute-optimizer.amazonaws.com/
  - description: Allows ComputeOptimizer to call AWS services and collect workload
      details on your behalf.
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "compute-optimizer.amazonaws.com"}}], "Version":
      "2012-10-17"}'


AWSServiceRoleForComputeOptimizer-arn:aws:iam::aws:policy/aws-service-role/ComputeOptimizerServiceRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForComputeOptimizer
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/ComputeOptimizerServiceRolePolicy


AWSServiceRoleForEC2Spot:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForEC2Spot
  - name: AWSServiceRoleForEC2Spot
  - arn: arn:aws:iam::123456789012:role/aws-service-role/spot.amazonaws.com/AWSServiceRoleForEC2Spot
  - id: AROAIWYJPB6SSVRYB2JBK
  - path: /aws-service-role/spot.amazonaws.com/
  - description: Default EC2 Spot Service Linked Role
  - max_session_duration: 3600
  - tags:
    - Key: tag-key
      Value: tag-value
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "spot.amazonaws.com"}}], "Version": "2012-10-17"}'


AWSServiceRoleForEC2Spot-arn:aws:iam::aws:policy/aws-service-role/AWSEC2SpotServiceRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForEC2Spot
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AWSEC2SpotServiceRolePolicy


AWSServiceRoleForEC2SpotFleet:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForEC2SpotFleet
  - name: AWSServiceRoleForEC2SpotFleet
  - arn: arn:aws:iam::123456789012:role/aws-service-role/spotfleet.amazonaws.com/AWSServiceRoleForEC2SpotFleet
  - id: AROAJCTOK4KQO3LXR24PU
  - path: /aws-service-role/spotfleet.amazonaws.com/
  - description: Default EC2 Spot Fleet Service Linked Role
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "spotfleet.amazonaws.com"}}], "Version": "2012-10-17"}'


AWSServiceRoleForEC2SpotFleet-arn:aws:iam::aws:policy/aws-service-role/AWSEC2SpotFleetServiceRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForEC2SpotFleet
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AWSEC2SpotFleetServiceRolePolicy


AWSServiceRoleForECS:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForECS
  - name: AWSServiceRoleForECS
  - arn: arn:aws:iam::123456789012:role/aws-service-role/ecs.amazonaws.com/AWSServiceRoleForECS
  - id: AROAX2FJ77DCQGWLDN5BE
  - path: /aws-service-role/ecs.amazonaws.com/
  - description: Role to enable Amazon ECS to manage your cluster.
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "ecs.amazonaws.com"}}], "Version": "2012-10-17"}'


AWSServiceRoleForECS-arn:aws:iam::aws:policy/aws-service-role/AmazonECSServiceRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForECS
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AmazonECSServiceRolePolicy


AWSServiceRoleForEMRCleanup:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForEMRCleanup
  - name: AWSServiceRoleForEMRCleanup
  - arn: arn:aws:iam::123456789012:role/aws-service-role/elasticmapreduce.amazonaws.com/AWSServiceRoleForEMRCleanup
  - id: AROAJCAGNAXQNDAFJ6OAY
  - path: /aws-service-role/elasticmapreduce.amazonaws.com/
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "elasticmapreduce.amazonaws.com"}}], "Version":
      "2012-10-17"}'


AWSServiceRoleForEMRCleanup-arn:aws:iam::aws:policy/aws-service-role/AmazonEMRCleanupPolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForEMRCleanup
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AmazonEMRCleanupPolicy


AWSServiceRoleForElastiCache:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForElastiCache
  - name: AWSServiceRoleForElastiCache
  - arn: arn:aws:iam::123456789012:role/aws-service-role/elasticache.amazonaws.com/AWSServiceRoleForElastiCache
  - id: AROAX2FJ77DCRG7TXWCIC
  - path: /aws-service-role/elasticache.amazonaws.com/
  - description: This policy allows ElastiCache to manage AWS resources on your behalf
      as necessary for managing your cache.
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "elasticache.amazonaws.com"}}], "Version":
      "2012-10-17"}'


AWSServiceRoleForElastiCache-arn:aws:iam::aws:policy/aws-service-role/ElastiCacheServiceRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForElastiCache
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/ElastiCacheServiceRolePolicy


AWSServiceRoleForElasticLoadBalancing:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForElasticLoadBalancing
  - name: AWSServiceRoleForElasticLoadBalancing
  - arn: arn:aws:iam::123456789012:role/aws-service-role/elasticloadbalancing.amazonaws.com/AWSServiceRoleForElasticLoadBalancing
  - id: AROAIHBDLHAYY6XX5SPEK
  - path: /aws-service-role/elasticloadbalancing.amazonaws.com/
  - description: Allows ELB to call AWS services on your behalf.
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "elasticloadbalancing.amazonaws.com"}}], "Version":
      "2012-10-17"}'


AWSServiceRoleForElasticLoadBalancing-arn:aws:iam::aws:policy/aws-service-role/AWSElasticLoadBalancingServiceRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForElasticLoadBalancing
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AWSElasticLoadBalancingServiceRolePolicy


AWSServiceRoleForGlobalAccelerator:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForGlobalAccelerator
  - name: AWSServiceRoleForGlobalAccelerator
  - arn: arn:aws:iam::123456789012:role/aws-service-role/globalaccelerator.amazonaws.com/AWSServiceRoleForGlobalAccelerator
  - id: AROAX2FJ77DC2OLFBN3WS
  - path: /aws-service-role/globalaccelerator.amazonaws.com/
  - description: Allows Global Accelerator to call AWS services on customer's behalf
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "globalaccelerator.amazonaws.com"}}], "Version":
      "2012-10-17"}'


AWSServiceRoleForGlobalAccelerator-arn:aws:iam::aws:policy/aws-service-role/AWSGlobalAcceleratorSLRPolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForGlobalAccelerator
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AWSGlobalAcceleratorSLRPolicy


AWSServiceRoleForImageBuilder:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForImageBuilder
  - name: AWSServiceRoleForImageBuilder
  - arn: arn:aws:iam::123456789012:role/aws-service-role/imagebuilder.amazonaws.com/AWSServiceRoleForImageBuilder
  - id: AROAX2FJ77DCXMK7OH4PI
  - path: /aws-service-role/imagebuilder.amazonaws.com/
  - description: Allows EC2 Image Builder to call AWS services on your behalf.
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "imagebuilder.amazonaws.com"}}], "Version":
      "2012-10-17"}'


AWSServiceRoleForImageBuilder-arn:aws:iam::aws:policy/aws-service-role/AWSServiceRoleForImageBuilder:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForImageBuilder
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AWSServiceRoleForImageBuilder


AWSServiceRoleForKafka:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForKafka
  - name: AWSServiceRoleForKafka
  - arn: arn:aws:iam::123456789012:role/aws-service-role/kafka.amazonaws.com/AWSServiceRoleForKafka
  - id: AROAX2FJ77DC4WCTGXP4I
  - path: /aws-service-role/kafka.amazonaws.com/
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "kafka.amazonaws.com"}}], "Version": "2012-10-17"}'


AWSServiceRoleForKafka-arn:aws:iam::aws:policy/aws-service-role/KafkaServiceRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForKafka
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/KafkaServiceRolePolicy


AWSServiceRoleForKeyManagementServiceMultiRegionKeys:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForKeyManagementServiceMultiRegionKeys
  - name: AWSServiceRoleForKeyManagementServiceMultiRegionKeys
  - arn: arn:aws:iam::123456789012:role/aws-service-role/mrk.kms.amazonaws.com/AWSServiceRoleForKeyManagementServiceMultiRegionKeys
  - id: AROAX2FJ77DC3DCDPV2QL
  - path: /aws-service-role/mrk.kms.amazonaws.com/
  - description: Enables access to AWS services and resources required for AWS KMS
      Multi-Region Keys
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "mrk.kms.amazonaws.com"}}], "Version": "2012-10-17"}'


? AWSServiceRoleForKeyManagementServiceMultiRegionKeys-arn:aws:iam::aws:policy/aws-service-role/AWSKeyManagementServiceMultiRegionKeysServiceRolePolicy


: aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForKeyManagementServiceMultiRegionKeys
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AWSKeyManagementServiceMultiRegionKeysServiceRolePolicy


AWSServiceRoleForOrganizations:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForOrganizations
  - name: AWSServiceRoleForOrganizations
  - arn: arn:aws:iam::123456789012:role/aws-service-role/organizations.amazonaws.com/AWSServiceRoleForOrganizations
  - id: AROAJOCZGPKD6WKT7USIS
  - path: /aws-service-role/organizations.amazonaws.com/
  - description: Service-linked role used by AWS Organizations to enable integration
      of other AWS services with Organizations.
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "organizations.amazonaws.com"}}], "Version":
      "2012-10-17"}'


AWSServiceRoleForOrganizations-arn:aws:iam::aws:policy/aws-service-role/AWSOrganizationsServiceTrustPolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForOrganizations
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AWSOrganizationsServiceTrustPolicy


AWSServiceRoleForRDS:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForRDS
  - name: AWSServiceRoleForRDS
  - arn: arn:aws:iam::123456789012:role/aws-service-role/rds.amazonaws.com/AWSServiceRoleForRDS
  - id: AROAJ6IHAHKHCDB3EVZIS
  - path: /aws-service-role/rds.amazonaws.com/
  - description: Allows Amazon RDS to manage AWS resources on your behalf
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "rds.amazonaws.com"}}], "Version": "2012-10-17"}'


AWSServiceRoleForRDS-arn:aws:iam::aws:policy/aws-service-role/AmazonRDSServiceRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForRDS
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AmazonRDSServiceRolePolicy


AWSServiceRoleForRedshift:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForRedshift
  - name: AWSServiceRoleForRedshift
  - arn: arn:aws:iam::123456789012:role/aws-service-role/redshift.amazonaws.com/AWSServiceRoleForRedshift
  - id: AROAIW7EHPQGLFP2UBK2Q
  - path: /aws-service-role/redshift.amazonaws.com/
  - description: 'Allows Amazon Redshift to call AWS services on your behalf '
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "redshift.amazonaws.com"}}], "Version": "2012-10-17"}'


AWSServiceRoleForRedshift-arn:aws:iam::aws:policy/aws-service-role/AmazonRedshiftServiceLinkedRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForRedshift
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AmazonRedshiftServiceLinkedRolePolicy


AWSServiceRoleForSecurityHub:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForSecurityHub
  - name: AWSServiceRoleForSecurityHub
  - arn: arn:aws:iam::123456789012:role/aws-service-role/securityhub.amazonaws.com/AWSServiceRoleForSecurityHub
  - id: AROAX2FJ77DC6HB5D4OFO
  - path: /aws-service-role/securityhub.amazonaws.com/
  - description: A service-linked role required for AWS Security Hub to access your
      resources.
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "securityhub.amazonaws.com"}}], "Version":
      "2012-10-17"}'


AWSServiceRoleForSecurityHub-arn:aws:iam::aws:policy/aws-service-role/AWSSecurityHubServiceRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForSecurityHub
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AWSSecurityHubServiceRolePolicy


AWSServiceRoleForSupport:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForSupport
  - name: AWSServiceRoleForSupport
  - arn: arn:aws:iam::123456789012:role/aws-service-role/support.amazonaws.com/AWSServiceRoleForSupport
  - id: AROAJ3J5Q3TRIL5PZGZWU
  - path: /aws-service-role/support.amazonaws.com/
  - description: Enables resource access for AWS to provide billing, administrative
      and support services
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "support.amazonaws.com"}}], "Version": "2012-10-17"}'


AWSServiceRoleForSupport-arn:aws:iam::aws:policy/aws-service-role/AWSSupportServiceRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForSupport
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AWSSupportServiceRolePolicy


AWSServiceRoleForTrustedAdvisor:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForTrustedAdvisor
  - name: AWSServiceRoleForTrustedAdvisor
  - arn: arn:aws:iam::123456789012:role/aws-service-role/trustedadvisor.amazonaws.com/AWSServiceRoleForTrustedAdvisor
  - id: AROAIJQ7FEPUSQL3T3YYM
  - path: /aws-service-role/trustedadvisor.amazonaws.com/
  - description: Access for the AWS Trusted Advisor Service to help reduce cost, increase
      performance, and improve security of your AWS environment.
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "trustedadvisor.amazonaws.com"}}], "Version":
      "2012-10-17"}'


AWSServiceRoleForTrustedAdvisor-arn:aws:iam::aws:policy/aws-service-role/AWSTrustedAdvisorServiceRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForTrustedAdvisor
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AWSTrustedAdvisorServiceRolePolicy


AWSServiceRoleForVPCTransitGateway:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForVPCTransitGateway
  - name: AWSServiceRoleForVPCTransitGateway
  - arn: arn:aws:iam::123456789012:role/aws-service-role/transitgateway.amazonaws.com/AWSServiceRoleForVPCTransitGateway
  - id: AROAX2FJ77DCVXXA2PYYX
  - path: /aws-service-role/transitgateway.amazonaws.com/
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "transitgateway.amazonaws.com"}}], "Version":
      "2012-10-17"}'


AWSServiceRoleForVPCTransitGateway-arn:aws:iam::aws:policy/aws-service-role/AWSVPCTransitGatewayServiceRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForVPCTransitGateway
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AWSVPCTransitGatewayServiceRolePolicy


Administrator:
  aws.iam.user.present:
  - name: Administrator
  - resource_id: Administrator
  - arn: arn:aws:iam::123456789012:user/Administrator
  - path: /
  - tags:
    - Key: Application
      Value: guardrails
  - user_name: Administrator


AmazonSageMaker-ExecutionRole-20180207T165162:
  aws.iam.role.present:
  - resource_id: AmazonSageMaker-ExecutionRole-20180207T165162
  - name: AmazonSageMaker-ExecutionRole-20180207T165162
  - arn: arn:aws:iam::123456789012:role/service-role/AmazonSageMaker-ExecutionRole-20180207T165162
  - id: AROAJUMA4U5Z4IPZSBSYG
  - path: /service-role/
  - description: SageMaker execution role created from the SageMaker AWS Management
      Console.
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "sagemaker.amazonaws.com"}}], "Version": "2012-10-17"}'


AmazonxyzEBSCSIRole:
  aws.iam.role.present:
  - resource_id: AmazonxyzEBSCSIRole
  - name: AmazonxyzEBSCSIRole
  - arn: arn:aws:iam::123456789012:role/AmazonxyzEBSCSIRole
  - id: AROAX2FJ77DCTHM2E6CSP
  - path: /
  - description: ''
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRoleWithWebIdentity",
      "Condition": {"StringEquals": {"oidc.xyz.us-east-2.amazonaws.com/id/D1EE3259ED98815196FA1D1A3E51C26A:aud":
      "sts.amazonaws.com"}}, "Effect": "Allow", "Principal": {"Federated": "arn:aws:iam::123456789012:oidc-provider/oidc.xyz.us-east-2.amazonaws.com/id/D1EE3259ED98815196FA1D1A3E51C26A"}}],
      "Version": "2012-10-17"}'


AmazonxyzEBSCSIRole-arn:aws:iam::123456789012:policy/Amazonxyz_EBS_CSI_Driver_Policy:
  aws.iam.role_policy_attachment.present:
  - role_name: AmazonxyzEBSCSIRole
  - policy_arn: arn:aws:iam::123456789012:policy/Amazonxyz_EBS_CSI_Driver_Policy


CloudHealth_Borathon:
  aws.iam.role.present:
  - resource_id: CloudHealth_Borathon
  - name: CloudHealth_Borathon
  - arn: arn:aws:iam::123456789012:role/CloudHealth_Borathon
  - id: AROAX2FJ77DC6M62R6UOP
  - path: /
  - description: CHT_CAS Borathon
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {"StringEquals": {"sts:ExternalId": "97c21f537f673e1419a4d11b882dad"}}, "Effect":
      "Allow", "Principal": {"AWS": "arn:aws:iam::454464851268:root"}}], "Version":
      "2012-10-17"}'


CloudHealth_Borathon-arn:aws:iam::123456789012:policy/CHTPolicy_Borathon:
  aws.iam.role_policy_attachment.present:
  - role_name: CloudHealth_Borathon
  - policy_arn: arn:aws:iam::123456789012:policy/CHTPolicy_Borathon


CloudTrail_CloudWatchLogs_Role:
  aws.iam.role.present:
  - resource_id: CloudTrail_CloudWatchLogs_Role
  - name: CloudTrail_CloudWatchLogs_Role
  - arn: arn:aws:iam::123456789012:role/CloudTrail_CloudWatchLogs_Role
  - id: AROAITPA5S7NF4PREISDM
  - path: /
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "cloudtrail.amazonaws.com"}, "Sid": ""}],
      "Version": "2012-10-17"}'


CloudTrail_CloudWatchLogs_Role-oneClick_CloudTrail_CloudWatchLogs_Role_1522065148468:
  aws.iam.role_policy.present:
  - resource_id: CloudTrail_CloudWatchLogs_Role-oneClick_CloudTrail_CloudWatchLogs_Role_1522065148468
  - role_name: CloudTrail_CloudWatchLogs_Role
  - name: oneClick_CloudTrail_CloudWatchLogs_Role_1522065148468
  - policy_document: '{"Statement": [{"Action": ["logs:CreateLogStream"], "Effect":
      "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:CloudTrail/DefaultLogGroup:log-stream:123456789012_CloudTrail_us-east-1*"],
      "Sid": "AWSCloudTrailCreateLogStream20141101"}, {"Action": ["logs:PutLogEvents"],
      "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:CloudTrail/DefaultLogGroup:log-stream:123456789012_CloudTrail_us-east-1*"],
      "Sid": "AWSCloudTrailPutLogEvents20141101"}], "Version": "2012-10-17"}'


CrossAccountSign:
  aws.iam.role.present:
  - resource_id: CrossAccountSign
  - name: CrossAccountSign
  - arn: arn:aws:iam::123456789012:role/CrossAccountSign
  - id: AROAIBW7XSQDB6LTDCF6K
  - path: /
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {}, "Effect": "Allow", "Principal": {"AWS": "arn:aws:iam::565541806941:root"}}],
      "Version": "2012-10-17"}'


CrossAccountSign-arn:aws:iam::aws:policy/AdministratorAccess:
  aws.iam.role_policy_attachment.present:
  - role_name: CrossAccountSign
  - policy_arn: arn:aws:iam::aws:policy/AdministratorAccess


Executelambda:
  aws.iam.role.present:
  - resource_id: Executelambda
  - name: Executelambda
  - arn: arn:aws:iam::123456789012:role/Executelambda
  - id: AROAIYDNOY3U7VXI3ZMSG
  - path: /
  - description: Allows Lambda functions to call AWS services on your behalf.
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "lambda.amazonaws.com"}}], "Version": "2012-10-17"}'


Executelambda-arn:aws:iam::123456789012:policy/EC2ListResources:
  aws.iam.role_policy_attachment.present:
  - role_name: Executelambda
  - policy_arn: arn:aws:iam::123456789012:policy/EC2ListResources


Executelambda-arn:aws:iam::aws:policy/AWSLambdaExecute:
  aws.iam.role_policy_attachment.present:
  - role_name: Executelambda
  - policy_arn: arn:aws:iam::aws:policy/AWSLambdaExecute


Executelambda-arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole:
  aws.iam.role_policy_attachment.present:
  - role_name: Executelambda
  - policy_arn: arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole


Fori18ntest_+=,.@-:
  aws.iam.role.present:
  - resource_id: Fori18ntest_+=,.@-
  - name: Fori18ntest_+=,.@-
  - arn: arn:aws:iam::123456789012:role/Fori18ntest_+=,.@-
  - id: AROAILCLFKW7MRBX5LTPI
  - path: /
  - description: Fori18ntest_+=,.@-
  - max_session_duration: 3600
  - tags:
    - Key: Fori18ntest _.:/=+-@
      Value: Fori18ntest _.:/=+-@
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {"StringEquals": {"sts:ExternalId": "bd57bfa55a312f557abc545ad12a94e3b456ca7bbf3a3b0c71cbcf13b23c13ec"}},
      "Effect": "Allow", "Principal": {"AWS": "arn:aws:iam::993194883101:root"}}],
      "Version": "2012-10-17"}'


Fori18ntest_+=,.@--arn:aws:iam::123456789012:policy/AmazonFori18ntest+=,.@-_:
  aws.iam.role_policy_attachment.present:
  - role_name: Fori18ntest_+=,.@-
  - policy_arn: arn:aws:iam::123456789012:policy/AmazonFori18ntest+=,.@-_


Idem_iam_vj_test:
  aws.iam.role.present:
  - resource_id: Idem_iam_vj_test
  - name: Idem_iam_vj_test
  - arn: arn:aws:iam::123456789012:role/Idem_iam_vj_test
  - id: AROAX2FJ77DCZLZEGFD6D
  - path: /
  - description: Allows access to other AWS service resources that are required to
      operate clusters managed by xyz.
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "xyz.amazonaws.com"}}, {"Action": "sts:AssumeRole",
      "Effect": "Allow", "Principal": {"Service": "ec2.amazonaws.com"}}], "Version":
      "2012-10-17"}'


Idem_iam_vj_test-arn:aws:iam::aws:policy/AmazonxyzClusterPolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: Idem_iam_vj_test
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzClusterPolicy


Idem_iam_vj_test-arn:aws:iam::aws:policy/AmazonxyzVPCResourceController:
  aws.iam.role_policy_attachment.present:
  - role_name: Idem_iam_vj_test
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzVPCResourceController


KK+=HAHA:
  aws.iam.role.present:
  - resource_id: KK+=HAHA
  - name: KK+=HAHA
  - arn: arn:aws:iam::123456789012:role/KK+=HAHA
  - id: AROAIX3RRKRKSQWD4MC7O
  - path: /
  - description: KK DE LINGDE
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {}, "Effect": "Allow", "Principal": {"AWS": "arn:aws:iam::993194883101:root"}}],
      "Version": "2012-10-17"}'


KK+=HAHA-arn:aws:iam::123456789012:policy/KK=+WW_EES:
  aws.iam.role_policy_attachment.present:
  - role_name: KK+=HAHA
  - policy_arn: arn:aws:iam::123456789012:policy/KK=+WW_EES


MyAppAdmin:
  aws.iam.role.present:
  - resource_id: MyAppAdmin
  - name: MyAppAdmin
  - arn: arn:aws:iam::123456789012:role/service-role/MyAppAdmin
  - id: AROAX2FJ77DCQB45AGYRJ
  - path: /service-role/
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "lambda.amazonaws.com"}}], "Version": "2012-10-17"}'


MyAppAdmin-arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-c5399330-733d-477d-b360-10358e90ba73:
  aws.iam.role_policy_attachment.present:
  - role_name: MyAppAdmin
  - policy_arn: arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-c5399330-733d-477d-b360-10358e90ba73


MyAppAdmin-arn:aws:iam::aws:policy/service-role/AWSConfigRulesExecutionRole:
  aws.iam.role_policy_attachment.present:
  - role_name: MyAppAdmin
  - policy_arn: arn:aws:iam::aws:policy/service-role/AWSConfigRulesExecutionRole


PermissionsAdmin:
  aws.iam.user.present:
  - name: PermissionsAdmin
  - resource_id: PermissionsAdmin
  - arn: arn:aws:iam::123456789012:user/PermissionsAdmin
  - path: /
  - user_name: PermissionsAdmin


Prelude_test01:
  aws.iam.user.present:
  - name: Prelude_test01
  - resource_id: Prelude_test01
  - arn: arn:aws:iam::123456789012:user/Prelude_test01
  - path: /
  - user_name: Prelude_test01


ReadOnlyEC2:
  aws.iam.role.present:
  - resource_id: ReadOnlyEC2
  - name: ReadOnlyEC2
  - arn: arn:aws:iam::123456789012:role/ReadOnlyEC2
  - id: AROAIXXNNOS7CQ6EFBDSO
  - path: /
  - description: Used for testing purposes.
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {"ForAnyValue:StringEquals": {"sts:ExternalId": ["prelude_test_external_id",
      "permissionstest"]}}, "Effect": "Allow", "Principal": {"AWS": ["AIDAX2FJ77DCWZQKD5MOL",
      "arn:aws:iam::746014882121:root"]}}], "Version": "2012-10-17"}'


ReadOnlyEC2-arn:aws:iam::aws:policy/AmazonEC2ReadOnlyAccess:
  aws.iam.role_policy_attachment.present:
  - role_name: ReadOnlyEC2
  - policy_arn: arn:aws:iam::aws:policy/AmazonEC2ReadOnlyAccess


S3BucketAccess:
  aws.iam.role.present:
  - resource_id: S3BucketAccess
  - name: S3BucketAccess
  - arn: arn:aws:iam::123456789012:role/S3BucketAccess
  - id: AROAX2FJ77DCSWZPLWBKN
  - path: /
  - description: Allows EC2 instances to call AWS services on your behalf.
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "ec2.amazonaws.com"}}], "Version": "2012-10-17"}'


S3BucketAccess-arn:aws:iam::aws:policy/AmazonS3FullAccess:
  aws.iam.role_policy_attachment.present:
  - role_name: S3BucketAccess
  - policy_arn: arn:aws:iam::aws:policy/AmazonS3FullAccess


SecureState:
  aws.iam.role.present:
  - resource_id: SecureState
  - name: SecureState
  - arn: arn:aws:iam::123456789012:role/SecureState
  - id: AROAX2FJ77DC4JNHFZVT3
  - path: /
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {"StringEquals": {"sts:ExternalId": "pWvdXbAberZWiAjlBowqJyhSvgzlJxjv"}}, "Effect":
      "Allow", "Principal": {"AWS": "arn:aws:iam::530342348278:root"}}], "Version":
      "2012-10-17"}'


SecureState-arn:aws:iam::aws:policy/SecurityAudit:
  aws.iam.role_policy_attachment.present:
  - role_name: SecureState
  - policy_arn: arn:aws:iam::aws:policy/SecurityAudit


SecureStateRole:
  aws.iam.role.present:
  - resource_id: SecureStateRole
  - name: SecureStateRole
  - arn: arn:aws:iam::123456789012:role/SecureStateRole
  - id: AROAX2FJ77DCRG33VYAJA
  - path: /
  - description: ''
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {}, "Effect": "Allow", "Principal": {"AWS": "arn:aws:iam::993194883101:root"}}],
      "Version": "2012-10-17"}'


SecureStateRole-arn:aws:iam::aws:policy/SecurityAudit:
  aws.iam.role_policy_attachment.present:
  - role_name: SecureStateRole
  - policy_arn: arn:aws:iam::aws:policy/SecurityAudit


StatesExecutionRole:
  aws.iam.role.present:
  - resource_id: StatesExecutionRole
  - name: StatesExecutionRole
  - arn: arn:aws:iam::123456789012:role/service-role/StatesExecutionRole
  - id: AROAJUP7YTEZSKB3AX4LW
  - path: /service-role/
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "states.amazonaws.com"}}], "Version": "2012-10-17"}'


StatesExecutionRole-arn:aws:iam::123456789012:policy/service-role/StatesExecutionPolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: StatesExecutionRole
  - policy_arn: arn:aws:iam::123456789012:policy/service-role/StatesExecutionPolicy


VMWMasterReadOnlyRole:
  aws.iam.role.present:
  - resource_id: VMWMasterReadOnlyRole
  - name: VMWMasterReadOnlyRole
  - arn: arn:aws:iam::123456789012:role/VMWMasterReadOnlyRole
  - id: AROAX2FJ77DCRWKPLXQCL
  - path: /
  - description: Read Only Role for the Master Payer Account
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"AWS": "arn:aws:iam::116462199383:root"}}], "Version":
      "2012-10-17"}'


VMWMasterReadOnlyRole-arn:aws:iam::aws:policy/ReadOnlyAccess:
  aws.iam.role_policy_attachment.present:
  - role_name: VMWMasterReadOnlyRole
  - policy_arn: arn:aws:iam::aws:policy/ReadOnlyAccess


VMWMasterRole:
  aws.iam.role.present:
  - resource_id: VMWMasterRole
  - name: VMWMasterRole
  - arn: arn:aws:iam::123456789012:role/VMWMasterRole
  - id: AROAJ5RG24UTE63G4RT56
  - path: /
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"AWS": "arn:aws:iam::116462199383:root"}}], "Version":
      "2012-10-17"}'


VMWMasterRole-AdministratorAccess:
  aws.iam.role_policy.present:
  - resource_id: VMWMasterRole-AdministratorAccess
  - role_name: VMWMasterRole
  - name: AdministratorAccess
  - policy_document: '{"Statement": [{"Action": "*", "Effect": "Allow", "Resource":
      "*"}], "Version": "2012-10-17"}'


Wavefront:
  aws.iam.role.present:
  - resource_id: Wavefront
  - name: Wavefront
  - arn: arn:aws:iam::123456789012:role/Wavefront
  - id: AROAJTKVFVMV32QSDVUOW
  - path: /
  - description: Wavefront
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {"StringEquals": {"sts:ExternalId": "waveb24f6af3"}}, "Effect": "Allow", "Principal":
      {"AWS": "arn:aws:iam::301213811993:root"}}], "Version": "2012-10-17"}'


Wavefront-arn:aws:iam::aws:policy/ReadOnlyAccess:
  aws.iam.role_policy_attachment.present:
  - role_name: Wavefront
  - policy_arn: arn:aws:iam::aws:policy/ReadOnlyAccess


abc-sddc-formation-56609b0b-e-RemoteRoleService-1KI0QMQM9WU0I:
  aws.iam.role.present:
  - resource_id: abc-sddc-formation-56609b0b-e-RemoteRoleService-1KI0QMQM9WU0I
  - name: abc-sddc-formation-56609b0b-e-RemoteRoleService-1KI0QMQM9WU0I
  - arn: arn:aws:iam::123456789012:role/abc-sddc-formation-56609b0b-e-RemoteRoleService-1KI0QMQM9WU0I
  - id: AROAX2FJ77DCYXGCA5DNA
  - path: /
  - description: ''
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {"StringEquals": {"sts:ExternalId": "abc-vmc"}}, "Effect": "Allow", "Principal":
      {"AWS": "arn:aws:iam::347624956669:root"}}], "Version": "2012-10-17"}'


? abc-sddc-formation-56609b0b-e-RemoteRoleService-1KI0QMQM9WU0I-arn:aws:iam::aws:policy/AmazonVPCCrossAccountNetworkInterfaceOperations


: aws.iam.role_policy_attachment.present:
  - role_name: abc-sddc-formation-56609b0b-e-RemoteRoleService-1KI0QMQM9WU0I
  - policy_arn: arn:aws:iam::aws:policy/AmazonVPCCrossAccountNetworkInterfaceOperations


abc-sddc-formation-56609b0b-e79-BasicLambdaRole-1G3NFJ9EKTXD:
  aws.iam.role.present:
  - resource_id: abc-sddc-formation-56609b0b-e79-BasicLambdaRole-1G3NFJ9EKTXD
  - name: abc-sddc-formation-56609b0b-e79-BasicLambdaRole-1G3NFJ9EKTXD
  - arn: arn:aws:iam::123456789012:role/abc-sddc-formation-56609b0b-e79-BasicLambdaRole-1G3NFJ9EKTXD
  - id: AROAX2FJ77DC3KEAINRQT
  - path: /
  - description: ''
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "lambda.amazonaws.com"}}], "Version": "2012-10-17"}'


abc-sddc-formation-56609b0b-e79-BasicLambdaRole-1G3NFJ9EKTXD-ExecRolePolicy:
  aws.iam.role_policy.present:
  - resource_id: abc-sddc-formation-56609b0b-e79-BasicLambdaRole-1G3NFJ9EKTXD-ExecRolePolicy
  - role_name: abc-sddc-formation-56609b0b-e79-BasicLambdaRole-1G3NFJ9EKTXD
  - name: ExecRolePolicy
  - policy_document: '{"Statement": [{"Action": ["logs:*"], "Effect": "Allow", "Resource":
      "arn:aws:logs:*:*:*"}], "Version": "2012-10-17"}'


abc-sddc-formation-56609b0b-e79-BasicLambdaRole-1G3NFJ9EKTXD-root:
  aws.iam.role_policy.present:
  - resource_id: abc-sddc-formation-56609b0b-e79-BasicLambdaRole-1G3NFJ9EKTXD-root
  - role_name: abc-sddc-formation-56609b0b-e79-BasicLambdaRole-1G3NFJ9EKTXD
  - name: root
  - policy_document: '{"Statement": [{"Action": ["logs:CreateLogGroup", "logs:CreateLogStream",
      "logs:PutLogEvents"], "Effect": "Allow", "Resource": "arn:aws:logs:*:*:*"},
      {"Action": ["ec2:DescribeVpcs", "ec2:DescribeRouteTables", "ec2:DescribeSubnets"],
      "Effect": "Allow", "Resource": "*"}], "Version": "2012-10-17"}'


abc-sddc-formation-56609b0b-e79-RemoteRolePayer-233XBH8AHXBL:
  aws.iam.role.present:
  - resource_id: abc-sddc-formation-56609b0b-e79-RemoteRolePayer-233XBH8AHXBL
  - name: abc-sddc-formation-56609b0b-e79-RemoteRolePayer-233XBH8AHXBL
  - arn: arn:aws:iam::123456789012:role/abc-sddc-formation-56609b0b-e79-RemoteRolePayer-233XBH8AHXBL
  - id: AROAX2FJ77DC56PYYNOTN
  - path: /
  - description: ''
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {"StringEquals": {"sts:ExternalId": "abc-vmc"}}, "Effect": "Allow", "Principal":
      {"AWS": "arn:aws:iam::909992878262:root"}}], "Version": "2012-10-17"}'


? abc-sddc-formation-56609b0b-e79-RemoteRolePayer-233XBH8AHXBL-arn:aws:iam::aws:policy/AmazonVPCCrossAccountNetworkInterfaceOperations


: aws.iam.role_policy_attachment.present:
  - role_name: abc-sddc-formation-56609b0b-e79-RemoteRolePayer-233XBH8AHXBL
  - policy_arn: arn:aws:iam::aws:policy/AmazonVPCCrossAccountNetworkInterfaceOperations


abc-sddc-formation-56609b0b-e79d-43e-RemoteRole-H1XZTJBVZ0CW:
  aws.iam.role.present:
  - resource_id: abc-sddc-formation-56609b0b-e79d-43e-RemoteRole-H1XZTJBVZ0CW
  - name: abc-sddc-formation-56609b0b-e79d-43e-RemoteRole-H1XZTJBVZ0CW
  - arn: arn:aws:iam::123456789012:role/abc-sddc-formation-56609b0b-e79d-43e-RemoteRole-H1XZTJBVZ0CW
  - id: AROAX2FJ77DCT7ZABH5WM
  - path: /
  - description: ''
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {"StringEquals": {"sts:ExternalId": "abc-vmc"}}, "Effect": "Allow", "Principal":
      {"AWS": "arn:aws:iam::083910886865:root"}}], "Version": "2012-10-17"}'


? abc-sddc-formation-56609b0b-e79d-43e-RemoteRole-H1XZTJBVZ0CW-arn:aws:iam::aws:policy/AmazonVPCCrossAccountNetworkInterfaceOperations


: aws.iam.role_policy_attachment.present:
  - role_name: abc-sddc-formation-56609b0b-e79d-43e-RemoteRole-H1XZTJBVZ0CW
  - policy_arn: arn:aws:iam::aws:policy/AmazonVPCCrossAccountNetworkInterfaceOperations


abc-sddc-formation-d6763282-9-RemoteRoleService-1CK3UIHJIVFGX:
  aws.iam.role.present:
  - resource_id: abc-sddc-formation-d6763282-9-RemoteRoleService-1CK3UIHJIVFGX
  - name: abc-sddc-formation-d6763282-9-RemoteRoleService-1CK3UIHJIVFGX
  - arn: arn:aws:iam::123456789012:role/abc-sddc-formation-d6763282-9-RemoteRoleService-1CK3UIHJIVFGX
  - id: AROAX2FJ77DC4AALNKS5P
  - path: /
  - description: ''
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {"StringEquals": {"sts:ExternalId": "abc-vmc"}}, "Effect": "Allow", "Principal":
      {"AWS": "arn:aws:iam::347624956669:root"}}], "Version": "2012-10-17"}'


? abc-sddc-formation-d6763282-9-RemoteRoleService-1CK3UIHJIVFGX-arn:aws:iam::aws:policy/AmazonVPCCrossAccountNetworkInterfaceOperations


: aws.iam.role_policy_attachment.present:
  - role_name: abc-sddc-formation-d6763282-9-RemoteRoleService-1CK3UIHJIVFGX
  - policy_arn: arn:aws:iam::aws:policy/AmazonVPCCrossAccountNetworkInterfaceOperations


abc-sddc-formation-d6763282-9ad-BasicLambdaRole-1LRYHQO6697D6:
  aws.iam.role.present:
  - resource_id: abc-sddc-formation-d6763282-9ad-BasicLambdaRole-1LRYHQO6697D6
  - name: abc-sddc-formation-d6763282-9ad-BasicLambdaRole-1LRYHQO6697D6
  - arn: arn:aws:iam::123456789012:role/abc-sddc-formation-d6763282-9ad-BasicLambdaRole-1LRYHQO6697D6
  - id: AROAX2FJ77DCSVRRFFAO2
  - path: /
  - description: ''
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "lambda.amazonaws.com"}}], "Version": "2012-10-17"}'


abc-sddc-formation-d6763282-9ad-BasicLambdaRole-1LRYHQO6697D6-ExecRolePolicy:
  aws.iam.role_policy.present:
  - resource_id: abc-sddc-formation-d6763282-9ad-BasicLambdaRole-1LRYHQO6697D6-ExecRolePolicy
  - role_name: abc-sddc-formation-d6763282-9ad-BasicLambdaRole-1LRYHQO6697D6
  - name: ExecRolePolicy
  - policy_document: '{"Statement": [{"Action": ["logs:*"], "Effect": "Allow", "Resource":
      "arn:aws:logs:*:*:*"}], "Version": "2012-10-17"}'


abc-sddc-formation-d6763282-9ad-BasicLambdaRole-1LRYHQO6697D6-root:
  aws.iam.role_policy.present:
  - resource_id: abc-sddc-formation-d6763282-9ad-BasicLambdaRole-1LRYHQO6697D6-root
  - role_name: abc-sddc-formation-d6763282-9ad-BasicLambdaRole-1LRYHQO6697D6
  - name: root
  - policy_document: '{"Statement": [{"Action": ["logs:CreateLogGroup", "logs:CreateLogStream",
      "logs:PutLogEvents"], "Effect": "Allow", "Resource": "arn:aws:logs:*:*:*"},
      {"Action": ["ec2:DescribeVpcs", "ec2:DescribeRouteTables", "ec2:DescribeSubnets"],
      "Effect": "Allow", "Resource": "*"}], "Version": "2012-10-17"}'


abc-sddc-formation-d6763282-9ad-RemoteRolePayer-YVFOCVMW6N3T:
  aws.iam.role.present:
  - resource_id: abc-sddc-formation-d6763282-9ad-RemoteRolePayer-YVFOCVMW6N3T
  - name: abc-sddc-formation-d6763282-9ad-RemoteRolePayer-YVFOCVMW6N3T
  - arn: arn:aws:iam::123456789012:role/abc-sddc-formation-d6763282-9ad-RemoteRolePayer-YVFOCVMW6N3T
  - id: AROAX2FJ77DCRMYV7SWVZ
  - path: /
  - description: ''
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {"StringEquals": {"sts:ExternalId": "abc-vmc"}}, "Effect": "Allow", "Principal":
      {"AWS": "arn:aws:iam::909992878262:root"}}], "Version": "2012-10-17"}'


? abc-sddc-formation-d6763282-9ad-RemoteRolePayer-YVFOCVMW6N3T-arn:aws:iam::aws:policy/AmazonVPCCrossAccountNetworkInterfaceOperations


: aws.iam.role_policy_attachment.present:
  - role_name: abc-sddc-formation-d6763282-9ad-RemoteRolePayer-YVFOCVMW6N3T
  - policy_arn: arn:aws:iam::aws:policy/AmazonVPCCrossAccountNetworkInterfaceOperations


abc-sddc-formation-d6763282-9adc-4e1-RemoteRole-5YTFUM6RD1E8:
  aws.iam.role.present:
  - resource_id: abc-sddc-formation-d6763282-9adc-4e1-RemoteRole-5YTFUM6RD1E8
  - name: abc-sddc-formation-d6763282-9adc-4e1-RemoteRole-5YTFUM6RD1E8
  - arn: arn:aws:iam::123456789012:role/abc-sddc-formation-d6763282-9adc-4e1-RemoteRole-5YTFUM6RD1E8
  - id: AROAX2FJ77DC4SEMMXRG7
  - path: /
  - description: ''
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {"StringEquals": {"sts:ExternalId": "abc-vmc"}}, "Effect": "Allow", "Principal":
      {"AWS": "arn:aws:iam::557453615640:root"}}], "Version": "2012-10-17"}'


? abc-sddc-formation-d6763282-9adc-4e1-RemoteRole-5YTFUM6RD1E8-arn:aws:iam::aws:policy/AmazonVPCCrossAccountNetworkInterfaceOperations


: aws.iam.role_policy_attachment.present:
  - role_name: abc-sddc-formation-d6763282-9adc-4e1-RemoteRole-5YTFUM6RD1E8
  - policy_arn: arn:aws:iam::aws:policy/AmazonVPCCrossAccountNetworkInterfaceOperations


abx_vfunctions:
  aws.iam.user.present:
  - name: abx_vfunctions
  - resource_id: abx_vfunctions
  - arn: arn:aws:iam::123456789012:user/abx_vfunctions
  - path: /
  - user_name: abx_vfunctions


afilipov:
  aws.iam.role.present:
  - resource_id: afilipov
  - name: afilipov
  - arn: arn:aws:iam::123456789012:role/afilipov
  - id: AROAX2FJ77DC3KYL746XR
  - path: /
  - description: Allows Lambda functions to call AWS services on your behalf.
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "lambda.amazonaws.com"}}], "Version": "2012-10-17"}'


afilipov-arn:aws:iam::123456789012:policy/AllowEC2ElasticIPReadWrite:
  aws.iam.role_policy_attachment.present:
  - role_name: afilipov
  - policy_arn: arn:aws:iam::123456789012:policy/AllowEC2ElasticIPReadWrite


afilipov-arn:aws:iam::aws:policy/AWSLambdaFullAccess:
  aws.iam.role_policy_attachment.present:
  - role_name: afilipov
  - policy_arn: arn:aws:iam::aws:policy/AWSLambdaFullAccess


afilipov-arn:aws:iam::aws:policy/AdministratorAccess:
  aws.iam.role_policy_attachment.present:
  - role_name: afilipov
  - policy_arn: arn:aws:iam::aws:policy/AdministratorAccess


afilipov-arn:aws:iam::aws:policy/AdministratorAccess-Amplify:
  aws.iam.role_policy_attachment.present:
  - role_name: afilipov
  - policy_arn: arn:aws:iam::aws:policy/AdministratorAccess-Amplify


afilipov-arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryFullAccess:
  aws.iam.role_policy_attachment.present:
  - role_name: afilipov
  - policy_arn: arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryFullAccess


afilipov-arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly:
  aws.iam.role_policy_attachment.present:
  - role_name: afilipov
  - policy_arn: arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly


afilipov-arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole:
  aws.iam.role_policy_attachment.present:
  - role_name: afilipov
  - policy_arn: arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole


akshay_test:
  aws.iam.user.present:
  - name: akshay_test
  - resource_id: akshay_test
  - arn: arn:aws:iam::123456789012:user/akshay_test
  - path: /
  - user_name: akshay_test


apigateway-sqs-access-role:
  aws.iam.role.present:
  - resource_id: apigateway-sqs-access-role
  - name: apigateway-sqs-access-role
  - arn: arn:aws:iam::123456789012:role/apigateway-sqs-access-role
  - id: AROAIYBMR7BDONKSJ4OWY
  - path: /
  - description: Allows API Gateway to monitor azure events.
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "apigateway.amazonaws.com"}, "Sid": ""}],
      "Version": "2012-10-17"}'


apigateway-sqs-access-role-arn:aws:iam::123456789012:policy/AzureEventQueuePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: apigateway-sqs-access-role
  - policy_arn: arn:aws:iam::123456789012:policy/AzureEventQueuePolicy


apigateway-sqs-access-role-arn:aws:iam::aws:policy/service-role/AmazonAPIGatewayPushToCloudWatchLogs:
  aws.iam.role_policy_attachment.present:
  - role_name: apigateway-sqs-access-role
  - policy_arn: arn:aws:iam::aws:policy/service-role/AmazonAPIGatewayPushToCloudWatchLogs


arn:aws:iam::123456789012:policy/ALBIngressControllerIAMPolicy:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["acm:DescribeCertificate", "acm:ListCertificates",
      "acm:GetCertificate"], "Effect": "Allow", "Resource": "*"}, {"Action": ["ec2:AuthorizeSecurityGroupIngress",
      "ec2:CreateSecurityGroup", "ec2:CreateTags", "ec2:DeleteTags", "ec2:DeleteSecurityGroup",
      "ec2:DescribeAccountAttributes", "ec2:DescribeAddresses", "ec2:DescribeInstances",
      "ec2:DescribeInstanceStatus", "ec2:DescribeInternetGateways", "ec2:DescribeNetworkInterfaces",
      "ec2:DescribeSecurityGroups", "ec2:DescribeSubnets", "ec2:DescribeTags", "ec2:DescribeVpcs",
      "ec2:ModifyInstanceAttribute", "ec2:ModifyNetworkInterfaceAttribute", "ec2:RevokeSecurityGroupIngress"],
      "Effect": "Allow", "Resource": "*"}, {"Action": ["elasticloadbalancing:AddListenerCertificates",
      "elasticloadbalancing:AddTags", "elasticloadbalancing:CreateListener", "elasticloadbalancing:CreateLoadBalancer",
      "elasticloadbalancing:CreateRule", "elasticloadbalancing:CreateTargetGroup",
      "elasticloadbalancing:DeleteListener", "elasticloadbalancing:DeleteLoadBalancer",
      "elasticloadbalancing:DeleteRule", "elasticloadbalancing:DeleteTargetGroup",
      "elasticloadbalancing:DeregisterTargets", "elasticloadbalancing:DescribeListenerCertificates",
      "elasticloadbalancing:DescribeListeners", "elasticloadbalancing:DescribeLoadBalancers",
      "elasticloadbalancing:DescribeLoadBalancerAttributes", "elasticloadbalancing:DescribeRules",
      "elasticloadbalancing:DescribeSSLPolicies", "elasticloadbalancing:DescribeTags",
      "elasticloadbalancing:DescribeTargetGroups", "elasticloadbalancing:DescribeTargetGroupAttributes",
      "elasticloadbalancing:DescribeTargetHealth", "elasticloadbalancing:ModifyListener",
      "elasticloadbalancing:ModifyLoadBalancerAttributes", "elasticloadbalancing:ModifyRule",
      "elasticloadbalancing:ModifyTargetGroup", "elasticloadbalancing:ModifyTargetGroupAttributes",
      "elasticloadbalancing:RegisterTargets", "elasticloadbalancing:RemoveListenerCertificates",
      "elasticloadbalancing:RemoveTags", "elasticloadbalancing:SetIpAddressType",
      "elasticloadbalancing:SetSecurityGroups", "elasticloadbalancing:SetSubnets",
      "elasticloadbalancing:SetWebACL"], "Effect": "Allow", "Resource": "*"}, {"Action":
      ["iam:CreateServiceLinkedRole", "iam:GetServerCertificate", "iam:ListServerCertificates"],
      "Effect": "Allow", "Resource": "*"}, {"Action": ["cognito-idp:DescribeUserPoolClient"],
      "Effect": "Allow", "Resource": "*"}, {"Action": ["waf-regional:GetWebACLForResource",
      "waf-regional:GetWebACL", "waf-regional:AssociateWebACL", "waf-regional:DisassociateWebACL"],
      "Effect": "Allow", "Resource": "*"}, {"Action": ["tag:GetResources", "tag:TagResources"],
      "Effect": "Allow", "Resource": "*"}, {"Action": ["waf:GetWebACL"], "Effect":
      "Allow", "Resource": "*"}], "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: ALBIngressControllerIAMPolicy
  - resource_id: arn:aws:iam::123456789012:policy/ALBIngressControllerIAMPolicy
  - id: ANPAX2FJ77DC6VVN4M7W5
  - path: /


arn:aws:iam::123456789012:policy/AWSLoadBalancerControllerAdditionalIAMPolicy:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["ec2:CreateTags", "ec2:DeleteTags"],
      "Condition": {"Null": {"aws:ResourceTag/ingress.k8s.aws/cluster": "false"}},
      "Effect": "Allow", "Resource": "arn:aws:ec2:*:*:security-group/*"}, {"Action":
      ["elasticloadbalancing:AddTags", "elasticloadbalancing:RemoveTags", "elasticloadbalancing:DeleteTargetGroup"],
      "Condition": {"Null": {"aws:ResourceTag/ingress.k8s.aws/cluster": "false"}},
      "Effect": "Allow", "Resource": ["arn:aws:elasticloadbalancing:*:*:targetgroup/*/*",
      "arn:aws:elasticloadbalancing:*:*:loadbalancer/net/*/*", "arn:aws:elasticloadbalancing:*:*:loadbalancer/app/*/*"]}],
      "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: AWSLoadBalancerControllerAdditionalIAMPolicy
  - resource_id: arn:aws:iam::123456789012:policy/AWSLoadBalancerControllerAdditionalIAMPolicy
  - id: ANPAX2FJ77DCRUTGRW5FW
  - path: /


arn:aws:iam::123456789012:policy/AWSLoadBalancerControllerIAMPolicy:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": "iam:CreateServiceLinkedRole", "Condition":
      {"StringEquals": {"iam:AWSServiceName": "elasticloadbalancing.amazonaws.com"}},
      "Effect": "Allow", "Resource": "*"}, {"Action": ["ec2:DescribeAccountAttributes",
      "ec2:DescribeAddresses", "ec2:DescribeAvailabilityZones", "ec2:DescribeInternetGateways",
      "ec2:DescribeVpcs", "ec2:DescribeVpcPeeringConnections", "ec2:DescribeSubnets",
      "ec2:DescribeSecurityGroups", "ec2:DescribeInstances", "ec2:DescribeNetworkInterfaces",
      "ec2:DescribeTags", "ec2:GetCoipPoolUsage", "ec2:DescribeCoipPools", "elasticloadbalancing:DescribeLoadBalancers",
      "elasticloadbalancing:DescribeLoadBalancerAttributes", "elasticloadbalancing:DescribeListeners",
      "elasticloadbalancing:DescribeListenerCertificates", "elasticloadbalancing:DescribeSSLPolicies",
      "elasticloadbalancing:DescribeRules", "elasticloadbalancing:DescribeTargetGroups",
      "elasticloadbalancing:DescribeTargetGroupAttributes", "elasticloadbalancing:DescribeTargetHealth",
      "elasticloadbalancing:DescribeTags"], "Effect": "Allow", "Resource": "*"}, {"Action":
      ["cognito-idp:DescribeUserPoolClient", "acm:ListCertificates", "acm:DescribeCertificate",
      "iam:ListServerCertificates", "iam:GetServerCertificate", "waf-regional:GetWebACL",
      "waf-regional:GetWebACLForResource", "waf-regional:AssociateWebACL", "waf-regional:DisassociateWebACL",
      "wafv2:GetWebACL", "wafv2:GetWebACLForResource", "wafv2:AssociateWebACL", "wafv2:DisassociateWebACL",
      "shield:GetSubscriptionState", "shield:DescribeProtection", "shield:CreateProtection",
      "shield:DeleteProtection"], "Effect": "Allow", "Resource": "*"}, {"Action":
      ["ec2:AuthorizeSecurityGroupIngress", "ec2:RevokeSecurityGroupIngress"], "Effect":
      "Allow", "Resource": "*"}, {"Action": ["ec2:CreateSecurityGroup"], "Effect":
      "Allow", "Resource": "*"}, {"Action": ["ec2:CreateTags"], "Condition": {"Null":
      {"aws:RequestTag/elbv2.k8s.aws/cluster": "false"}, "StringEquals": {"ec2:CreateAction":
      "CreateSecurityGroup"}}, "Effect": "Allow", "Resource": "arn:aws:ec2:*:*:security-group/*"},
      {"Action": ["ec2:CreateTags", "ec2:DeleteTags"], "Condition": {"Null": {"aws:RequestTag/elbv2.k8s.aws/cluster":
      "true", "aws:ResourceTag/elbv2.k8s.aws/cluster": "false"}}, "Effect": "Allow",
      "Resource": "arn:aws:ec2:*:*:security-group/*"}, {"Action": ["ec2:AuthorizeSecurityGroupIngress",
      "ec2:RevokeSecurityGroupIngress", "ec2:DeleteSecurityGroup"], "Condition": {"Null":
      {"aws:ResourceTag/elbv2.k8s.aws/cluster": "false"}}, "Effect": "Allow", "Resource":
      "*"}, {"Action": ["elasticloadbalancing:CreateLoadBalancer", "elasticloadbalancing:CreateTargetGroup"],
      "Condition": {"Null": {"aws:RequestTag/elbv2.k8s.aws/cluster": "false"}}, "Effect":
      "Allow", "Resource": "*"}, {"Action": ["elasticloadbalancing:CreateListener",
      "elasticloadbalancing:DeleteListener", "elasticloadbalancing:CreateRule", "elasticloadbalancing:DeleteRule"],
      "Effect": "Allow", "Resource": "*"}, {"Action": ["elasticloadbalancing:AddTags",
      "elasticloadbalancing:RemoveTags"], "Condition": {"Null": {"aws:RequestTag/elbv2.k8s.aws/cluster":
      "true", "aws:ResourceTag/elbv2.k8s.aws/cluster": "false"}}, "Effect": "Allow",
      "Resource": ["arn:aws:elasticloadbalancing:*:*:targetgroup/*/*", "arn:aws:elasticloadbalancing:*:*:loadbalancer/net/*/*",
      "arn:aws:elasticloadbalancing:*:*:loadbalancer/app/*/*"]}, {"Action": ["elasticloadbalancing:AddTags",
      "elasticloadbalancing:RemoveTags"], "Effect": "Allow", "Resource": ["arn:aws:elasticloadbalancing:*:*:listener/net/*/*/*",
      "arn:aws:elasticloadbalancing:*:*:listener/app/*/*/*", "arn:aws:elasticloadbalancing:*:*:listener-rule/net/*/*/*",
      "arn:aws:elasticloadbalancing:*:*:listener-rule/app/*/*/*"]}, {"Action": ["elasticloadbalancing:ModifyLoadBalancerAttributes",
      "elasticloadbalancing:SetIpAddressType", "elasticloadbalancing:SetSecurityGroups",
      "elasticloadbalancing:SetSubnets", "elasticloadbalancing:DeleteLoadBalancer",
      "elasticloadbalancing:ModifyTargetGroup", "elasticloadbalancing:ModifyTargetGroupAttributes",
      "elasticloadbalancing:DeleteTargetGroup"], "Condition": {"Null": {"aws:ResourceTag/elbv2.k8s.aws/cluster":
      "false"}}, "Effect": "Allow", "Resource": "*"}, {"Action": ["elasticloadbalancing:RegisterTargets",
      "elasticloadbalancing:DeregisterTargets"], "Effect": "Allow", "Resource": "arn:aws:elasticloadbalancing:*:*:targetgroup/*/*"},
      {"Action": ["elasticloadbalancing:SetWebAcl", "elasticloadbalancing:ModifyListener",
      "elasticloadbalancing:AddListenerCertificates", "elasticloadbalancing:RemoveListenerCertificates",
      "elasticloadbalancing:ModifyRule"], "Effect": "Allow", "Resource": "*"}], "Version":
      "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: AWSLoadBalancerControllerIAMPolicy
  - resource_id: arn:aws:iam::123456789012:policy/AWSLoadBalancerControllerIAMPolicy
  - id: ANPAX2FJ77DC5VVD5KYUI
  - path: /


arn:aws:iam::123456789012:policy/AllowEC2ElasticIPReadWrite:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["ec2:ReleaseAddress", "ec2:DisassociateAddress",
      "ec2:DescribeAddresses", "ec2:DescribeInstances", "ec2:DescribeNetworkInterfaces",
      "ec2:AssociateAddress", "ec2:AllocateAddress"], "Effect": "Allow", "Resource":
      "*", "Sid": "VisualEditor0"}], "Version": "2012-10-17"}'
  - default_version_id: v6
  - tags: []
  - name: AllowEC2ElasticIPReadWrite
  - resource_id: arn:aws:iam::123456789012:policy/AllowEC2ElasticIPReadWrite
  - id: ANPAX2FJ77DCQUYBXL54N
  - path: /


arn:aws:iam::123456789012:policy/AmazonFori18ntest+=,.@-_:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["autoscaling:Describe*", "cloudwatch:Describe*",
      "cloudwatch:Get*", "cloudwatch:List*", "ec2:Describe*", "elasticloadbalancing:Describe*",
      "s3:Get*", "s3:List*"], "Effect": "Allow", "Resource": ["*"], "Sid": "1"}],
      "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: AmazonFori18ntest+=,.@-_
  - resource_id: arn:aws:iam::123456789012:policy/AmazonFori18ntest+=,.@-_
  - id: ANPAIPPXXIXAP67XMGXYW
  - path: /


arn:aws:iam::123456789012:policy/Amazonxyz_EBS_CSI_Driver_Policy:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["ec2:CreateSnapshot", "ec2:AttachVolume",
      "ec2:DetachVolume", "ec2:ModifyVolume", "ec2:DescribeAvailabilityZones", "ec2:DescribeInstances",
      "ec2:DescribeSnapshots", "ec2:DescribeTags", "ec2:DescribeVolumes", "ec2:DescribeVolumesModifications"],
      "Effect": "Allow", "Resource": "*"}, {"Action": ["ec2:CreateTags"], "Condition":
      {"StringEquals": {"ec2:CreateAction": ["CreateVolume", "CreateSnapshot"]}},
      "Effect": "Allow", "Resource": ["arn:aws:ec2:*:*:volume/*", "arn:aws:ec2:*:*:snapshot/*"]},
      {"Action": ["ec2:DeleteTags"], "Effect": "Allow", "Resource": ["arn:aws:ec2:*:*:volume/*",
      "arn:aws:ec2:*:*:snapshot/*"]}, {"Action": ["ec2:CreateVolume"], "Condition":
      {"StringLike": {"aws:RequestTag/ebs.csi.aws.com/cluster": "true"}}, "Effect":
      "Allow", "Resource": "*"}, {"Action": ["ec2:CreateVolume"], "Condition": {"StringLike":
      {"aws:RequestTag/CSIVolumeName": "*"}}, "Effect": "Allow", "Resource": "*"},
      {"Action": ["ec2:CreateVolume"], "Condition": {"StringLike": {"aws:RequestTag/kubernetes.io/cluster/*":
      "owned"}}, "Effect": "Allow", "Resource": "*"}, {"Action": ["ec2:DeleteVolume"],
      "Condition": {"StringLike": {"ec2:ResourceTag/ebs.csi.aws.com/cluster": "true"}},
      "Effect": "Allow", "Resource": "*"}, {"Action": ["ec2:DeleteVolume"], "Condition":
      {"StringLike": {"ec2:ResourceTag/CSIVolumeName": "*"}}, "Effect": "Allow", "Resource":
      "*"}, {"Action": ["ec2:DeleteVolume"], "Condition": {"StringLike": {"ec2:ResourceTag/kubernetes.io/cluster/*":
      "owned"}}, "Effect": "Allow", "Resource": "*"}, {"Action": ["ec2:DeleteSnapshot"],
      "Condition": {"StringLike": {"ec2:ResourceTag/CSIVolumeSnapshotName": "*"}},
      "Effect": "Allow", "Resource": "*"}, {"Action": ["ec2:DeleteSnapshot"], "Condition":
      {"StringLike": {"ec2:ResourceTag/ebs.csi.aws.com/cluster": "true"}}, "Effect":
      "Allow", "Resource": "*"}], "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: Amazonxyz_EBS_CSI_Driver_Policy
  - resource_id: arn:aws:iam::123456789012:policy/Amazonxyz_EBS_CSI_Driver_Policy
  - id: ANPAX2FJ77DC4MFI3OXEL
  - path: /


arn:aws:iam::123456789012:policy/AzureEventQueuePolicy:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["sqs:SendMessage", "sqs:ReceiveMessage"],
      "Effect": "Allow", "Resource": ["arn:aws:sqs:us-east-2:123456789012:azuredemoqueue.fifo"]}],
      "Version": "2012-10-17"}'
  - default_version_id: v2
  - tags: []
  - name: AzureEventQueuePolicy
  - resource_id: arn:aws:iam::123456789012:policy/AzureEventQueuePolicy
  - id: ANPAIEJ6247JCH4E4QZBI
  - path: /


arn:aws:iam::123456789012:policy/CHTPolicy_Borathon:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["autoscaling:Describe*", "aws-portal:ViewBilling",
      "aws-portal:ViewUsage", "cloudformation:ListStacks", "cloudformation:ListStackResources",
      "cloudformation:DescribeStacks", "cloudformation:DescribeStackEvents", "cloudformation:DescribeStackResources",
      "cloudformation:GetTemplate", "cloudfront:Get*", "cloudfront:List*", "cloudtrail:DescribeTrails",
      "cloudtrail:GetEventSelectors", "cloudtrail:ListTags", "cloudwatch:Describe*",
      "cloudwatch:Get*", "cloudwatch:List*", "config:Get*", "config:Describe*", "config:Deliver*",
      "config:List*", "cur:Describe*", "dms:Describe*", "dms:List*", "dynamodb:DescribeTable",
      "dynamodb:List*", "ec2:Describe*", "ec2:GetReservedInstancesExchangeQuote",
      "ecs:List*", "ecs:Describe*", "elasticache:Describe*", "elasticache:ListTagsForResource",
      "elasticbeanstalk:Check*", "elasticbeanstalk:Describe*", "elasticbeanstalk:List*",
      "elasticbeanstalk:RequestEnvironmentInfo", "elasticbeanstalk:RetrieveEnvironmentInfo",
      "elasticfilesystem:Describe*", "elasticloadbalancing:Describe*", "elasticmapreduce:Describe*",
      "elasticmapreduce:List*", "es:List*", "es:Describe*", "firehose:ListDeliveryStreams",
      "firehose:DescribeDeliveryStream", "iam:List*", "iam:Get*", "iam:GenerateCredentialReport",
      "kinesis:Describe*", "kinesis:List*", "kms:DescribeKey", "kms:GetKeyRotationStatus",
      "kms:ListKeys", "lambda:List*", "logs:Describe*", "redshift:Describe*", "route53:Get*",
      "route53:List*", "rds:Describe*", "rds:ListTagsForResource", "s3:GetBucketAcl",
      "s3:GetBucketLocation", "s3:GetBucketLogging", "s3:GetBucketPolicy", "s3:GetBucketTagging",
      "s3:GetBucketVersioning", "s3:GetBucketWebsite", "s3:List*", "sagemaker:Describe*",
      "sagemaker:List*", "sdb:GetAttributes", "sdb:List*", "ses:Get*", "ses:List*",
      "sns:Get*", "sns:List*", "sqs:GetQueueAttributes", "sqs:ListQueues", "storagegateway:List*",
      "storagegateway:Describe*", "workspaces:Describe*"], "Effect": "Allow", "Resource":
      "*"}], "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: CHTPolicy_Borathon
  - resource_id: arn:aws:iam::123456789012:policy/CHTPolicy_Borathon
  - id: ANPAX2FJ77DC2B7YAZMZ6
  - path: /


arn:aws:iam::123456789012:policy/DescribeImagesPolicy:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["ec2:DescribeImages"], "Effect":
      "Allow", "Resource": "*"}], "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: DescribeImagesPolicy
  - resource_id: arn:aws:iam::123456789012:policy/DescribeImagesPolicy
  - id: ANPAX2FJ77DC6FMEIRL7I
  - path: /


arn:aws:iam::123456789012:policy/EC2ListResources:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["ec2:DescribeInstances", "ec2:DescribeAggregateIdFormat",
      "ec2:DescribeSnapshots", "ec2:DescribePlacementGroups", "ec2:DescribeHostReservationOfferings",
      "ec2:DescribeInternetGateways", "ec2:DescribeVolumeStatus", "ec2:DescribeSpotDatafeedSubscription",
      "ec2:DescribeVolumes", "ec2:DescribeFpgaImageAttribute", "ec2:DescribeExportTasks",
      "ec2:DescribeAccountAttributes", "ec2:DescribeNetworkInterfacePermissions",
      "ec2:DescribeReservedInstances", "ec2:DescribeKeyPairs", "ec2:DescribeNetworkAcls",
      "ec2:DescribeRouteTables", "ec2:DescribeReservedInstancesListings", "ec2:DescribeEgressOnlyInternetGateways",
      "ec2:DescribeSpotFleetRequestHistory", "ec2:DescribeLaunchTemplates", "ec2:DescribeVpcClassicLinkDnsSupport",
      "ec2:DescribeSnapshotAttribute", "ec2:DescribeVpcPeeringConnections", "ec2:DescribeReservedInstancesOfferings",
      "ec2:DescribeIdFormat", "ec2:DescribeFleetInstances", "ec2:DescribeVpcEndpointServiceConfigurations",
      "ec2:DescribePrefixLists", "ec2:DescribeVolumeAttribute", "ec2:DescribeInstanceCreditSpecifications",
      "ec2:DescribeVpcClassicLink", "ec2:DescribeImportSnapshotTasks", "ec2:DescribeVpcEndpointServicePermissions",
      "ec2:DescribeImageAttribute", "ec2:DescribeFleets", "ec2:DescribeVpcEndpoints",
      "ec2:DescribeReservedInstancesModifications", "ec2:DescribeSubnets", "ec2:DescribeVpnGateways",
      "ec2:DescribeMovingAddresses", "ec2:DescribeFleetHistory", "ec2:DescribePrincipalIdFormat",
      "ec2:DescribeAddresses", "ec2:DescribeInstanceAttribute", "ec2:DescribeRegions",
      "ec2:DescribeFlowLogs", "ec2:DescribeDhcpOptions", "ec2:DescribeVpcEndpointServices",
      "ec2:DescribeSpotInstanceRequests", "ec2:DescribeVpcAttribute", "ec2:DescribeSpotPriceHistory",
      "ec2:DescribeNetworkInterfaces", "ec2:DescribeAvailabilityZones", "ec2:DescribeNetworkInterfaceAttribute",
      "ec2:DescribeVpcEndpointConnections", "ec2:DescribeInstanceStatus", "ec2:DescribeHostReservations",
      "ec2:DescribeIamInstanceProfileAssociations", "ec2:DescribeLaunchTemplateVersions",
      "ec2:DescribeBundleTasks", "ec2:DescribeIdentityIdFormat", "ec2:DescribeImportImageTasks",
      "ec2:DescribeClassicLinkInstances", "ec2:DescribeNatGateways", "ec2:DescribeCustomerGateways",
      "ec2:DescribeVpcEndpointConnectionNotifications", "ec2:DescribeSecurityGroups",
      "ec2:DescribeSpotFleetRequests", "ec2:DescribeHosts", "ec2:DescribeImages",
      "ec2:DescribeFpgaImages", "ec2:DescribeSpotFleetInstances", "ec2:DescribeSecurityGroupReferences",
      "ec2:DescribeVpcs", "ec2:DescribeConversionTasks", "ec2:DescribeStaleSecurityGroups"],
      "Effect": "Allow", "Resource": "*", "Sid": "VisualEditor0"}], "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: EC2ListResources
  - resource_id: arn:aws:iam::123456789012:policy/EC2ListResources
  - id: ANPAILKUVUHIUSA3KUTQW
  - path: /


arn:aws:iam::123456789012:policy/KK=+WW_EES:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["autoscaling:Describe*", "cloudwatch:Describe*",
      "cloudwatch:Get*", "cloudwatch:List*", "ec2:Describe*", "elasticloadbalancing:Describe*",
      "s3:Get*", "s3:List*"], "Effect": "Allow", "Resource": ["*"], "Sid": "1"}],
      "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: KK=+WW_EES
  - resource_id: arn:aws:iam::123456789012:policy/KK=+WW_EES
  - id: ANPAI3PZT5EAIULAN2HGI
  - path: /


arn:aws:iam::123456789012:policy/apigateway-sqs-access-policy:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["sqs:SendMessage", "sqs:ReceiveMessage"],
      "Effect": "Allow", "Resource": ["arn:aws:sqs:us-east-2:123456789012:azuredemoqueue.fifo"]}],
      "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: apigateway-sqs-access-policy
  - resource_id: arn:aws:iam::123456789012:policy/apigateway-sqs-access-policy
  - id: ANPAJP6WUCIFEUQ2SLGUQ
  - path: /


arn:aws:iam::123456789012:policy/azure-events-forwarder-lambda-iam:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": "lambda:*", "Effect": "Allow", "Resource":
      "*", "Sid": "VisualEditor0"}], "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: azure-events-forwarder-lambda-iam
  - resource_id: arn:aws:iam::123456789012:policy/azure-events-forwarder-lambda-iam
  - id: ANPAJS2XE3NYTBWNWSEKC
  - path: /


arn:aws:iam::123456789012:policy/control-plane.tkg.cloud.abc.com:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["autoscaling:DescribeAutoScalingGroups",
      "autoscaling:DescribeLaunchConfigurations", "autoscaling:DescribeTags", "ec2:DescribeInstances",
      "ec2:DescribeImages", "ec2:DescribeRegions", "ec2:DescribeRouteTables", "ec2:DescribeSecurityGroups",
      "ec2:DescribeSubnets", "ec2:DescribeVolumes", "ec2:CreateSecurityGroup", "ec2:CreateTags",
      "ec2:CreateVolume", "ec2:ModifyInstanceAttribute", "ec2:ModifyVolume", "ec2:AttachVolume",
      "ec2:AuthorizeSecurityGroupIngress", "ec2:CreateRoute", "ec2:DeleteRoute", "ec2:DeleteSecurityGroup",
      "ec2:DeleteVolume", "ec2:DetachVolume", "ec2:RevokeSecurityGroupIngress", "ec2:DescribeVpcs",
      "elasticloadbalancing:AddTags", "elasticloadbalancing:AttachLoadBalancerToSubnets",
      "elasticloadbalancing:ApplySecurityGroupsToLoadBalancer", "elasticloadbalancing:CreateLoadBalancer",
      "elasticloadbalancing:CreateLoadBalancerPolicy", "elasticloadbalancing:CreateLoadBalancerListeners",
      "elasticloadbalancing:ConfigureHealthCheck", "elasticloadbalancing:DeleteLoadBalancer",
      "elasticloadbalancing:DeleteLoadBalancerListeners", "elasticloadbalancing:DescribeLoadBalancers",
      "elasticloadbalancing:DescribeLoadBalancerAttributes", "elasticloadbalancing:DetachLoadBalancerFromSubnets",
      "elasticloadbalancing:DeregisterInstancesFromLoadBalancer", "elasticloadbalancing:ModifyLoadBalancerAttributes",
      "elasticloadbalancing:RegisterInstancesWithLoadBalancer", "elasticloadbalancing:SetLoadBalancerPoliciesForBackendServer",
      "elasticloadbalancing:AddTags", "elasticloadbalancing:CreateListener", "elasticloadbalancing:CreateTargetGroup",
      "elasticloadbalancing:DeleteListener", "elasticloadbalancing:DeleteTargetGroup",
      "elasticloadbalancing:DescribeListeners", "elasticloadbalancing:DescribeLoadBalancerPolicies",
      "elasticloadbalancing:DescribeTargetGroups", "elasticloadbalancing:DescribeTargetHealth",
      "elasticloadbalancing:ModifyListener", "elasticloadbalancing:ModifyTargetGroup",
      "elasticloadbalancing:RegisterTargets", "elasticloadbalancing:SetLoadBalancerPoliciesOfListener",
      "iam:CreateServiceLinkedRole", "kms:DescribeKey"], "Effect": "Allow", "Resource":
      ["*"]}], "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: control-plane.tkg.cloud.abc.com
  - resource_id: arn:aws:iam::123456789012:policy/control-plane.tkg.cloud.abc.com
  - id: ANPAX2FJ77DC22ZNIPHUE
  - path: /


arn:aws:iam::123456789012:policy/control-plane.tmc.cloud.abc.com:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["autoscaling:DescribeAutoScalingGroups",
      "autoscaling:DescribeLaunchConfigurations", "autoscaling:DescribeTags", "ec2:DescribeInstances",
      "ec2:DescribeImages", "ec2:DescribeRegions", "ec2:DescribeRouteTables", "ec2:DescribeSecurityGroups",
      "ec2:DescribeSubnets", "ec2:DescribeVolumes", "ec2:CreateSecurityGroup", "ec2:CreateTags",
      "ec2:CreateVolume", "ec2:ModifyInstanceAttribute", "ec2:ModifyVolume", "ec2:AttachVolume",
      "ec2:AuthorizeSecurityGroupIngress", "ec2:CreateRoute", "ec2:DeleteRoute", "ec2:DeleteSecurityGroup",
      "ec2:DeleteVolume", "ec2:DetachVolume", "ec2:RevokeSecurityGroupIngress", "ec2:DescribeVpcs",
      "elasticloadbalancing:AddTags", "elasticloadbalancing:AttachLoadBalancerToSubnets",
      "elasticloadbalancing:ApplySecurityGroupsToLoadBalancer", "elasticloadbalancing:CreateLoadBalancer",
      "elasticloadbalancing:CreateLoadBalancerPolicy", "elasticloadbalancing:CreateLoadBalancerListeners",
      "elasticloadbalancing:ConfigureHealthCheck", "elasticloadbalancing:DeleteLoadBalancer",
      "elasticloadbalancing:DeleteLoadBalancerListeners", "elasticloadbalancing:DescribeLoadBalancers",
      "elasticloadbalancing:DescribeLoadBalancerAttributes", "elasticloadbalancing:DetachLoadBalancerFromSubnets",
      "elasticloadbalancing:DeregisterInstancesFromLoadBalancer", "elasticloadbalancing:ModifyLoadBalancerAttributes",
      "elasticloadbalancing:RegisterInstancesWithLoadBalancer", "elasticloadbalancing:SetLoadBalancerPoliciesForBackendServer",
      "elasticloadbalancing:AddTags", "elasticloadbalancing:CreateListener", "elasticloadbalancing:CreateTargetGroup",
      "elasticloadbalancing:DeleteListener", "elasticloadbalancing:DeleteTargetGroup",
      "elasticloadbalancing:DescribeListeners", "elasticloadbalancing:DescribeLoadBalancerPolicies",
      "elasticloadbalancing:DescribeTargetGroups", "elasticloadbalancing:DescribeTargetHealth",
      "elasticloadbalancing:ModifyListener", "elasticloadbalancing:ModifyTargetGroup",
      "elasticloadbalancing:RegisterTargets", "elasticloadbalancing:SetLoadBalancerPoliciesOfListener",
      "iam:CreateServiceLinkedRole", "kms:DescribeKey"], "Effect": "Allow", "Resource":
      ["*"]}, {"Action": ["secretsmanager:DeleteSecret", "secretsmanager:GetSecretValue"],
      "Effect": "Allow", "Resource": ["arn:aws:secretsmanager:*:*:secret:aws.cluster.x-k8s.io/*"]}],
      "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: control-plane.tmc.cloud.abc.com
  - resource_id: arn:aws:iam::123456789012:policy/control-plane.tmc.cloud.abc.com
  - id: ANPAX2FJ77DC7IE5MMLRD
  - path: /


arn:aws:iam::123456789012:policy/controllers.tkg.cloud.abc.com:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["ec2:AllocateAddress", "ec2:AssociateRouteTable",
      "ec2:AttachInternetGateway", "ec2:AuthorizeSecurityGroupIngress", "ec2:CreateInternetGateway",
      "ec2:CreateNatGateway", "ec2:CreateRoute", "ec2:CreateRouteTable", "ec2:CreateSecurityGroup",
      "ec2:CreateSubnet", "ec2:CreateTags", "ec2:CreateVpc", "ec2:ModifyVpcAttribute",
      "ec2:DeleteInternetGateway", "ec2:DeleteNatGateway", "ec2:DeleteRouteTable",
      "ec2:DeleteSecurityGroup", "ec2:DeleteSubnet", "ec2:DeleteTags", "ec2:DeleteVpc",
      "ec2:DescribeAccountAttributes", "ec2:DescribeAddresses", "ec2:DescribeAvailabilityZones",
      "ec2:DescribeInstances", "ec2:DescribeInternetGateways", "ec2:DescribeImages",
      "ec2:DescribeNatGateways", "ec2:DescribeNetworkInterfaces", "ec2:DescribeNetworkInterfaceAttribute",
      "ec2:DescribeRouteTables", "ec2:DescribeSecurityGroups", "ec2:DescribeSubnets",
      "ec2:DescribeVpcs", "ec2:DescribeVpcAttribute", "ec2:DescribeVolumes", "ec2:DetachInternetGateway",
      "ec2:DisassociateRouteTable", "ec2:DisassociateAddress", "ec2:ModifyInstanceAttribute",
      "ec2:ModifyNetworkInterfaceAttribute", "ec2:ModifySubnetAttribute", "ec2:ReleaseAddress",
      "ec2:RevokeSecurityGroupIngress", "ec2:RunInstances", "ec2:TerminateInstances",
      "tag:GetResources", "elasticloadbalancing:AddTags", "elasticloadbalancing:CreateLoadBalancer",
      "elasticloadbalancing:ConfigureHealthCheck", "elasticloadbalancing:DeleteLoadBalancer",
      "elasticloadbalancing:DescribeLoadBalancers", "elasticloadbalancing:DescribeLoadBalancerAttributes",
      "elasticloadbalancing:DescribeTags", "elasticloadbalancing:ModifyLoadBalancerAttributes",
      "elasticloadbalancing:RegisterInstancesWithLoadBalancer", "elasticloadbalancing:DeregisterInstancesFromLoadBalancer",
      "elasticloadbalancing:RemoveTags"], "Effect": "Allow", "Resource": ["*"]}, {"Action":
      ["iam:CreateServiceLinkedRole"], "Condition": {"StringLike": {"iam:AWSServiceName":
      "elasticloadbalancing.amazonaws.com"}}, "Effect": "Allow", "Resource": ["arn:*:iam::*:role/aws-service-role/elasticloadbalancing.amazonaws.com/AWSServiceRoleForElasticLoadBalancing"]},
      {"Action": ["iam:PassRole"], "Effect": "Allow", "Resource": ["arn:*:iam::*:role/*.tkg.cloud.abc.com"]},
      {"Action": ["secretsmanager:CreateSecret", "secretsmanager:DeleteSecret", "secretsmanager:TagResource"],
      "Effect": "Allow", "Resource": ["arn:*:secretsmanager:*:*:secret:aws.cluster.x-k8s.io/*"]}],
      "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: controllers.tkg.cloud.abc.com
  - resource_id: arn:aws:iam::123456789012:policy/controllers.tkg.cloud.abc.com
  - id: ANPAX2FJ77DCXUDJQ2YMU
  - path: /


arn:aws:iam::123456789012:policy/controllers.tmc.cloud.abc.com:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["ec2:*", "tag:*", "elasticloadbalancing:*"],
      "Effect": "Allow", "Resource": ["*"]}, {"Action": ["secretsmanager:CreateSecret",
      "secretsmanager:DeleteSecret", "secretsmanager:TagResource"], "Effect": "Allow",
      "Resource": ["arn:aws:secretsmanager:*:*:secret:aws.cluster.x-k8s.io/*"]}, {"Action":
      ["iam:CreateServiceLinkedRole"], "Condition": {"StringLike": {"iam:AWSServiceName":
      "elasticloadbalancing.amazonaws.com"}}, "Effect": "Allow", "Resource": ["arn:aws:iam::123456789012:role/aws-service-role/elasticloadbalancing.amazonaws.com/AWSServiceRoleForElasticLoadBalancing"]},
      {"Action": ["iam:PassRole"], "Effect": "Allow", "Resource": ["arn:aws:iam::123456789012:role/*.tmc.cloud.abc.com"]}],
      "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: controllers.tmc.cloud.abc.com
  - resource_id: arn:aws:iam::123456789012:policy/controllers.tmc.cloud.abc.com
  - id: ANPAX2FJ77DC3B4NSRAKB
  - path: /


arn:aws:iam::123456789012:policy/get-encrypted-parameter:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["kms:Decrypt", "ssm:GetParameter"],
      "Effect": "Allow", "Resource": ["arn:aws:ssm:us-west-1:123456789012:parameter/krisi-test-secure-parameter",
      "arn:aws:kms:us-west-1:123456789012:key/alias/aws/ssm"], "Sid": "VisualEditor0"}],
      "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: get-encrypted-parameter
  - resource_id: arn:aws:iam::123456789012:policy/get-encrypted-parameter
  - id: ANPAX2FJ77DCRYJJWEDYF
  - path: /


arn:aws:iam::123456789012:policy/guardrails-vRNI-policy:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["ec2:Describe*", "logs:Describe*",
      "sts:*", "iam:ListAccountAliases", "logs:TestMetricFilter", "logs:FilterLogEvents",
      "logs:Get*"], "Effect": "Allow", "Resource": "*", "Sid": "VisualEditor0"}],
      "Version": "2012-10-17"}'
  - default_version_id: v2
  - tags:
    - Key: guardrails
      Value: ''
  - name: guardrails-vRNI-policy
  - resource_id: arn:aws:iam::123456789012:policy/guardrails-vRNI-policy
  - id: ANPAX2FJ77DCY3CBNPDRC
  - path: /


arn:aws:iam::123456789012:policy/idem-test-policy-3fb27cf8-ba7f-462a-badb-b84f1e4bac3a:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["ec2:CreateSubnet"], "Effect": "Allow",
      "Resource": "*"}], "Version": "2012-10-17"}'
  - default_version_id: v2
  - tags:
    - Key: idem-test-iam-policy-key-36a3c8e4-b01a-4719-af9f-cd2244d04fe9
      Value: idem-test-iam-policy-value-c684397b-224d-48c2-a82d-b4681b03e2fd
    - Key: Name
      Value: idem-test-policy-3fb27cf8-ba7f-462a-badb-b84f1e4bac3a
  - name: idem-test-policy-3fb27cf8-ba7f-462a-badb-b84f1e4bac3a
  - resource_id: arn:aws:iam::123456789012:policy/idem-test-policy-3fb27cf8-ba7f-462a-badb-b84f1e4bac3a
  - id: ANPAX2FJ77DCSQ2VFWIVP
  - path: /


arn:aws:iam::123456789012:policy/idem-test-policy-9b3215c6-6645-4352-bdc4-90d8f0af5c4b:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["ec2:CreateSubnet"], "Effect": "Allow",
      "Resource": "*"}], "Version": "2012-10-17"}'
  - default_version_id: v2
  - tags:
    - Key: Name
      Value: idem-test-policy-9b3215c6-6645-4352-bdc4-90d8f0af5c4b
  - name: idem-test-policy-9b3215c6-6645-4352-bdc4-90d8f0af5c4b
  - resource_id: arn:aws:iam::123456789012:policy/idem-test-policy-9b3215c6-6645-4352-bdc4-90d8f0af5c4b
  - id: ANPAX2FJ77DCXT6WJZC2P
  - path: /


arn:aws:iam::123456789012:policy/idem-test-policy-a1480c57-1776-4b12-b463-f5e0b8e276dc:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["ec2:CreateSubnet"], "Effect": "Allow",
      "Resource": "*"}], "Version": "2012-10-17"}'
  - default_version_id: v2
  - tags:
    - Key: idem-test-iam-policy-key-fcce768d-346f-40fe-9bc4-91a2d7fa028e
      Value: idem-test-iam-policy-value-1f227395-a191-4bdc-8880-92f2fc8f8186
    - Key: Name
      Value: idem-test-policy-a1480c57-1776-4b12-b463-f5e0b8e276dc
  - name: idem-test-policy-a1480c57-1776-4b12-b463-f5e0b8e276dc
  - resource_id: arn:aws:iam::123456789012:policy/idem-test-policy-a1480c57-1776-4b12-b463-f5e0b8e276dc
  - id: ANPAX2FJ77DC3A5Q3XEHY
  - path: /


arn:aws:iam::123456789012:policy/idem-test-policy-c12916c3-052c-4cd2-9be0-13ff4e18296d:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["ec2:CreateSubnet"], "Effect": "Allow",
      "Resource": "*"}], "Version": "2012-10-17"}'
  - default_version_id: v2
  - tags:
    - Key: iam-policy-key-34a7a85d-f61a-4077-abfb-d4a9ee2f72b3
      Value: iam-policy-value-03095bce-24f5-4e98-bb39-b3d6eab5684a
    - Key: Name
      Value: idem-test-policy-c12916c3-052c-4cd2-9be0-13ff4e18296d
  - name: idem-test-policy-c12916c3-052c-4cd2-9be0-13ff4e18296d
  - resource_id: arn:aws:iam::123456789012:policy/idem-test-policy-c12916c3-052c-4cd2-9be0-13ff4e18296d
  - id: ANPAX2FJ77DC5CX4PN3L4
  - path: /


arn:aws:iam::123456789012:policy/idem-test-policy-e225f994-b47d-49b7-bb20-bde95e66947e:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["ec2:CreateVpc"], "Effect": "Allow",
      "Resource": "*"}], "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags:
    - Key: Name
      Value: idem-test-policy-e225f994-b47d-49b7-bb20-bde95e66947e
  - name: idem-test-policy-e225f994-b47d-49b7-bb20-bde95e66947e
  - resource_id: arn:aws:iam::123456789012:policy/idem-test-policy-e225f994-b47d-49b7-bb20-bde95e66947e
  - id: ANPAX2FJ77DCTDB2ZTALD
  - path: /


arn:aws:iam::123456789012:policy/krum-ec2-autoscaling-minimum-permissions-set:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["autoscaling:AttachLoadBalancers",
      "autoscaling:DescribeAutoScalingInstances", "autoscaling:DescribePolicies",
      "autoscaling:DescribeLaunchConfigurations", "autoscaling:SuspendProcesses",
      "autoscaling:DescribeLoadBalancers", "autoscaling:PutScheduledUpdateGroupAction",
      "autoscaling:AttachInstances", "autoscaling:DeleteLaunchConfiguration", "autoscaling:DescribeAutoScalingGroups",
      "autoscaling:PutScalingPolicy", "autoscaling:UpdateAutoScalingGroup", "autoscaling:DescribeScheduledActions",
      "autoscaling:DeleteAutoScalingGroup", "autoscaling:CreateAutoScalingGroup"],
      "Effect": "Allow", "Resource": "*", "Sid": "VisualEditor0"}], "Version": "2012-10-17"}'
  - default_version_id: v5
  - tags: []
  - name: krum-ec2-autoscaling-minimum-permissions-set
  - resource_id: arn:aws:iam::123456789012:policy/krum-ec2-autoscaling-minimum-permissions-set
  - id: ANPAX2FJ77DC37NOUAVLY
  - path: /


arn:aws:iam::123456789012:policy/krum-sts-minimum-permissions-set:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect": "Allow",
      "Resource": "*", "Sid": "VisualEditor0"}], "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: krum-sts-minimum-permissions-set
  - resource_id: arn:aws:iam::123456789012:policy/krum-sts-minimum-permissions-set
  - id: ANPAX2FJ77DC2CLWMBLXJ
  - path: /


arn:aws:iam::123456789012:policy/nodes.tkg.cloud.abc.com:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["ec2:DescribeInstances", "ec2:DescribeRegions",
      "ecr:GetAuthorizationToken", "ecr:BatchCheckLayerAvailability", "ecr:GetDownloadUrlForLayer",
      "ecr:GetRepositoryPolicy", "ecr:DescribeRepositories", "ecr:ListImages", "ecr:BatchGetImage"],
      "Effect": "Allow", "Resource": ["*"]}, {"Action": ["secretsmanager:DeleteSecret",
      "secretsmanager:GetSecretValue"], "Effect": "Allow", "Resource": ["arn:*:secretsmanager:*:*:secret:aws.cluster.x-k8s.io/*"]},
      {"Action": ["ssm:UpdateInstanceInformation", "ssmmessages:CreateControlChannel",
      "ssmmessages:CreateDataChannel", "ssmmessages:OpenControlChannel", "ssmmessages:OpenDataChannel",
      "s3:GetEncryptionConfiguration"], "Effect": "Allow", "Resource": ["*"]}], "Version":
      "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: nodes.tkg.cloud.abc.com
  - resource_id: arn:aws:iam::123456789012:policy/nodes.tkg.cloud.abc.com
  - id: ANPAX2FJ77DCVU7JFMYF2
  - path: /


arn:aws:iam::123456789012:policy/nodes.tmc.cloud.abc.com:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["ec2:DescribeInstances", "ec2:DescribeRegions",
      "ecr:GetAuthorizationToken", "ecr:BatchCheckLayerAvailability", "ecr:GetDownloadUrlForLayer",
      "ecr:GetRepositoryPolicy", "ecr:DescribeRepositories", "ecr:ListImages", "ecr:BatchGetImage"],
      "Effect": "Allow", "Resource": ["*"]}, {"Action": ["secretsmanager:DeleteSecret",
      "secretsmanager:GetSecretValue"], "Effect": "Allow", "Resource": ["arn:aws:secretsmanager:*:*:secret:aws.cluster.x-k8s.io/*"]}],
      "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: nodes.tmc.cloud.abc.com
  - resource_id: arn:aws:iam::123456789012:policy/nodes.tmc.cloud.abc.com
  - id: ANPAX2FJ77DC5QCMKSOWO
  - path: /


arn:aws:iam::123456789012:policy/org1_cluster01_potato_dev_k8s_admins:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": "xyz:DescribeCluster", "Effect": "Allow",
      "Resource": "arn:aws:xyz:us-west-2:123456789012:cluster/idem-test"}], "Version":
      "2012-10-17"}'
  - default_version_id: v2
  - tags: []
  - name: org1_cluster01_potato_dev_k8s_admins
  - resource_id: arn:aws:iam::123456789012:policy/org1_cluster01_potato_dev_k8s_admins
  - id: ANPAX2FJ77DC2ZZ2FCW4I
  - path: /


arn:aws:iam::123456789012:policy/org1_cluster01_potato_dev_org1_k8s_readonly:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": "xyz:DescribeCluster", "Effect": "Allow",
      "Resource": "arn:aws:xyz:us-west-2:123456789012:cluster/idem-test"}], "Version":
      "2012-10-17"}'
  - default_version_id: v2
  - tags: []
  - name: org1_cluster01_potato_dev_org1_k8s_readonly
  - resource_id: arn:aws:iam::123456789012:policy/org1_cluster01_potato_dev_org1_k8s_readonly
  - id: ANPAX2FJ77DCZCK64AYYV
  - path: /


arn:aws:iam::123456789012:policy/org1_idem_test_potato_dev_k8s_admins:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": "xyz:DescribeCluster", "Effect": "Allow",
      "Resource": "arn:aws:xyz:us-west-2:123456789012:cluster/idem-test"}], "Version":
      "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: org1_idem_test_potato_dev_k8s_admins
  - resource_id: arn:aws:iam::123456789012:policy/org1_idem_test_potato_dev_k8s_admins
  - id: ANPAX2FJ77DCSVDDH4UNQ
  - path: /


arn:aws:iam::123456789012:policy/org1_idem_test_potato_dev_org1_k8s_readonly:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": "xyz:DescribeCluster", "Effect": "Allow",
      "Resource": "arn:aws:xyz:us-west-2:123456789012:cluster/idem-test"}], "Version":
      "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: org1_idem_test_potato_dev_org1_k8s_readonly
  - resource_id: arn:aws:iam::123456789012:policy/org1_idem_test_potato_dev_org1_k8s_readonly
  - id: ANPAX2FJ77DC5UTRYPOWD
  - path: /


arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-0e827eb6-10ae-40f8-b460-4df21de60081:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": "logs:CreateLogGroup", "Effect": "Allow",
      "Resource": "arn:aws:logs:us-east-1:123456789012:*"}, {"Action": ["logs:CreateLogStream",
      "logs:PutLogEvents"], "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:/aws/lambda/go-test:*"]}],
      "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: AWSLambdaBasicExecutionRole-0e827eb6-10ae-40f8-b460-4df21de60081
  - resource_id: arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-0e827eb6-10ae-40f8-b460-4df21de60081
  - id: ANPAX2FJ77DCS7BW5CPI4
  - path: /service-role/


arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-2cbcf4ca-8169-413b-81bc-02ca66628d0d:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": "logs:CreateLogGroup", "Effect": "Allow",
      "Resource": "arn:aws:logs:us-east-1:123456789012:*"}, {"Action": ["logs:CreateLogStream",
      "logs:PutLogEvents"], "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:/aws/lambda/go-test:*"]}],
      "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: AWSLambdaBasicExecutionRole-2cbcf4ca-8169-413b-81bc-02ca66628d0d
  - resource_id: arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-2cbcf4ca-8169-413b-81bc-02ca66628d0d
  - id: ANPAX2FJ77DCUOL3BPYMW
  - path: /service-role/


arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-481e19cb-d70c-47d1-9597-ca85ee2958d2:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": "logs:CreateLogGroup", "Effect": "Allow",
      "Resource": "arn:aws:logs:eu-central-1:123456789012:*"}, {"Action": ["logs:CreateLogStream",
      "logs:PutLogEvents"], "Effect": "Allow", "Resource": ["arn:aws:logs:eu-central-1:123456789012:log-group:/aws/lambda/myTEST:*"]}],
      "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: AWSLambdaBasicExecutionRole-481e19cb-d70c-47d1-9597-ca85ee2958d2
  - resource_id: arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-481e19cb-d70c-47d1-9597-ca85ee2958d2
  - id: ANPAX2FJ77DCTIRFZFMSV
  - path: /service-role/


arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-7536bc56-a15a-4ff6-bdb8-b6ed04fdfbec:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": "logs:CreateLogGroup", "Effect": "Allow",
      "Resource": "arn:aws:logs:us-east-1:123456789012:*"}, {"Action": ["logs:CreateLogStream",
      "logs:PutLogEvents"], "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:/aws/lambda/powershell:*"]}],
      "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: AWSLambdaBasicExecutionRole-7536bc56-a15a-4ff6-bdb8-b6ed04fdfbec
  - resource_id: arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-7536bc56-a15a-4ff6-bdb8-b6ed04fdfbec
  - id: ANPAX2FJ77DCWUZ7ZXZHH
  - path: /service-role/


arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-77be7a89-c9ca-49c3-aac0-5d97de6b022a:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": "logs:CreateLogGroup", "Effect": "Allow",
      "Resource": "arn:aws:logs:us-west-2:123456789012:*"}, {"Action": ["logs:CreateLogStream",
      "logs:PutLogEvents"], "Effect": "Allow", "Resource": ["arn:aws:logs:us-west-2:123456789012:log-group:/aws/lambda/my-s3-function:*"]}],
      "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: AWSLambdaBasicExecutionRole-77be7a89-c9ca-49c3-aac0-5d97de6b022a
  - resource_id: arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-77be7a89-c9ca-49c3-aac0-5d97de6b022a
  - id: ANPAX2FJ77DC6KEPGQ5QL
  - path: /service-role/


arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-90b8d0c2-0619-4122-97a2-450841c1c21b:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["logs:CreateLogGroup", "logs:CreateLogStream",
      "logs:PutLogEvents", "logs:DescribeLogStreams"], "Effect": "Allow", "Resource":
      "arn:aws:logs:us-east-2:123456789012:*:*"}, {"Action": ["logs:CreateLogStream",
      "logs:PutLogEvents"], "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-2:123456789012:log-group:/aws/lambda/*:*"]}],
      "Version": "2012-10-17"}'
  - default_version_id: v3
  - tags: []
  - name: AWSLambdaBasicExecutionRole-90b8d0c2-0619-4122-97a2-450841c1c21b
  - resource_id: arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-90b8d0c2-0619-4122-97a2-450841c1c21b
  - id: ANPAIQOQODYEEYZ6K2IKA
  - path: /service-role/


arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-9b8dc771-615a-40ed-ad1b-5a3a4b4e577f:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": "logs:CreateLogGroup", "Effect": "Allow",
      "Resource": "arn:aws:logs:us-east-2:123456789012:*"}, {"Action": ["logs:CreateLogStream",
      "logs:PutLogEvents"], "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-2:123456789012:log-group:/aws/lambda/myFunctionName:*"]}],
      "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: AWSLambdaBasicExecutionRole-9b8dc771-615a-40ed-ad1b-5a3a4b4e577f
  - resource_id: arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-9b8dc771-615a-40ed-ad1b-5a3a4b4e577f
  - id: ANPAJK5D2FWFQLVWTZBA2
  - path: /service-role/


arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-ad4139c9-476f-49fa-9b5e-c3f59131819b:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": "logs:CreateLogGroup", "Effect": "Allow",
      "Resource": "arn:aws:logs:us-west-2:123456789012:*"}, {"Action": ["logs:CreateLogStream",
      "logs:PutLogEvents"], "Effect": "Allow", "Resource": ["arn:aws:logs:us-west-2:123456789012:log-group:/aws/lambda/idem-test:*"]}],
      "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: AWSLambdaBasicExecutionRole-ad4139c9-476f-49fa-9b5e-c3f59131819b
  - resource_id: arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-ad4139c9-476f-49fa-9b5e-c3f59131819b
  - id: ANPAX2FJ77DC3UFZ6PKNP
  - path: /service-role/


arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-b7f13f4b-0583-447f-a37d-8319109d2155:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": "logs:CreateLogGroup", "Effect": "Allow",
      "Resource": "arn:aws:logs:us-east-2:123456789012:*"}, {"Action": ["logs:CreateLogStream",
      "logs:PutLogEvents"], "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-2:123456789012:log-group:/aws/lambda/rnitin-lambda-sample1:*"]}],
      "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: AWSLambdaBasicExecutionRole-b7f13f4b-0583-447f-a37d-8319109d2155
  - resource_id: arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-b7f13f4b-0583-447f-a37d-8319109d2155
  - id: ANPAX2FJ77DCVGZP633OC
  - path: /service-role/


arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-c115d523-c25b-44e3-b42b-32143f4af4bf:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": "logs:CreateLogGroup", "Effect": "Allow",
      "Resource": "arn:aws:logs:us-east-1:123456789012:*"}, {"Action": ["logs:CreateLogStream",
      "logs:PutLogEvents"], "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:/aws/lambda/t1:*"]}],
      "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: AWSLambdaBasicExecutionRole-c115d523-c25b-44e3-b42b-32143f4af4bf
  - resource_id: arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-c115d523-c25b-44e3-b42b-32143f4af4bf
  - id: ANPAX2FJ77DCWHGZZ6T5F
  - path: /service-role/


arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-c5399330-733d-477d-b360-10358e90ba73:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": "logs:CreateLogGroup", "Effect": "Allow",
      "Resource": "arn:aws:logs:us-east-1:123456789012:*"}, {"Action": ["logs:CreateLogStream",
      "logs:PutLogEvents"], "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:/aws/lambda/InstanceTypeCheck:*"]}],
      "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: AWSLambdaBasicExecutionRole-c5399330-733d-477d-b360-10358e90ba73
  - resource_id: arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-c5399330-733d-477d-b360-10358e90ba73
  - id: ANPAX2FJ77DC7P4W7OWDX
  - path: /service-role/


arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-f28418d0-fcc1-413a-9443-7eda82a55427:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": "logs:CreateLogGroup", "Effect": "Allow",
      "Resource": "arn:aws:logs:eu-central-1:123456789012:*"}, {"Action": ["logs:CreateLogStream",
      "logs:PutLogEvents"], "Effect": "Allow", "Resource": ["arn:aws:logs:eu-central-1:123456789012:log-group:/aws/lambda/myFirstAPI:*"]}],
      "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: AWSLambdaBasicExecutionRole-f28418d0-fcc1-413a-9443-7eda82a55427
  - resource_id: arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-f28418d0-fcc1-413a-9443-7eda82a55427
  - id: ANPAX2FJ77DCZQQRC7QU6
  - path: /service-role/


arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-f35de733-79b9-4ffc-9ef2-1afaa15f52c8:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": "logs:CreateLogGroup", "Effect": "Allow",
      "Resource": "arn:aws:logs:us-east-1:123456789012:*"}, {"Action": ["logs:CreateLogStream",
      "logs:PutLogEvents"], "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:/aws/lambda/java-test:*"]}],
      "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: AWSLambdaBasicExecutionRole-f35de733-79b9-4ffc-9ef2-1afaa15f52c8
  - resource_id: arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-f35de733-79b9-4ffc-9ef2-1afaa15f52c8
  - id: ANPAX2FJ77DCTVDKPNBR7
  - path: /service-role/


arn:aws:iam::123456789012:policy/service-role/AWSLambdaS3ExecutionRole-7507ff52-f9ad-40bf-8f91-bcfc501db9e6:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["s3:GetObject"], "Effect": "Allow",
      "Resource": "arn:aws:s3:::*"}], "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: AWSLambdaS3ExecutionRole-7507ff52-f9ad-40bf-8f91-bcfc501db9e6
  - resource_id: arn:aws:iam::123456789012:policy/service-role/AWSLambdaS3ExecutionRole-7507ff52-f9ad-40bf-8f91-bcfc501db9e6
  - id: ANPAX2FJ77DCUBQFXA46X
  - path: /service-role/


arn:aws:iam::123456789012:policy/service-role/AmazonSageMaker-ExecutionPolicy-20180207T165162:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["s3:GetObject", "s3:PutObject", "s3:DeleteObject",
      "s3:ListBucket"], "Effect": "Allow", "Resource": ["arn:aws:s3:::*"]}], "Version":
      "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: AmazonSageMaker-ExecutionPolicy-20180207T165162
  - resource_id: arn:aws:iam::123456789012:policy/service-role/AmazonSageMaker-ExecutionPolicy-20180207T165162
  - id: ANPAIERQNST7U6EUC6BVW
  - path: /service-role/


arn:aws:iam::123456789012:policy/service-role/ProtonRolePolicy-test:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["cloudformation:CancelUpdateStack",
      "cloudformation:ContinueUpdateRollback", "cloudformation:CreateChangeSet", "cloudformation:CreateStack",
      "cloudformation:DeleteChangeSet", "cloudformation:DeleteStack", "cloudformation:DescribeChangeSet",
      "cloudformation:DescribeStackDriftDetectionStatus", "cloudformation:DescribeStackEvents",
      "cloudformation:DescribeStackResourceDrifts", "cloudformation:DescribeStacks",
      "cloudformation:DetectStackResourceDrift", "cloudformation:ExecuteChangeSet",
      "cloudformation:ListChangeSets", "cloudformation:ListStackResources", "cloudformation:UpdateStack"],
      "Effect": "Allow", "Resource": "arn:aws:cloudformation:*:123456789012:stack/AWSProton-*"},
      {"Condition": {"ForAnyValue:StringEquals": {"aws:CalledVia": ["cloudformation.amazonaws.com"]}},
      "Effect": "Allow", "NotAction": ["organizations:*", "account:*"], "Resource":
      "*"}, {"Action": ["organizations:DescribeOrganization", "account:ListRegions"],
      "Condition": {"ForAnyValue:StringEquals": {"aws:CalledVia": ["cloudformation.amazonaws.com"]}},
      "Effect": "Allow", "Resource": "*"}], "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: ProtonRolePolicy-test
  - resource_id: arn:aws:iam::123456789012:policy/service-role/ProtonRolePolicy-test
  - id: ANPAX2FJ77DCQ6NJ3BXDI
  - path: /service-role/


arn:aws:iam::123456789012:policy/service-role/StatesExecutionPolicy:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["lambda:InvokeFunction"], "Effect":
      "Allow", "Resource": "*"}], "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: StatesExecutionPolicy
  - resource_id: arn:aws:iam::123456789012:policy/service-role/StatesExecutionPolicy
  - id: ANPAIHMYKKWSGBHYVB5FW
  - path: /service-role/


arn:aws:iam::123456789012:policy/service-role/config-role-us-east-1_AWSConfigDeliveryPermissions_us-east-1:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["s3:PutObject*"], "Condition": {"StringLike":
      {"s3:x-amz-acl": "bucket-owner-full-control"}}, "Effect": "Allow", "Resource":
      ["arn:aws:s3:::config-bucket-123456789012/AWSLogs/123456789012/*"]}, {"Action":
      ["s3:GetBucketAcl"], "Effect": "Allow", "Resource": "arn:aws:s3:::config-bucket-123456789012"}],
      "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: config-role-us-east-1_AWSConfigDeliveryPermissions_us-east-1
  - resource_id: arn:aws:iam::123456789012:policy/service-role/config-role-us-east-1_AWSConfigDeliveryPermissions_us-east-1
  - id: ANPAI6UT23RYY2ONIP5SY
  - path: /service-role/


arn:aws:iam::123456789012:policy/spotinst-iam-stack-33rom-SpotinstManagedPolicy-1VGQO1DZRBAM8:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["ec2:RequestSpotInstances", "ec2:CancelSpotInstanceRequests",
      "ec2:CreateSpotDatafeedSubscription", "ec2:Describe*", "ec2:AssociateAddress",
      "ec2:AttachVolume", "ec2:ConfirmProductInstance", "ec2:CopyImage", "ec2:CopySnapshot",
      "ec2:CreateImage", "ec2:CreateSnapshot", "ec2:CreateTags", "ec2:CreateVolume",
      "ec2:DeleteTags", "ec2:DisassociateAddress", "ec2:ModifyImageAttribute", "ec2:ModifyInstanceAttribute",
      "ec2:MonitorInstances", "ec2:RebootInstances", "ec2:RegisterImage", "ec2:RunInstances",
      "ec2:StartInstances", "ec2:StopInstances", "ec2:TerminateInstances", "ec2:UnassignPrivateIpAddresses",
      "ec2:DeregisterImage", "ec2:DeleteSnapshot", "ec2:DeleteVolume", "ec2:ModifyReservedInstances",
      "ec2:CreateReservedInstancesListing", "ec2:CancelReservedInstancesListing",
      "ec2:ModifyNetworkInterfaceAttribute", "ec2:DeleteNetworkInterface"], "Effect":
      "Allow", "Resource": ["*"], "Sid": "GeneralSpotInstancesAccess"}, {"Action":
      ["elasticloadbalancing:Describe*", "elasticloadbalancing:Deregister*", "elasticloadbalancing:Register*",
      "elasticloadbalancing:RemoveTags", "elasticloadbalancing:RegisterTargets", "elasticloadbalancing:EnableAvailabilityZonesForLoadBalancer",
      "elasticloadbalancing:DisableAvailabilityZonesForLoadBalancer", "elasticloadbalancing:DescribeTags",
      "elasticloadbalancing:CreateTargetGroup", "elasticloadbalancing:DeleteTargetGroup",
      "elasticloadbalancing:ModifyRule", "elasticloadbalancing:AddTags", "elasticloadbalancing:ModifyTargetGroupAttributes",
      "elasticloadbalancing:ModifyTargetGroup", "elasticloadbalancing:ModifyListener"],
      "Effect": "Allow", "Resource": ["*"], "Sid": "AccessELB"}, {"Action": ["cloudwatch:DescribeAlarmHistory",
      "cloudwatch:DescribeAlarms", "cloudwatch:DescribeAlarmsForMetric", "cloudwatch:GetMetricStatistics",
      "cloudwatch:GetMetricData", "cloudwatch:ListMetrics", "cloudwatch:PutMetricData",
      "cloudwatch:PutMetricAlarm"], "Effect": "Allow", "Resource": ["*"], "Sid": "AccessCloudWatch"},
      {"Action": ["sns:Publish", "sns:ListTopics", "sns:CreateTopic", "sns:GetTopicAttributes",
      "sns:ListSubscriptionsByTopic", "sns:Subscribe"], "Effect": "Allow", "Resource":
      ["*"], "Sid": "AccessSNS"}, {"Action": ["iam:AddRoleToInstanceProfile", "iam:ListInstanceProfiles",
      "iam:ListInstanceProfilesForRole", "iam:PassRole", "iam:ListRoles", "iam:ListAccountAliases",
      "iam:GetPolicyVersion", "iam:ListPolicies", "iam:GetPolicy", "iam:ListAttachedRolePolicies",
      "organizations:ListAccounts", "iam:CreateServiceLinkedRole", "iam:PutRolePolicy",
      "iam:GetInstanceProfile", "iam:GetRolePolicy", "iam:ListRolePolicies", "iam:SimulatePrincipalPolicy"],
      "Effect": "Allow", "Resource": ["*"], "Sid": "AccessIAM"}, {"Action": ["elasticbeanstalk:Describe*",
      "elasticbeanstalk:RequestEnvironmentInfo", "elasticbeanstalk:RetrieveEnvironmentInfo",
      "elasticbeanstalk:ValidateConfigurationSettings", "elasticbeanstalk:UpdateEnvironment",
      "elasticbeanstalk:ListPlatformVersions", "cloudformation:GetTemplate", "cloudformation:DescribeStackResources",
      "cloudformation:DescribeStackResource", "cloudformation:DescribeStacks", "cloudformation:ListStackResources",
      "cloudformation:UpdateStack", "cloudformation:DescribeStackEvents", "logs:PutRetentionPolicy",
      "logs:createLogGroup", "elasticbeanstalk:ListTagsForResource"], "Effect": "Allow",
      "Resource": ["*"], "Sid": "GeneralAccessElaticBeanstalk"}, {"Action": ["autoscaling:*"],
      "Effect": "Allow", "Resource": ["*"], "Sid": "AccessAutoScalingGroups"}, {"Action":
      ["xyz:ListClusters", "xyz:DescribeNodegroup", "xyz:ListNodegroups"], "Effect":
      "Allow", "Resource": ["*"], "Sid": "Accessxyz"}, {"Action": ["elasticmapreduce:*",
      "s3:GetObject"], "Effect": "Allow", "Resource": ["*"], "Sid": "AccessEMR"},
      {"Action": ["ecs:List*", "ecs:Describe*", "ecs:DeregisterContainerInstance",
      "ecs:UpdateContainerInstancesState", "ecs:RegisterTaskDefinition", "ecs:CreateService",
      "application-autoscaling:PutScalingPolicy", "application-autoscaling:RegisterScalableTarget",
      "application-autoscaling:Describe*", "ecs:putAttributes"], "Effect": "Allow",
      "Resource": ["*"], "Sid": "AccessECS"}, {"Action": ["batch:List*", "batch:Describe*"],
      "Effect": "Allow", "Resource": ["*"], "Sid": "AccessBatch"}, {"Action": ["opsworks:DeregisterInstance",
      "opsworks:DescribeInstances", "opsworks:DescribeStacks", "opsworks:DescribeLayers"],
      "Effect": "Allow", "Resource": ["*"], "Sid": "AccessOpsWorks"}, {"Action": ["codedeploy:*"],
      "Effect": "Allow", "Resource": ["*"], "Sid": "AccessCodeDeploy"}, {"Action":
      ["s3:GetObject", "s3:List*", "s3:GetBucketLocation"], "Effect": "Allow", "Resource":
      ["*"], "Sid": "AccessGeneralS3"}, {"Action": ["route53:ListHostedZones", "route53:ListResourceRecordSets",
      "route53:ChangeResourceRecordSets"], "Effect": "Allow", "Resource": ["*"], "Sid":
      "AccessRoute53"}, {"Action": ["s3:*"], "Effect": "Allow", "Resource": ["arn:aws:s3:::elasticbeanstalk*"],
      "Sid": "AccesS3forElasticBeanstalk"}, {"Action": ["ecs:Poll", "ecs:DiscoverPollEndpoint",
      "ecs:StartTelemetrySession", "ecs:StartTask", "ecs:StopTask", "ecs:DescribeContainerInstances",
      "ecs:RegisterContainerInstance", "ecs:DeregisterContainerInstance", "ecs:SubmitContainerStateChange",
      "ecs:SubmitTaskStateChange"], "Effect": "Allow", "Resource": ["*"], "Sid": "DockerBasedBeanstalkEnvironments"},
      {"Action": ["elasticfilesystem:DescribeFileSystems"], "Effect": "Allow", "Resource":
      ["*"], "Sid": "ElasticFileSystem"}, {"Action": ["pricing:GetProducts"], "Effect":
      "Allow", "Resource": ["*"], "Sid": "Pricing"}, {"Action": ["savingsplans:Describe*",
      "savingsplans:List*"], "Effect": "Allow", "Resource": ["*"], "Sid": "SavingsPlan"},
      {"Action": ["lambda:ListFunctions"], "Effect": "Allow", "Resource": ["*"], "Sid":
      "Lambda"}], "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: spotinst-iam-stack-33rom-SpotinstManagedPolicy-1VGQO1DZRBAM8
  - resource_id: arn:aws:iam::123456789012:policy/spotinst-iam-stack-33rom-SpotinstManagedPolicy-1VGQO1DZRBAM8
  - id: ANPAX2FJ77DCS3TXKYLFL
  - path: /


arn:aws:iam::123456789012:policy/spotinst-iam-stack-8tt4w-SpotinstManagedPolicy-6URGPL3AFJO8:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["ec2:RequestSpotInstances", "ec2:CancelSpotInstanceRequests",
      "ec2:CreateSpotDatafeedSubscription", "ec2:Describe*", "ec2:AssociateAddress",
      "ec2:AttachVolume", "ec2:ConfirmProductInstance", "ec2:CopyImage", "ec2:CopySnapshot",
      "ec2:CreateImage", "ec2:CreateSnapshot", "ec2:CreateTags", "ec2:CreateVolume",
      "ec2:DeleteTags", "ec2:DisassociateAddress", "ec2:ModifyImageAttribute", "ec2:ModifyInstanceAttribute",
      "ec2:MonitorInstances", "ec2:RebootInstances", "ec2:RegisterImage", "ec2:RunInstances",
      "ec2:StartInstances", "ec2:StopInstances", "ec2:TerminateInstances", "ec2:UnassignPrivateIpAddresses",
      "ec2:DeregisterImage", "ec2:DeleteSnapshot", "ec2:DeleteVolume", "ec2:ModifyReservedInstances",
      "ec2:CreateReservedInstancesListing", "ec2:CancelReservedInstancesListing",
      "ec2:ModifyNetworkInterfaceAttribute", "ec2:DeleteNetworkInterface"], "Effect":
      "Allow", "Resource": ["*"], "Sid": "GeneralSpotInstancesAccess"}, {"Action":
      ["elasticloadbalancing:Describe*", "elasticloadbalancing:Deregister*", "elasticloadbalancing:Register*",
      "elasticloadbalancing:RemoveTags", "elasticloadbalancing:RegisterTargets", "elasticloadbalancing:EnableAvailabilityZonesForLoadBalancer",
      "elasticloadbalancing:DisableAvailabilityZonesForLoadBalancer", "elasticloadbalancing:DescribeTags",
      "elasticloadbalancing:CreateTargetGroup", "elasticloadbalancing:DeleteTargetGroup",
      "elasticloadbalancing:ModifyRule", "elasticloadbalancing:AddTags", "elasticloadbalancing:ModifyTargetGroupAttributes",
      "elasticloadbalancing:ModifyTargetGroup", "elasticloadbalancing:ModifyListener"],
      "Effect": "Allow", "Resource": ["*"], "Sid": "AccessELB"}, {"Action": ["cloudwatch:DescribeAlarmHistory",
      "cloudwatch:DescribeAlarms", "cloudwatch:DescribeAlarmsForMetric", "cloudwatch:GetMetricStatistics",
      "cloudwatch:GetMetricData", "cloudwatch:ListMetrics", "cloudwatch:PutMetricData",
      "cloudwatch:PutMetricAlarm"], "Effect": "Allow", "Resource": ["*"], "Sid": "AccessCloudWatch"},
      {"Action": ["sns:Publish", "sns:ListTopics", "sns:CreateTopic", "sns:GetTopicAttributes",
      "sns:ListSubscriptionsByTopic", "sns:Subscribe"], "Effect": "Allow", "Resource":
      ["*"], "Sid": "AccessSNS"}, {"Action": ["iam:AddRoleToInstanceProfile", "iam:ListInstanceProfiles",
      "iam:ListInstanceProfilesForRole", "iam:PassRole", "iam:ListRoles", "iam:ListAccountAliases",
      "iam:GetPolicyVersion", "iam:ListPolicies", "iam:GetPolicy", "iam:ListAttachedRolePolicies",
      "organizations:ListAccounts", "iam:CreateServiceLinkedRole", "iam:PutRolePolicy",
      "iam:GetInstanceProfile", "iam:GetRolePolicy", "iam:ListRolePolicies", "iam:SimulatePrincipalPolicy"],
      "Effect": "Allow", "Resource": ["*"], "Sid": "AccessIAM"}, {"Action": ["elasticbeanstalk:Describe*",
      "elasticbeanstalk:RequestEnvironmentInfo", "elasticbeanstalk:RetrieveEnvironmentInfo",
      "elasticbeanstalk:ValidateConfigurationSettings", "elasticbeanstalk:UpdateEnvironment",
      "elasticbeanstalk:ListPlatformVersions", "cloudformation:GetTemplate", "cloudformation:DescribeStackResources",
      "cloudformation:DescribeStackResource", "cloudformation:DescribeStacks", "cloudformation:ListStackResources",
      "cloudformation:UpdateStack", "cloudformation:DescribeStackEvents", "logs:PutRetentionPolicy",
      "logs:createLogGroup", "elasticbeanstalk:ListTagsForResource"], "Effect": "Allow",
      "Resource": ["*"], "Sid": "GeneralAccessElaticBeanstalk"}, {"Action": ["autoscaling:*"],
      "Effect": "Allow", "Resource": ["*"], "Sid": "AccessAutoScalingGroups"}, {"Action":
      ["xyz:ListClusters", "xyz:DescribeNodegroup", "xyz:ListNodegroups"], "Effect":
      "Allow", "Resource": ["*"], "Sid": "Accessxyz"}, {"Action": ["elasticmapreduce:*",
      "s3:GetObject"], "Effect": "Allow", "Resource": ["*"], "Sid": "AccessEMR"},
      {"Action": ["ecs:List*", "ecs:Describe*", "ecs:DeregisterContainerInstance",
      "ecs:UpdateContainerInstancesState", "ecs:RegisterTaskDefinition", "ecs:CreateService",
      "application-autoscaling:PutScalingPolicy", "application-autoscaling:RegisterScalableTarget",
      "application-autoscaling:Describe*", "ecs:putAttributes"], "Effect": "Allow",
      "Resource": ["*"], "Sid": "AccessECS"}, {"Action": ["batch:List*", "batch:Describe*"],
      "Effect": "Allow", "Resource": ["*"], "Sid": "AccessBatch"}, {"Action": ["opsworks:DeregisterInstance",
      "opsworks:DescribeInstances", "opsworks:DescribeStacks", "opsworks:DescribeLayers"],
      "Effect": "Allow", "Resource": ["*"], "Sid": "AccessOpsWorks"}, {"Action": ["codedeploy:*"],
      "Effect": "Allow", "Resource": ["*"], "Sid": "AccessCodeDeploy"}, {"Action":
      ["s3:GetObject", "s3:List*", "s3:GetBucketLocation"], "Effect": "Allow", "Resource":
      ["*"], "Sid": "AccessGeneralS3"}, {"Action": ["route53:ListHostedZones", "route53:ListResourceRecordSets",
      "route53:ChangeResourceRecordSets"], "Effect": "Allow", "Resource": ["*"], "Sid":
      "AccessRoute53"}, {"Action": ["s3:*"], "Effect": "Allow", "Resource": ["arn:aws:s3:::elasticbeanstalk*"],
      "Sid": "AccesS3forElasticBeanstalk"}, {"Action": ["ecs:Poll", "ecs:DiscoverPollEndpoint",
      "ecs:StartTelemetrySession", "ecs:StartTask", "ecs:StopTask", "ecs:DescribeContainerInstances",
      "ecs:RegisterContainerInstance", "ecs:DeregisterContainerInstance", "ecs:SubmitContainerStateChange",
      "ecs:SubmitTaskStateChange"], "Effect": "Allow", "Resource": ["*"], "Sid": "DockerBasedBeanstalkEnvironments"},
      {"Action": ["elasticfilesystem:DescribeFileSystems"], "Effect": "Allow", "Resource":
      ["*"], "Sid": "ElasticFileSystem"}, {"Action": ["pricing:GetProducts"], "Effect":
      "Allow", "Resource": ["*"], "Sid": "Pricing"}, {"Action": ["savingsplans:Describe*",
      "savingsplans:List*"], "Effect": "Allow", "Resource": ["*"], "Sid": "SavingsPlan"},
      {"Action": ["lambda:ListFunctions"], "Effect": "Allow", "Resource": ["*"], "Sid":
      "Lambda"}], "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: spotinst-iam-stack-8tt4w-SpotinstManagedPolicy-6URGPL3AFJO8
  - resource_id: arn:aws:iam::123456789012:policy/spotinst-iam-stack-8tt4w-SpotinstManagedPolicy-6URGPL3AFJO8
  - id: ANPAX2FJ77DC3HUBK77FV
  - path: /


arn:aws:iam::123456789012:policy/test_policy:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": "account:DeleteVpc", "Effect": "Allow",
      "Resource": "*", "Sid": "VisualEditor0"}], "Version": "2012-10-17"}'
  - default_version_id: v3
  - tags:
    - Key: MP2
      Value: tag-2
  - name: test_policy
  - resource_id: arn:aws:iam::123456789012:policy/test_policy
  - id: ANPAX2FJ77DCURKJFXYIV
  - path: /


arn:aws:iam::123456789012:policy/vmw-cloudhealth-automation:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["ec2:DeleteSnapshot"], "Effect":
      "Allow", "Resource": "*"}, {"Action": ["ec2:DeleteVolume"], "Effect": "Allow",
      "Resource": "*"}, {"Action": ["ec2:ModifyReservedInstances"], "Effect": "Allow",
      "Resource": "*"}, {"Action": ["ec2:DescribeReservedInstancesOfferings", "ec2:PurchaseReservedInstancesOffering",
      "sts:GetFederationToken"], "Effect": "Allow", "Resource": "*"}, {"Action": ["rds:DescribeReservedDBInstancesOfferings",
      "rds:PurchaseReservedDBInstancesOffering"], "Effect": "Allow", "Resource": "*"},
      {"Action": ["lambda:InvokeFunction"], "Effect": "Allow", "Resource": "*"}, {"Action":
      ["ec2:ReleaseAddress"], "Effect": "Allow", "Resource": "*"}, {"Action": ["ec2:CreateSnapshot"],
      "Effect": "Allow", "Resource": "*"}, {"Action": ["ec2:ModifyReservedInstances",
      "ec2:DescribeReservedInstancesOfferings", "ec2:GetReservedInstancesExchangeQuote",
      "ec2:AcceptReservedInstancesExchangeQuote"], "Effect": "Allow", "Resource":
      "*"}], "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: vmw-cloudhealth-automation
  - resource_id: arn:aws:iam::123456789012:policy/vmw-cloudhealth-automation
  - id: ANPAICPYXZBIW3Y35O6SI
  - path: /


arn:aws:iam::123456789012:policy/vmw-cloudhealth-policy:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["aws-portal:ViewBilling", "aws-portal:ViewUsage",
      "autoscaling:Describe*", "cloudformation:ListStacks", "cloudformation:ListStackResources",
      "cloudformation:DescribeStacks", "cloudformation:DescribeStackEvents", "cloudformation:DescribeStackResources",
      "cloudformation:GetTemplate", "cloudfront:Get*", "cloudfront:List*", "cloudtrail:DescribeTrails",
      "cloudtrail:ListTags", "cloudwatch:Describe*", "cloudwatch:Get*", "cloudwatch:List*",
      "config:Get*", "config:Describe*", "config:Deliver*", "config:List*", "cur:Describe*",
      "dynamodb:DescribeTable", "dynamodb:List*", "ec2:Describe*", "elasticache:Describe*",
      "elasticache:ListTagsForResource", "elasticbeanstalk:Check*", "elasticbeanstalk:Describe*",
      "elasticbeanstalk:List*", "elasticbeanstalk:RequestEnvironmentInfo", "elasticbeanstalk:RetrieveEnvironmentInfo",
      "elasticfilesystem:Describe*", "elasticloadbalancing:Describe*", "elasticmapreduce:Describe*",
      "elasticmapreduce:List*", "es:List*", "es:Describe*", "iam:List*", "iam:Get*",
      "iam:GenerateCredentialReport", "lambda:List*", "redshift:Describe*", "route53:Get*",
      "route53:List*", "rds:Describe*", "rds:ListTagsForResource", "s3:List*", "s3:GetBucketTagging",
      "s3:GetBucketLocation", "s3:GetBucketLogging", "s3:GetBucketVersioning", "s3:GetBucketWebsite",
      "sdb:GetAttributes", "sdb:List*", "ses:Get*", "ses:List*", "sns:Get*", "sns:List*",
      "sqs:GetQueueAttributes", "sqs:ListQueues", "storagegateway:List*", "storagegateway:Describe*",
      "workspaces:Describe*", "kinesis:Describe*", "kinesis:List*", "firehose:DescribeDeliveryStream",
      "firehose:ListDeliveryStreams"], "Effect": "Allow", "Resource": "*"}], "Version":
      "2012-10-17"}'
  - default_version_id: v3
  - tags: []
  - name: vmw-cloudhealth-policy
  - resource_id: arn:aws:iam::123456789012:policy/vmw-cloudhealth-policy
  - id: ANPAIPCUZFTQNXYIPTMWQ
  - path: /


arn:aws:iam::123456789012:policy/xyz-idem-test-admin:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": ["xyz:*"], "Effect": "Allow", "Resource":
      "*"}, {"Action": ["iam:PassRole"], "Effect": "Allow", "Resource": ["arn:aws:iam::123456789012:role/idem-test-temp-xyz-cluster"]},
      {"Action": ["kms:Create*", "kms:Describe*", "kms:Enable*", "kms:List*", "kms:Put*",
      "kms:Update*", "kms:Revoke*", "kms:Disable*", "kms:Get*", "kms:Delete*", "kms:TagResource",
      "kms:UntagResource", "kms:ScheduleKeyDeletion", "kms:CancelKeyDeletion"], "Effect":
      "Allow", "Resource": "arn:aws:kms:eu-west-3:123456789012:key/8ac5f341-fd1c-4e9d-9596-8f844dba5cc8"}],
      "Version": "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: xyz-idem-test-admin
  - resource_id: arn:aws:iam::123456789012:policy/xyz-idem-test-admin
  - id: ANPAX2FJ77DCY4DVSXP5E
  - path: /


arn:aws:iam::123456789012:policy/xyz-idem-test-jenkins:
  aws.iam.policy.present:
  - policy_document: '{"Statement": [{"Action": "xyz:DescribeCluster", "Effect": "Allow",
      "Resource": "arn:aws:xyz:eu-west-3:123456789012:cluster/idem-test"}], "Version":
      "2012-10-17"}'
  - default_version_id: v1
  - tags: []
  - name: xyz-idem-test-jenkins
  - resource_id: arn:aws:iam::123456789012:policy/xyz-idem-test-jenkins
  - id: ANPAX2FJ77DCWXVXTSXMQ
  - path: /


aws-ec2-spot-fleet-autoscale-role:
  aws.iam.role.present:
  - resource_id: aws-ec2-spot-fleet-autoscale-role
  - name: aws-ec2-spot-fleet-autoscale-role
  - arn: arn:aws:iam::123456789012:role/aws-ec2-spot-fleet-autoscale-role
  - id: AROAIFJXKTO4N4L4IDXUI
  - path: /
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "application-autoscaling.amazonaws.com"},
      "Sid": ""}], "Version": "2012-10-17"}'


aws-ec2-spot-fleet-autoscale-role-arn:aws:iam::aws:policy/service-role/AmazonEC2SpotFleetAutoscaleRole:
  aws.iam.role_policy_attachment.present:
  - role_name: aws-ec2-spot-fleet-autoscale-role
  - policy_arn: arn:aws:iam::aws:policy/service-role/AmazonEC2SpotFleetAutoscaleRole


aws-ec2-spot-fleet-tagging-role:
  aws.iam.role.present:
  - resource_id: aws-ec2-spot-fleet-tagging-role
  - name: aws-ec2-spot-fleet-tagging-role
  - arn: arn:aws:iam::123456789012:role/aws-ec2-spot-fleet-tagging-role
  - id: AROAJ4GSDAXW637ILXHWA
  - path: /
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "spotfleet.amazonaws.com"}, "Sid": ""}], "Version":
      "2012-10-17"}'


aws-ec2-spot-fleet-tagging-role-arn:aws:iam::aws:policy/service-role/AmazonEC2SpotFleetTaggingRole:
  aws.iam.role_policy_attachment.present:
  - role_name: aws-ec2-spot-fleet-tagging-role
  - policy_arn: arn:aws:iam::aws:policy/service-role/AmazonEC2SpotFleetTaggingRole


aws-elasticbeanstalk-ec2-role:
  aws.iam.role.present:
  - resource_id: aws-elasticbeanstalk-ec2-role
  - name: aws-elasticbeanstalk-ec2-role
  - arn: arn:aws:iam::123456789012:role/aws-elasticbeanstalk-ec2-role
  - id: AROAX2FJ77DC5DELYTJZV
  - path: /
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "ec2.amazonaws.com"}}], "Version": "2008-10-17"}'


aws-elasticbeanstalk-ec2-role-arn:aws:iam::aws:policy/AWSElasticBeanstalkMulticontainerDocker:
  aws.iam.role_policy_attachment.present:
  - role_name: aws-elasticbeanstalk-ec2-role
  - policy_arn: arn:aws:iam::aws:policy/AWSElasticBeanstalkMulticontainerDocker


aws-elasticbeanstalk-ec2-role-arn:aws:iam::aws:policy/AWSElasticBeanstalkWebTier:
  aws.iam.role_policy_attachment.present:
  - role_name: aws-elasticbeanstalk-ec2-role
  - policy_arn: arn:aws:iam::aws:policy/AWSElasticBeanstalkWebTier


aws-elasticbeanstalk-ec2-role-arn:aws:iam::aws:policy/AWSElasticBeanstalkWorkerTier:
  aws.iam.role_policy_attachment.present:
  - role_name: aws-elasticbeanstalk-ec2-role
  - policy_arn: arn:aws:iam::aws:policy/AWSElasticBeanstalkWorkerTier


aws-elasticbeanstalk-service-role:
  aws.iam.role.present:
  - resource_id: aws-elasticbeanstalk-service-role
  - name: aws-elasticbeanstalk-service-role
  - arn: arn:aws:iam::123456789012:role/aws-elasticbeanstalk-service-role
  - id: AROAX2FJ77DCUFJOVFA6A
  - path: /
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {"StringEquals": {"sts:ExternalId": "elasticbeanstalk"}}, "Effect": "Allow",
      "Principal": {"Service": "elasticbeanstalk.amazonaws.com"}}], "Version": "2012-10-17"}'


aws-elasticbeanstalk-service-role-arn:aws:iam::aws:policy/AWSElasticBeanstalkManagedUpdatesCustomerRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: aws-elasticbeanstalk-service-role
  - policy_arn: arn:aws:iam::aws:policy/AWSElasticBeanstalkManagedUpdatesCustomerRolePolicy


aws-elasticbeanstalk-service-role-arn:aws:iam::aws:policy/service-role/AWSElasticBeanstalkEnhancedHealth:
  aws.iam.role_policy_attachment.present:
  - role_name: aws-elasticbeanstalk-service-role
  - policy_arn: arn:aws:iam::aws:policy/service-role/AWSElasticBeanstalkEnhancedHealth


aws-node-simple-http-endpoint-dev-us-east-1-lambdaRole:
  aws.iam.role.present:
  - resource_id: aws-node-simple-http-endpoint-dev-us-east-1-lambdaRole
  - name: aws-node-simple-http-endpoint-dev-us-east-1-lambdaRole
  - arn: arn:aws:iam::123456789012:role/aws-node-simple-http-endpoint-dev-us-east-1-lambdaRole
  - id: AROAX2FJ77DCQ7JZUN46Y
  - path: /
  - description: ''
  - max_session_duration: 3600
  - tags:
    - Key: STAGE
      Value: dev
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "lambda.amazonaws.com"}}], "Version": "2012-10-17"}'


aws-node-simple-http-endpoint-dev-us-east-1-lambdaRole-aws-node-simple-http-endpoint-dev-lambda:
  aws.iam.role_policy.present:
  - resource_id: aws-node-simple-http-endpoint-dev-us-east-1-lambdaRole-aws-node-simple-http-endpoint-dev-lambda
  - role_name: aws-node-simple-http-endpoint-dev-us-east-1-lambdaRole
  - name: aws-node-simple-http-endpoint-dev-lambda
  - policy_document: '{"Statement": [{"Action": ["logs:CreateLogStream", "logs:CreateLogGroup"],
      "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:/aws/lambda/aws-node-simple-http-endpoint-dev*:*"]},
      {"Action": ["logs:PutLogEvents"], "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:/aws/lambda/aws-node-simple-http-endpoint-dev*:*:*"]}],
      "Version": "2012-10-17"}'


bn0lunfd-dev-us-east-1-lambdaRole:
  aws.iam.role.present:
  - resource_id: bn0lunfd-dev-us-east-1-lambdaRole
  - name: bn0lunfd-dev-us-east-1-lambdaRole
  - arn: arn:aws:iam::123456789012:role/bn0lunfd-dev-us-east-1-lambdaRole
  - id: AROAX2FJ77DCXDNF35CEQ
  - path: /
  - description: ''
  - max_session_duration: 3600
  - tags:
    - Key: STAGE
      Value: dev
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "lambda.amazonaws.com"}}], "Version": "2012-10-17"}'


bn0lunfd-dev-us-east-1-lambdaRole-bn0lunfd-dev-lambda:
  aws.iam.role_policy.present:
  - resource_id: bn0lunfd-dev-us-east-1-lambdaRole-bn0lunfd-dev-lambda
  - role_name: bn0lunfd-dev-us-east-1-lambdaRole
  - name: bn0lunfd-dev-lambda
  - policy_document: '{"Statement": [{"Action": ["logs:CreateLogStream", "logs:CreateLogGroup"],
      "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:/aws/lambda/bn0lunfd-dev*:*"]},
      {"Action": ["logs:PutLogEvents"], "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:/aws/lambda/bn0lunfd-dev*:*:*"]}],
      "Version": "2012-10-17"}'


cluster01-InfosecVulnScanRole:
  aws.iam.role.present:
  - resource_id: cluster01-InfosecVulnScanRole
  - name: cluster01-InfosecVulnScanRole
  - arn: arn:aws:iam::123456789012:role/cluster01-InfosecVulnScanRole
  - id: AROAX2FJ77DC7HX4KPKQB
  - path: /
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "ec2.amazonaws.com"}, "Sid": ""}], "Version":
      "2012-10-17"}'


cluster01-InfosecVulnScanRole-cluster01-InfosecVulnScanRole:
  aws.iam.role_policy.present:
  - resource_id: cluster01-InfosecVulnScanRole-cluster01-InfosecVulnScanRole
  - role_name: cluster01-InfosecVulnScanRole
  - name: cluster01-InfosecVulnScanRole
  - policy_document: '{"Statement": [{"Action": "ec2:Describe*", "Effect": "Allow",
      "Resource": "*"}, {"Action": "elasticloadbalancing:Describe*", "Effect": "Allow",
      "Resource": "*"}, {"Action": ["cloudwatch:ListMetrics", "cloudwatch:GetMetricStatistics",
      "cloudwatch:Describe*"], "Effect": "Allow", "Resource": "*"}, {"Action": "autoscaling:Describe*",
      "Effect": "Allow", "Resource": "*"}], "Version": "2012-10-17"}'


cluster01-temp-xyz:
  aws.iam.role.present:
  - resource_id: cluster01-temp-xyz
  - name: cluster01-temp-xyz
  - arn: arn:aws:iam::123456789012:role/cluster01-temp-xyz
  - id: AROAX2FJ77DCXP6NZEXRE
  - path: /
  - description: Allows EC2 instances to call AWS services on your behalf.
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "ec2.amazonaws.com"}}], "Version": "2012-10-17"}'


cluster01-temp-xyz-arn:aws:iam::123456789012:policy/idem-test-policy-3fb27cf8-ba7f-462a-badb-b84f1e4bac3a:
  aws.iam.role_policy_attachment.present:
  - role_name: cluster01-temp-xyz
  - policy_arn: arn:aws:iam::123456789012:policy/idem-test-policy-3fb27cf8-ba7f-462a-badb-b84f1e4bac3a


cluster01-temp-xyz-cluster-node:
  aws.iam.role.present:
  - resource_id: cluster01-temp-xyz-cluster-node
  - name: cluster01-temp-xyz-cluster-node
  - arn: arn:aws:iam::123456789012:role/cluster01-temp-xyz-cluster-node
  - id: AROAX2FJ77DC633PDXM3K
  - path: /
  - description: Allows EC2 instances to call AWS services on your behalf.
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "ec2.amazonaws.com"}}], "Version": "2012-10-17"}'


cluster01-temp-xyz-cluster-node-arn:aws:iam::aws:policy/AmazonxyzClusterPolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: cluster01-temp-xyz-cluster-node
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzClusterPolicy


cluster01-temp-xyz-cluster-node-cluster01-temp-xyz-bootstrap-access:
  aws.iam.role_policy.present:
  - resource_id: cluster01-temp-xyz-cluster-node-cluster01-temp-xyz-bootstrap-access
  - role_name: cluster01-temp-xyz-cluster-node
  - name: cluster01-temp-xyz-bootstrap-access
  - policy_document: '{"Statement": [{"Action": ["ec2:CreateTags", "ec2:AssociateAddress",
      "logs:CreateLogGroup", "logs:CreateLogStream", "logs:PutLogEvents"], "Effect":
      "Allow", "Resource": "*"}], "Version": "2012-10-17"}'


clusterlifecycle.tmc.cloud.abc.com:
  aws.iam.role.present:
  - resource_id: clusterlifecycle.tmc.cloud.abc.com
  - name: clusterlifecycle.tmc.cloud.abc.com
  - arn: arn:aws:iam::123456789012:role/clusterlifecycle.tmc.cloud.abc.com
  - id: AROAX2FJ77DCRUSPQE5PS
  - path: /
  - description: ''
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {"StringEquals": {"sts:ExternalId": "342fa94e-7f90-5f08-b459-1b0479e9b7cc"}},
      "Effect": "Allow", "Principal": {"AWS": "arn:aws:iam::630260974543:role/whitesand-aws-usw2-mgmt-Kiam-Server-Role"}}],
      "Version": "2012-10-17"}'


clusterlifecycle.tmc.cloud.abc.com-arn:aws:iam::123456789012:policy/controllers.tmc.cloud.abc.com:
  aws.iam.role_policy_attachment.present:
  - role_name: clusterlifecycle.tmc.cloud.abc.com
  - policy_arn: arn:aws:iam::123456789012:policy/controllers.tmc.cloud.abc.com


config-role-us-east-1:
  aws.iam.role.present:
  - resource_id: config-role-us-east-1
  - name: config-role-us-east-1
  - arn: arn:aws:iam::123456789012:role/service-role/config-role-us-east-1
  - id: AROAJXGXUGHLEHWNVOOSY
  - path: /service-role/
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "config.amazonaws.com"}, "Sid": ""}], "Version":
      "2012-10-17"}'


? config-role-us-east-1-arn:aws:iam::123456789012:policy/service-role/config-role-us-east-1_AWSConfigDeliveryPermissions_us-east-1


: aws.iam.role_policy_attachment.present:
  - role_name: config-role-us-east-1
  - policy_arn: arn:aws:iam::123456789012:policy/service-role/config-role-us-east-1_AWSConfigDeliveryPermissions_us-east-1


config-role-us-east-1-arn:aws:iam::aws:policy/service-role/AWSConfigRole:
  aws.iam.role_policy_attachment.present:
  - role_name: config-role-us-east-1
  - policy_arn: arn:aws:iam::aws:policy/service-role/AWSConfigRole


control-plane.tkg.cloud.abc.com:
  aws.iam.role.present:
  - resource_id: control-plane.tkg.cloud.abc.com
  - name: control-plane.tkg.cloud.abc.com
  - arn: arn:aws:iam::123456789012:role/control-plane.tkg.cloud.abc.com
  - id: AROAX2FJ77DCTKKPSKXZF
  - path: /
  - description: ''
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "ec2.amazonaws.com"}}], "Version": "2012-10-17"}'


control-plane.tkg.cloud.abc.com-arn:aws:iam::123456789012:policy/control-plane.tkg.cloud.abc.com:
  aws.iam.role_policy_attachment.present:
  - role_name: control-plane.tkg.cloud.abc.com
  - policy_arn: arn:aws:iam::123456789012:policy/control-plane.tkg.cloud.abc.com


control-plane.tkg.cloud.abc.com-arn:aws:iam::123456789012:policy/controllers.tkg.cloud.abc.com:
  aws.iam.role_policy_attachment.present:
  - role_name: control-plane.tkg.cloud.abc.com
  - policy_arn: arn:aws:iam::123456789012:policy/controllers.tkg.cloud.abc.com


control-plane.tkg.cloud.abc.com-arn:aws:iam::123456789012:policy/nodes.tkg.cloud.abc.com:
  aws.iam.role_policy_attachment.present:
  - role_name: control-plane.tkg.cloud.abc.com
  - policy_arn: arn:aws:iam::123456789012:policy/nodes.tkg.cloud.abc.com


control-plane.tmc.cloud.abc.com:
  aws.iam.role.present:
  - resource_id: control-plane.tmc.cloud.abc.com
  - name: control-plane.tmc.cloud.abc.com
  - arn: arn:aws:iam::123456789012:role/control-plane.tmc.cloud.abc.com
  - id: AROAX2FJ77DC6V5TZK7SS
  - path: /
  - description: ''
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "ec2.amazonaws.com"}}], "Version": "2012-10-17"}'


control-plane.tmc.cloud.abc.com-arn:aws:iam::123456789012:policy/control-plane.tmc.cloud.abc.com:
  aws.iam.role_policy_attachment.present:
  - role_name: control-plane.tmc.cloud.abc.com
  - policy_arn: arn:aws:iam::123456789012:policy/control-plane.tmc.cloud.abc.com


control-plane.tmc.cloud.abc.com-arn:aws:iam::123456789012:policy/nodes.tmc.cloud.abc.com:
  aws.iam.role_policy_attachment.present:
  - role_name: control-plane.tmc.cloud.abc.com
  - policy_arn: arn:aws:iam::123456789012:policy/nodes.tmc.cloud.abc.com


controllers.tkg.cloud.abc.com:
  aws.iam.role.present:
  - resource_id: controllers.tkg.cloud.abc.com
  - name: controllers.tkg.cloud.abc.com
  - arn: arn:aws:iam::123456789012:role/controllers.tkg.cloud.abc.com
  - id: AROAX2FJ77DCW3RYJFY5C
  - path: /
  - description: ''
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "ec2.amazonaws.com"}}], "Version": "2012-10-17"}'


controllers.tkg.cloud.abc.com-arn:aws:iam::123456789012:policy/controllers.tkg.cloud.abc.com:
  aws.iam.role_policy_attachment.present:
  - role_name: controllers.tkg.cloud.abc.com
  - policy_arn: arn:aws:iam::123456789012:policy/controllers.tkg.cloud.abc.com


default-test-temp-xyz-cluster:
  aws.iam.role.present:
  - resource_id: default-test-temp-xyz-cluster
  - name: default-test-temp-xyz-cluster
  - arn: arn:aws:iam::123456789012:role/default-test-temp-xyz-cluster
  - id: AROAX2FJ77DC6SU5DTHYB
  - path: /
  - max_session_duration: 3600
  - tags:
    - Key: Environment
      Value: default
    - Key: Owner
      Value: org1
    - Key: KubernetesCluster
      Value: idem-test
    - Key: COGS
      Value: OPEX
    - Key: Automation
      Value: 'true'
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "xyz.amazonaws.com"}}], "Version": "2012-10-17"}'


ecsInstanceRole:
  aws.iam.role.present:
  - resource_id: ecsInstanceRole
  - name: ecsInstanceRole
  - arn: arn:aws:iam::123456789012:role/ecsInstanceRole
  - id: AROAI6QGH7NNYYR5TDIOC
  - path: /
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "ec2.amazonaws.com"}, "Sid": ""}], "Version":
      "2008-10-17"}'


ecsInstanceRole-arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role:
  aws.iam.role_policy_attachment.present:
  - role_name: ecsInstanceRole
  - policy_arn: arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role


ecsTaskExecutionRole:
  aws.iam.role.present:
  - resource_id: ecsTaskExecutionRole
  - name: ecsTaskExecutionRole
  - arn: arn:aws:iam::123456789012:role/ecsTaskExecutionRole
  - id: AROAX2FJ77DCXK7IXAFHG
  - path: /
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "ecs-tasks.amazonaws.com"}, "Sid": ""}], "Version":
      "2008-10-17"}'


ecsTaskExecutionRole-arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: ecsTaskExecutionRole
  - policy_arn: arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy


ensemble_test:
  aws.iam.user.present:
  - name: ensemble_test
  - resource_id: ensemble_test
  - arn: arn:aws:iam::123456789012:user/ensemble_test
  - path: /
  - tags:
    - Key: iam_user
      Value: ensemble
  - user_name: ensemble_test


ensemble_test-ensemble_primary_acc_policy:
  aws.iam.user_policy.present:
  - resource_id: ensemble_test-ensemble_primary_acc_policy
  - user_name: ensemble_test
  - name: ensemble_primary_acc_policy
  - policy_document: '{"Statement": [{"Action": ["iam:ListAccountAliases"], "Effect":
      "Allow", "Resource": ["*"]}, {"Action": ["ec2:Describe*"], "Effect": "Allow",
      "Resource": "*"}, {"Action": ["logs:Describe*", "logs:Get*", "logs:TestMetricFilter",
      "logs:FilterLogEvents"], "Effect": "Allow", "Resource": "*"}, {"Action": ["organizations:ListAccounts"],
      "Effect": "Allow", "Resource": "*"}, {"Action": "sts:AssumeRole", "Effect":
      "Allow", "Resource": "*"}], "Version": "2012-10-17"}'


extension-jenkins-idem-test:
  aws.iam.user.present:
  - name: extension-jenkins-idem-test
  - resource_id: extension-jenkins-idem-test
  - arn: arn:aws:iam::123456789012:user/xyz/idem-test/extension-jenkins-idem-test
  - path: /xyz/idem-test/
  - tags:
    - Key: COGS
      Value: OPEX
    - Key: Owner
      Value: org1
    - Key: Automation
      Value: 'true'
    - Key: Environment
      Value: test-dev
    - Key: KubernetesCluster
      Value: idem-test
  - user_name: extension-jenkins-idem-test


extension-jenkins-idem-test-extension-jenkins-idem-test:
  aws.iam.user_policy.present:
  - resource_id: extension-jenkins-idem-test-extension-jenkins-idem-test
  - user_name: extension-jenkins-idem-test
  - name: extension-jenkins-idem-test
  - policy_document: '{"Statement": [{"Action": ["xyz:DescribeCluster"], "Effect":
      "Allow", "Resource": "arn:aws:xyz:eu-west-3:123456789012:cluster/idem-test"},
      {"Action": ["sts:AssumeRole"], "Effect": "Allow", "Resource": ["arn:aws:iam::123456789012:role/xyz-idem-test-jenkins"],
      "Sid": ""}, {"Action": "s3:*", "Effect": "Allow", "Resource": ["arn:aws:s3:::ssm-ansible-test-dev",
      "arn:aws:s3:::ssm-ansible-test-dev/*"], "Sid": ""}, {"Action": ["ssm:StartSession"],
      "Condition": {"StringLike": {"ssm:resourceTag/KubernetesCluster": ["idem-test"]}},
      "Effect": "Allow", "Resource": "*"}, {"Action": ["ssm:TerminateSession"], "Effect":
      "Allow", "Resource": "*"}], "Version": "2012-10-17"}'


extension-jenkins-idem-test-extension-jenkins-rolling-upgrade-idem-test:
  aws.iam.user_policy.present:
  - resource_id: extension-jenkins-idem-test-extension-jenkins-rolling-upgrade-idem-test
  - user_name: extension-jenkins-idem-test
  - name: extension-jenkins-rolling-upgrade-idem-test
  - policy_document: '{"Statement": [{"Action": "ec2:Describe*", "Effect": "Allow",
      "Resource": "*"}, {"Action": ["sts:AssumeRole"], "Effect": "Allow", "Resource":
      ["arn:aws:iam::123456789012:role/xyz-idem-test-jenkins"], "Sid": ""}, {"Action":
      ["autoscaling:DeleteTags", "autoscaling:ResumeProcesses", "autoscaling:CreateOrUpdateTags",
      "autoscaling:UpdateAutoScalingGroup", "autoscaling:SuspendProcesses", "autoscaling:TerminateInstanceInAutoScalingGroup"],
      "Condition": {"StringEquals": {"autoscaling:ResourceTag/KubernetesCluster":
      "idem-test"}}, "Effect": "Allow", "Resource": "*", "Sid": ""}, {"Action": ["xyz:UpdateClusterVersion",
      "ec2:DescribeInstances", "ec2:RebootInstances", "autoscaling:DescribeAutoScalingGroups",
      "xyz:DescribeUpdate", "xyz:DescribeCluster", "xyz:ListClusters", "xyz:CreateCluster"],
      "Effect": "Allow", "Resource": "*", "Sid": ""}, {"Action": "ec2:DescribeInstances",
      "Effect": "Allow", "Resource": "*", "Sid": ""}], "Version": "2012-10-17"}'


f2scv1hr-dev-us-east-1-lambdaRole:
  aws.iam.role.present:
  - resource_id: f2scv1hr-dev-us-east-1-lambdaRole
  - name: f2scv1hr-dev-us-east-1-lambdaRole
  - arn: arn:aws:iam::123456789012:role/f2scv1hr-dev-us-east-1-lambdaRole
  - id: AROAX2FJ77DCUAPA7PNZX
  - path: /
  - description: ''
  - max_session_duration: 3600
  - tags:
    - Key: STAGE
      Value: dev
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "lambda.amazonaws.com"}}], "Version": "2012-10-17"}'


f2scv1hr-dev-us-east-1-lambdaRole-f2scv1hr-dev-lambda:
  aws.iam.role_policy.present:
  - resource_id: f2scv1hr-dev-us-east-1-lambdaRole-f2scv1hr-dev-lambda
  - role_name: f2scv1hr-dev-us-east-1-lambdaRole
  - name: f2scv1hr-dev-lambda
  - policy_document: '{"Statement": [{"Action": ["logs:CreateLogStream", "logs:CreateLogGroup"],
      "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:/aws/lambda/f2scv1hr-dev*:*"]},
      {"Action": ["logs:PutLogEvents"], "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:/aws/lambda/f2scv1hr-dev*:*:*"]}],
      "Version": "2012-10-17"}'


go-test-role-4uc6dizk:
  aws.iam.role.present:
  - resource_id: go-test-role-4uc6dizk
  - name: go-test-role-4uc6dizk
  - arn: arn:aws:iam::123456789012:role/service-role/go-test-role-4uc6dizk
  - id: AROAX2FJ77DCUCB3LKFE3
  - path: /service-role/
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "lambda.amazonaws.com"}}], "Version": "2012-10-17"}'


? go-test-role-4uc6dizk-arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-0e827eb6-10ae-40f8-b460-4df21de60081


: aws.iam.role_policy_attachment.present:
  - role_name: go-test-role-4uc6dizk
  - policy_arn: arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-0e827eb6-10ae-40f8-b460-4df21de60081


go-test-role-9mmf25d1:
  aws.iam.role.present:
  - resource_id: go-test-role-9mmf25d1
  - name: go-test-role-9mmf25d1
  - arn: arn:aws:iam::123456789012:role/service-role/go-test-role-9mmf25d1
  - id: AROAX2FJ77DC3X6MKSX3E
  - path: /service-role/
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "lambda.amazonaws.com"}}], "Version": "2012-10-17"}'


? go-test-role-9mmf25d1-arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-2cbcf4ca-8169-413b-81bc-02ca66628d0d


: aws.iam.role_policy_attachment.present:
  - role_name: go-test-role-9mmf25d1
  - policy_arn: arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-2cbcf4ca-8169-413b-81bc-02ca66628d0d


guardduty-event-role:
  aws.iam.role.present:
  - resource_id: guardduty-event-role
  - name: guardduty-event-role
  - arn: arn:aws:iam::123456789012:role/guardduty-event-role
  - id: AROAX2FJ77DCXSLVF6ATC
  - path: /
  - description: ''
  - max_session_duration: 3600
  - tags:
    - Key: ManagedBy
      Value: ConformityEngine
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "events.amazonaws.com"}}], "Version": "2012-10-17"}'


guardduty-event-role-GuardDuty-Event-IAMPolicy:
  aws.iam.role_policy.present:
  - resource_id: guardduty-event-role-GuardDuty-Event-IAMPolicy
  - role_name: guardduty-event-role
  - name: GuardDuty-Event-IAMPolicy
  - policy_document: '{"Statement": [{"Action": "events:PutEvents", "Effect": "Allow",
      "Resource": "arn:aws:events:*:963874769787:event-bus/default"}], "Version":
      "2012-10-17"}'


hello-world-serverless-dev-us-east-1-lambdaRole:
  aws.iam.role.present:
  - resource_id: hello-world-serverless-dev-us-east-1-lambdaRole
  - name: hello-world-serverless-dev-us-east-1-lambdaRole
  - arn: arn:aws:iam::123456789012:role/hello-world-serverless-dev-us-east-1-lambdaRole
  - id: AROAX2FJ77DC75KMSK5OR
  - path: /
  - description: ''
  - max_session_duration: 3600
  - tags:
    - Key: STAGE
      Value: dev
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "lambda.amazonaws.com"}}], "Version": "2012-10-17"}'


hello-world-serverless-dev-us-east-1-lambdaRole-hello-world-serverless-dev-lambda:
  aws.iam.role_policy.present:
  - resource_id: hello-world-serverless-dev-us-east-1-lambdaRole-hello-world-serverless-dev-lambda
  - role_name: hello-world-serverless-dev-us-east-1-lambdaRole
  - name: hello-world-serverless-dev-lambda
  - policy_document: '{"Statement": [{"Action": ["logs:CreateLogStream", "logs:CreateLogGroup"],
      "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:/aws/lambda/hello-world-serverless-dev*:*"]},
      {"Action": ["logs:PutLogEvents"], "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:/aws/lambda/hello-world-serverless-dev*:*:*"]}],
      "Version": "2012-10-17"}'


iam-user-7:
  aws.iam.user.present:
  - name: iam-user-7
  - resource_id: iam-user-7
  - arn: arn:aws:iam::123456789012:user/idem/aws/iam-user-7
  - path: /idem/aws/
  - tags:
    - Key: test-key-6
      Value: test-value-6
    - Key: test-key-5
      Value: test-value-5
    - Key: test-key-4
      Value: test-value-4
    - Key: test-key
      Value: test-value
    - Key: test-key-1
      Value: test-value-1
    - Key: test-key-3
      Value: test-value-3
  - user_name: iam-user-7


idem-fixture-user-18964248-75dd-4765-80b2-100901a8c74e:
  aws.iam.user.present:
  - name: idem-fixture-user-18964248-75dd-4765-80b2-100901a8c74e
  - resource_id: idem-fixture-user-18964248-75dd-4765-80b2-100901a8c74e
  - arn: arn:aws:iam::123456789012:user/idem-fixture-user-18964248-75dd-4765-80b2-100901a8c74e
  - path: /
  - user_name: idem-fixture-user-18964248-75dd-4765-80b2-100901a8c74e


idem-fixture-user-2a75ba7c-bc9b-4b2a-902e-160d999531dc:
  aws.iam.user.present:
  - name: idem-fixture-user-2a75ba7c-bc9b-4b2a-902e-160d999531dc
  - resource_id: idem-fixture-user-2a75ba7c-bc9b-4b2a-902e-160d999531dc
  - arn: arn:aws:iam::123456789012:user/idem-fixture-user-2a75ba7c-bc9b-4b2a-902e-160d999531dc
  - path: /
  - user_name: idem-fixture-user-2a75ba7c-bc9b-4b2a-902e-160d999531dc


idem-fixture-user-482d6d7d-c27d-4f67-86d1-5425398be9c2:
  aws.iam.user.present:
  - name: idem-fixture-user-482d6d7d-c27d-4f67-86d1-5425398be9c2
  - resource_id: idem-fixture-user-482d6d7d-c27d-4f67-86d1-5425398be9c2
  - arn: arn:aws:iam::123456789012:user/idem-fixture-user-482d6d7d-c27d-4f67-86d1-5425398be9c2
  - path: /
  - user_name: idem-fixture-user-482d6d7d-c27d-4f67-86d1-5425398be9c2


idem-fixture-user-6b6158d0-35c5-4855-8b26-512f3963af22:
  aws.iam.user.present:
  - name: idem-fixture-user-6b6158d0-35c5-4855-8b26-512f3963af22
  - resource_id: idem-fixture-user-6b6158d0-35c5-4855-8b26-512f3963af22
  - arn: arn:aws:iam::123456789012:user/idem-fixture-user-6b6158d0-35c5-4855-8b26-512f3963af22
  - path: /
  - user_name: idem-fixture-user-6b6158d0-35c5-4855-8b26-512f3963af22


idem-fixture-xyz-role-1de845b9-5a78-4455-82e5-6b9e8934a472:
  aws.iam.role.present:
  - resource_id: idem-fixture-xyz-role-1de845b9-5a78-4455-82e5-6b9e8934a472
  - name: idem-fixture-xyz-role-1de845b9-5a78-4455-82e5-6b9e8934a472
  - arn: arn:aws:iam::123456789012:role/idem-fixture-xyz-role-1de845b9-5a78-4455-82e5-6b9e8934a472
  - id: AROAX2FJ77DCS2KUCRY2U
  - path: /
  - description: Idem IAM role integration test fixture
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "xyz.amazonaws.com"}}, {"Action": "sts:AssumeRole",
      "Effect": "Allow", "Principal": {"Service": "ec2.amazonaws.com"}}], "Version":
      "2012-10-17"}'


idem-fixture-xyz-role-1de845b9-5a78-4455-82e5-6b9e8934a472-arn:aws:iam::aws:policy/AmazonxyzClusterPolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: idem-fixture-xyz-role-1de845b9-5a78-4455-82e5-6b9e8934a472
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzClusterPolicy


idem-fixture-xyz-role-1de845b9-5a78-4455-82e5-6b9e8934a472-arn:aws:iam::aws:policy/AmazonxyzVPCResourceController:
  aws.iam.role_policy_attachment.present:
  - role_name: idem-fixture-xyz-role-1de845b9-5a78-4455-82e5-6b9e8934a472
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzVPCResourceController


idem-fixture-xyz-role-72a373e1-4e69-422f-93e9-21d1d9097e80:
  aws.iam.role.present:
  - resource_id: idem-fixture-xyz-role-72a373e1-4e69-422f-93e9-21d1d9097e80
  - name: idem-fixture-xyz-role-72a373e1-4e69-422f-93e9-21d1d9097e80
  - arn: arn:aws:iam::123456789012:role/idem-fixture-xyz-role-72a373e1-4e69-422f-93e9-21d1d9097e80
  - id: AROAX2FJ77DC4UR543KD4
  - path: /
  - description: Idem IAM role integration test fixture
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "xyz.amazonaws.com"}}, {"Action": "sts:AssumeRole",
      "Effect": "Allow", "Principal": {"Service": "ec2.amazonaws.com"}}], "Version":
      "2012-10-17"}'


idem-fixture-xyz-role-72a373e1-4e69-422f-93e9-21d1d9097e80-arn:aws:iam::aws:policy/AmazonxyzClusterPolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: idem-fixture-xyz-role-72a373e1-4e69-422f-93e9-21d1d9097e80
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzClusterPolicy


idem-fixture-xyz-role-72a373e1-4e69-422f-93e9-21d1d9097e80-arn:aws:iam::aws:policy/AmazonxyzVPCResourceController:
  aws.iam.role_policy_attachment.present:
  - role_name: idem-fixture-xyz-role-72a373e1-4e69-422f-93e9-21d1d9097e80
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzVPCResourceController


idem-fixture-xyz-role-8d72ec1d-94de-4bba-9b44-0b789b83fcd4:
  aws.iam.role.present:
  - resource_id: idem-fixture-xyz-role-8d72ec1d-94de-4bba-9b44-0b789b83fcd4
  - name: idem-fixture-xyz-role-8d72ec1d-94de-4bba-9b44-0b789b83fcd4
  - arn: arn:aws:iam::123456789012:role/idem-fixture-xyz-role-8d72ec1d-94de-4bba-9b44-0b789b83fcd4
  - id: AROAX2FJ77DCQI6G2X55L
  - path: /
  - description: Idem IAM role integration test fixture
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "xyz.amazonaws.com"}}, {"Action": "sts:AssumeRole",
      "Effect": "Allow", "Principal": {"Service": "ec2.amazonaws.com"}}], "Version":
      "2012-10-17"}'


idem-fixture-xyz-role-8d72ec1d-94de-4bba-9b44-0b789b83fcd4-arn:aws:iam::aws:policy/AmazonxyzClusterPolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: idem-fixture-xyz-role-8d72ec1d-94de-4bba-9b44-0b789b83fcd4
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzClusterPolicy


idem-fixture-xyz-role-8d72ec1d-94de-4bba-9b44-0b789b83fcd4-arn:aws:iam::aws:policy/AmazonxyzVPCResourceController:
  aws.iam.role_policy_attachment.present:
  - role_name: idem-fixture-xyz-role-8d72ec1d-94de-4bba-9b44-0b789b83fcd4
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzVPCResourceController


idem-s3:
  aws.iam.user.present:
  - name: idem-s3
  - resource_id: idem-s3
  - arn: arn:aws:iam::123456789012:user/idem-s3
  - path: /
  - tags:
    - Key: user
      Value: Lakshmi Mojjada
  - user_name: idem-s3


idem-s3-idem-s332:
  aws.iam.user_policy.present:
  - resource_id: idem-s3-idem-s332
  - user_name: idem-s3
  - name: idem-s332
  - policy_document: '{"Statement": [{"Action": ["s3:*", "s3-object-lambda:*"], "Effect":
      "Allow", "Resource": "arn:aws:s3:::idem-s3-test"}], "Version": "2012-10-17"}'


idem-test-role-5d1c7b2a-c892-4cd8-afe9-14a0478cdcc9:
  aws.iam.role.present:
  - resource_id: idem-test-role-5d1c7b2a-c892-4cd8-afe9-14a0478cdcc9
  - name: idem-test-role-5d1c7b2a-c892-4cd8-afe9-14a0478cdcc9
  - arn: arn:aws:iam::123456789012:role/idem-test-role-5d1c7b2a-c892-4cd8-afe9-14a0478cdcc9
  - id: AROAX2FJ77DCR4EKXW32Q
  - path: /
  - description: Idem IAM role integration test
  - max_session_duration: 3700
  - tags:
    - Key: Name
      Value: idem-test-role-5d1c7b2a-c892-4cd8-afe9-14a0478cdcc9
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "spot.amazonaws.com"}}], "Version": "2012-10-17"}'


idem-test-role-ad2cea2a-01fa-42fc-833d-c990fa59f478:
  aws.iam.role.present:
  - resource_id: idem-test-role-ad2cea2a-01fa-42fc-833d-c990fa59f478
  - name: idem-test-role-ad2cea2a-01fa-42fc-833d-c990fa59f478
  - arn: arn:aws:iam::123456789012:role/idem-test-role-ad2cea2a-01fa-42fc-833d-c990fa59f478
  - id: AROAX2FJ77DC5BZ3LC232
  - path: /
  - description: Idem IAM role integration test
  - max_session_duration: 3700
  - tags:
    - Key: Name
      Value: idem-test-role-ad2cea2a-01fa-42fc-833d-c990fa59f478
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "spot.amazonaws.com"}}], "Version": "2012-10-17"}'


idem-test-role-cf6a414c-4739-4fe8-8905-edf0575cc543:
  aws.iam.role.present:
  - resource_id: idem-test-role-cf6a414c-4739-4fe8-8905-edf0575cc543
  - name: idem-test-role-cf6a414c-4739-4fe8-8905-edf0575cc543
  - arn: arn:aws:iam::123456789012:role/idem-test-role-cf6a414c-4739-4fe8-8905-edf0575cc543
  - id: AROAX2FJ77DCSALRGM4OP
  - path: /
  - description: Idem IAM role test description updated
  - max_session_duration: 3800
  - tags:
    - Key: idem-test-iam-key-048b3bc6-1e7e-4ed4-b599-d570c8ba2579
      Value: idem-test-iam-value-fad4ba66-2e1a-4257-87a8-88ffe8e96edd
    - Key: Name
      Value: idem-test-role-cf6a414c-4739-4fe8-8905-edf0575cc543
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "spot.amazonaws.com"}}], "Version": "2012-10-17"}'


idem-test-temp-xyz-cluster:
  aws.iam.role.present:
  - resource_id: idem-test-temp-xyz-cluster
  - name: idem-test-temp-xyz-cluster
  - arn: arn:aws:iam::123456789012:role/idem-test-temp-xyz-cluster
  - id: AROAX2FJ77DC2JM67OZSY
  - path: /
  - max_session_duration: 3600
  - tags:
    - Key: Owner
      Value: org1
    - Key: COGS
      Value: OPEX
    - Key: Environment
      Value: test-dev
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Automation
      Value: 'true'
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "xyz.amazonaws.com"}}], "Version": "2012-10-17"}'


idem-test-temp-xyz-cluster-arn:aws:iam::aws:policy/AmazonxyzClusterPolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: idem-test-temp-xyz-cluster
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzClusterPolicy


idem-test-temp-xyz-cluster-arn:aws:iam::aws:policy/AmazonxyzServicePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: idem-test-temp-xyz-cluster
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzServicePolicy


idem-test-temp-xyz-cluster-node:
  aws.iam.role.present:
  - resource_id: idem-test-temp-xyz-cluster-node
  - name: idem-test-temp-xyz-cluster-node
  - arn: arn:aws:iam::123456789012:role/idem-test-temp-xyz-cluster-node
  - id: AROAX2FJ77DC6POVG7T5L
  - path: /
  - max_session_duration: 3600
  - tags:
    - Key: Automation
      Value: 'true'
    - Key: Environment
      Value: test-dev
    - Key: KubernetesCluster
      Value: idem-test
    - Key: COGS
      Value: OPEX
    - Key: Owner
      Value: org1
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "ec2.amazonaws.com"}}], "Version": "2012-10-17"}'


idem-test-temp-xyz-cluster-node-Amazon_EBS_CSI_Driver:
  aws.iam.role_policy.present:
  - resource_id: idem-test-temp-xyz-cluster-node-Amazon_EBS_CSI_Driver
  - role_name: idem-test-temp-xyz-cluster-node
  - name: Amazon_EBS_CSI_Driver
  - policy_document: '{"Statement": [{"Action": ["ec2:AttachVolume", "ec2:CreateSnapshot",
      "ec2:CreateTags", "ec2:CreateVolume", "ec2:DeleteSnapshot", "ec2:DeleteTags",
      "ec2:DeleteVolume", "ec2:DescribeAvailabilityZones", "ec2:DescribeInstances",
      "ec2:DescribeSnapshots", "ec2:DescribeTags", "ec2:DescribeVolumes", "ec2:DescribeVolumesModifications",
      "ec2:DetachVolume", "ec2:ModifyVolume"], "Effect": "Allow", "Resource": "*"}],
      "Version": "2012-10-17"}'


idem-test-temp-xyz-cluster-node-arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly:
  aws.iam.role_policy_attachment.present:
  - role_name: idem-test-temp-xyz-cluster-node
  - policy_arn: arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly


idem-test-temp-xyz-cluster-node-arn:aws:iam::aws:policy/AmazonEC2ReadOnlyAccess:
  aws.iam.role_policy_attachment.present:
  - role_name: idem-test-temp-xyz-cluster-node
  - policy_arn: arn:aws:iam::aws:policy/AmazonEC2ReadOnlyAccess


idem-test-temp-xyz-cluster-node-arn:aws:iam::aws:policy/AmazonElasticFileSystemFullAccess:
  aws.iam.role_policy_attachment.present:
  - role_name: idem-test-temp-xyz-cluster-node
  - policy_arn: arn:aws:iam::aws:policy/AmazonElasticFileSystemFullAccess


idem-test-temp-xyz-cluster-node-arn:aws:iam::aws:policy/AmazonxyzWorkerNodePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: idem-test-temp-xyz-cluster-node
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzWorkerNodePolicy


idem-test-temp-xyz-cluster-node-arn:aws:iam::aws:policy/Amazonxyz_CNI_Policy:
  aws.iam.role_policy_attachment.present:
  - role_name: idem-test-temp-xyz-cluster-node
  - policy_arn: arn:aws:iam::aws:policy/Amazonxyz_CNI_Policy


idem-test-temp-xyz-cluster-node-arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM:
  aws.iam.role_policy_attachment.present:
  - role_name: idem-test-temp-xyz-cluster-node
  - policy_arn: arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM


idem-test-temp-xyz-cluster-node-credstash_xyz_idem-test_access_policy:
  aws.iam.role_policy.present:
  - resource_id: idem-test-temp-xyz-cluster-node-credstash_xyz_idem-test_access_policy
  - role_name: idem-test-temp-xyz-cluster-node
  - name: credstash_xyz_idem-test_access_policy
  - policy_document: '{"Statement": [{"Action": ["kms:GenerateDataKey", "kms:Decrypt"],
      "Effect": "Allow", "Resource": "arn:aws:kms:eu-west-3:123456789012:key/8ac5f341-fd1c-4e9d-9596-8f844dba5cc8"},
      {"Action": ["dynamodb:PutItem", "dynamodb:GetItem", "dynamodb:Query", "dynamodb:Scan"],
      "Effect": "Allow", "Resource": "arn:aws:dynamodb:eu-west-3:123456789012:table/xyz-idem-test-credential-store"}],
      "Version": "2012-10-17"}'


idem_aws_demo_user:
  aws.iam.user.present:
  - name: idem_aws_demo_user
  - resource_id: idem_aws_demo_user
  - arn: arn:aws:iam::123456789012:user/idem_aws_demo_user
  - path: /
  - user_name: idem_aws_demo_user


idem_aws_demo_user-idem-s3:
  aws.iam.user_policy.present:
  - resource_id: idem_aws_demo_user-idem-s3
  - user_name: idem_aws_demo_user
  - name: idem-s3
  - policy_document: '{"Statement": [{"Action": ["s3:*", "s3-object-lambda:*"], "Effect":
      "Deny", "Resource": "arn:aws:s3:::idem-s3-test"}], "Version": "2012-10-17"}'


idem_aws_demo_user2:
  aws.iam.user.present:
  - name: idem_aws_demo_user2
  - resource_id: idem_aws_demo_user2
  - arn: arn:aws:iam::123456789012:user/idem_aws_demo_user2
  - path: /
  - user_name: idem_aws_demo_user2


idem_aws_demo_user5:
  aws.iam.user.present:
  - name: idem_aws_demo_user5
  - resource_id: idem_aws_demo_user5
  - arn: arn:aws:iam::123456789012:user/idem_aws_demo_user5
  - path: /
  - user_name: idem_aws_demo_user5


idem_aws_demo_user_0:
  aws.iam.user.present:
  - name: idem_aws_demo_user_0
  - resource_id: idem_aws_demo_user_0
  - arn: arn:aws:iam::123456789012:user/idem_aws_demo_user_0
  - path: /
  - user_name: idem_aws_demo_user_0


java-test-role-wex7ne6b:
  aws.iam.role.present:
  - resource_id: java-test-role-wex7ne6b
  - name: java-test-role-wex7ne6b
  - arn: arn:aws:iam::123456789012:role/service-role/java-test-role-wex7ne6b
  - id: AROAX2FJ77DC5A2VOUCLT
  - path: /service-role/
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "lambda.amazonaws.com"}}], "Version": "2012-10-17"}'


? java-test-role-wex7ne6b-arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-f35de733-79b9-4ffc-9ef2-1afaa15f52c8


: aws.iam.role_policy_attachment.present:
  - role_name: java-test-role-wex7ne6b
  - policy_arn: arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-f35de733-79b9-4ffc-9ef2-1afaa15f52c8


ki5roed2-dev-us-east-1-lambdaRole:
  aws.iam.role.present:
  - resource_id: ki5roed2-dev-us-east-1-lambdaRole
  - name: ki5roed2-dev-us-east-1-lambdaRole
  - arn: arn:aws:iam::123456789012:role/ki5roed2-dev-us-east-1-lambdaRole
  - id: AROAX2FJ77DCXK5X2PMKC
  - path: /
  - description: ''
  - max_session_duration: 3600
  - tags:
    - Key: STAGE
      Value: dev
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "lambda.amazonaws.com"}}], "Version": "2012-10-17"}'


ki5roed2-dev-us-east-1-lambdaRole-ki5roed2-dev-lambda:
  aws.iam.role_policy.present:
  - resource_id: ki5roed2-dev-us-east-1-lambdaRole-ki5roed2-dev-lambda
  - role_name: ki5roed2-dev-us-east-1-lambdaRole
  - name: ki5roed2-dev-lambda
  - policy_document: '{"Statement": [{"Action": ["logs:CreateLogStream", "logs:CreateLogGroup"],
      "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:/aws/lambda/ki5roed2-dev*:*"]},
      {"Action": ["logs:PutLogEvents"], "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:/aws/lambda/ki5roed2-dev*:*:*"]}],
      "Version": "2012-10-17"}'


krisi-temp:
  aws.iam.role.present:
  - resource_id: krisi-temp
  - name: krisi-temp
  - arn: arn:aws:iam::123456789012:role/krisi-temp
  - id: AROAX2FJ77DC6KLXMCMPY
  - path: /
  - description: Allow obtaining and decrypting parameters in the user-data of an
      instance
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "ec2.amazonaws.com"}}], "Version": "2012-10-17"}'


krisi-temp-arn:aws:iam::123456789012:policy/get-encrypted-parameter:
  aws.iam.role_policy_attachment.present:
  - role_name: krisi-temp
  - policy_arn: arn:aws:iam::123456789012:policy/get-encrypted-parameter


lambdaExecutionRole:
  aws.iam.role.present:
  - resource_id: lambdaExecutionRole
  - name: lambdaExecutionRole
  - arn: arn:aws:iam::123456789012:role/service-role/lambdaExecutionRole
  - id: AROAIBJ5EOBFRV5GYFGC4
  - path: /service-role/
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "lambda.amazonaws.com"}}], "Version": "2012-10-17"}'


lambdaExecutionRole-arn:aws:iam::123456789012:policy/EC2ListResources:
  aws.iam.role_policy_attachment.present:
  - role_name: lambdaExecutionRole
  - policy_arn: arn:aws:iam::123456789012:policy/EC2ListResources


lambdaExecutionRole-arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole:
  aws.iam.role_policy_attachment.present:
  - role_name: lambdaExecutionRole
  - policy_arn: arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole


masters.training.k8s.local:
  aws.iam.role.present:
  - resource_id: masters.training.k8s.local
  - name: masters.training.k8s.local
  - arn: arn:aws:iam::123456789012:role/masters.training.k8s.local
  - id: AROAJNJFDWDHVLENZXBSK
  - path: /
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "ec2.amazonaws.com"}}], "Version": "2012-10-17"}'


masters.training.k8s.local-masters.training.k8s.local:
  aws.iam.role_policy.present:
  - resource_id: masters.training.k8s.local-masters.training.k8s.local
  - role_name: masters.training.k8s.local
  - name: masters.training.k8s.local
  - policy_document: '{"Statement": [{"Action": ["ec2:DescribeInstances", "ec2:DescribeRouteTables",
      "ec2:DescribeSecurityGroups", "ec2:DescribeSubnets", "ec2:DescribeVolumes"],
      "Effect": "Allow", "Resource": ["*"], "Sid": "kopsK8sEC2MasterPermsDescribeResources"},
      {"Action": ["ec2:CreateRoute", "ec2:CreateSecurityGroup", "ec2:CreateTags",
      "ec2:CreateVolume", "ec2:ModifyInstanceAttribute"], "Effect": "Allow", "Resource":
      ["*"], "Sid": "kopsK8sEC2MasterPermsAllResources"}, {"Action": ["ec2:AttachVolume",
      "ec2:AuthorizeSecurityGroupIngress", "ec2:DeleteRoute", "ec2:DeleteSecurityGroup",
      "ec2:DeleteVolume", "ec2:DetachVolume", "ec2:RevokeSecurityGroupIngress"], "Condition":
      {"StringEquals": {"ec2:ResourceTag/KubernetesCluster": "training.k8s.local"}},
      "Effect": "Allow", "Resource": ["*"], "Sid": "kopsK8sEC2MasterPermsTaggedResources"},
      {"Action": ["autoscaling:DescribeAutoScalingGroups", "autoscaling:DescribeLaunchConfigurations",
      "autoscaling:GetAsgForInstance"], "Effect": "Allow", "Resource": ["*"], "Sid":
      "kopsK8sASMasterPermsAllResources"}, {"Action": ["autoscaling:SetDesiredCapacity",
      "autoscaling:TerminateInstanceInAutoScalingGroup", "autoscaling:UpdateAutoScalingGroup"],
      "Condition": {"StringEquals": {"autoscaling:ResourceTag/KubernetesCluster":
      "training.k8s.local"}}, "Effect": "Allow", "Resource": ["*"], "Sid": "kopsK8sASMasterPermsTaggedResources"},
      {"Action": ["elasticloadbalancing:AttachLoadBalancerToSubnets", "elasticloadbalancing:ApplySecurityGroupsToLoadBalancer",
      "elasticloadbalancing:CreateLoadBalancer", "elasticloadbalancing:CreateLoadBalancerPolicy",
      "elasticloadbalancing:CreateLoadBalancerListeners", "elasticloadbalancing:ConfigureHealthCheck",
      "elasticloadbalancing:DeleteLoadBalancer", "elasticloadbalancing:DeleteLoadBalancerListeners",
      "elasticloadbalancing:DescribeLoadBalancers", "elasticloadbalancing:DescribeLoadBalancerAttributes",
      "elasticloadbalancing:DetachLoadBalancerFromSubnets", "elasticloadbalancing:DeregisterInstancesFromLoadBalancer",
      "elasticloadbalancing:ModifyLoadBalancerAttributes", "elasticloadbalancing:RegisterInstancesWithLoadBalancer",
      "elasticloadbalancing:SetLoadBalancerPoliciesForBackendServer"], "Effect": "Allow",
      "Resource": ["*"], "Sid": "kopsK8sELBMasterPermsRestrictive"}, {"Action": ["iam:ListServerCertificates",
      "iam:GetServerCertificate"], "Effect": "Allow", "Resource": ["*"], "Sid": "kopsMasterCertIAMPerms"},
      {"Action": ["s3:GetBucketLocation", "s3:ListBucket"], "Effect": "Allow", "Resource":
      ["arn:aws:s3:::training.k8s.local"], "Sid": "kopsK8sS3GetListBucket"}, {"Action":
      ["s3:Get*"], "Effect": "Allow", "Resource": "arn:aws:s3:::training.k8s.local/training.k8s.local/*",
      "Sid": "kopsK8sS3MasterBucketFullGet"}, {"Action": ["ecr:GetAuthorizationToken",
      "ecr:BatchCheckLayerAvailability", "ecr:GetDownloadUrlForLayer", "ecr:GetRepositoryPolicy",
      "ecr:DescribeRepositories", "ecr:ListImages", "ecr:BatchGetImage"], "Effect":
      "Allow", "Resource": ["*"], "Sid": "kopsK8sECR"}], "Version": "2012-10-17"}'


migrationhub-discovery:
  aws.iam.role.present:
  - resource_id: migrationhub-discovery
  - name: migrationhub-discovery
  - arn: arn:aws:iam::123456789012:role/service-role/migrationhub-discovery
  - id: AROAX2FJ77DCTS5IBOACY
  - path: /service-role/
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "migrationhub.amazonaws.com"}}], "Version":
      "2012-10-17"}'


migrationhub-discovery-arn:aws:iam::aws:policy/service-role/AWSMigrationHubDiscoveryAccess:
  aws.iam.role_policy_attachment.present:
  - role_name: migrationhub-discovery
  - policy_arn: arn:aws:iam::aws:policy/service-role/AWSMigrationHubDiscoveryAccess


my-s3-function-role:
  aws.iam.role.present:
  - resource_id: my-s3-function-role
  - name: my-s3-function-role
  - arn: arn:aws:iam::123456789012:role/service-role/my-s3-function-role
  - id: AROAX2FJ77DCXJ6MMTVQC
  - path: /service-role/
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "lambda.amazonaws.com"}}], "Version": "2012-10-17"}'


? my-s3-function-role-arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-77be7a89-c9ca-49c3-aac0-5d97de6b022a


: aws.iam.role_policy_attachment.present:
  - role_name: my-s3-function-role
  - policy_arn: arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-77be7a89-c9ca-49c3-aac0-5d97de6b022a


? my-s3-function-role-arn:aws:iam::123456789012:policy/service-role/AWSLambdaS3ExecutionRole-7507ff52-f9ad-40bf-8f91-bcfc501db9e6


: aws.iam.role_policy_attachment.present:
  - role_name: my-s3-function-role
  - policy_arn: arn:aws:iam::123456789012:policy/service-role/AWSLambdaS3ExecutionRole-7507ff52-f9ad-40bf-8f91-bcfc501db9e6


myFirstAPI-role-py7jxvn3:
  aws.iam.role.present:
  - resource_id: myFirstAPI-role-py7jxvn3
  - name: myFirstAPI-role-py7jxvn3
  - arn: arn:aws:iam::123456789012:role/service-role/myFirstAPI-role-py7jxvn3
  - id: AROAX2FJ77DCXBLUKYW2K
  - path: /service-role/
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "lambda.amazonaws.com"}}], "Version": "2012-10-17"}'


? myFirstAPI-role-py7jxvn3-arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-f28418d0-fcc1-413a-9443-7eda82a55427


: aws.iam.role_policy_attachment.present:
  - role_name: myFirstAPI-role-py7jxvn3
  - policy_arn: arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-f28418d0-fcc1-413a-9443-7eda82a55427


myTEST-role-ovnuxvqo:
  aws.iam.role.present:
  - resource_id: myTEST-role-ovnuxvqo
  - name: myTEST-role-ovnuxvqo
  - arn: arn:aws:iam::123456789012:role/service-role/myTEST-role-ovnuxvqo
  - id: AROAX2FJ77DCTPHMDE345
  - path: /service-role/
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "lambda.amazonaws.com"}}], "Version": "2012-10-17"}'


? myTEST-role-ovnuxvqo-arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-481e19cb-d70c-47d1-9597-ca85ee2958d2


: aws.iam.role_policy_attachment.present:
  - role_name: myTEST-role-ovnuxvqo
  - policy_arn: arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-481e19cb-d70c-47d1-9597-ca85ee2958d2


my_new_role:
  aws.iam.role.present:
  - resource_id: my_new_role
  - name: my_new_role
  - arn: arn:aws:iam::123456789012:role/my_new_role
  - id: AROAJJGC2M32V3S7ULRFA
  - path: /
  - description: Allows EC2 instances to call AWS services on your behalf.
  - max_session_duration: 3600
  - tags:
    - Key: test-1
      Value: test-val-1
    - Key: test-2
      Value: test-val-2
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "spotfleet.amazonaws.com"}}], "Version": "2012-10-17"}'


my_new_role-arn:aws:iam::aws:policy/service-role/AmazonEC2SpotFleetTaggingRole:
  aws.iam.role_policy_attachment.present:
  - role_name: my_new_role
  - policy_arn: arn:aws:iam::aws:policy/service-role/AmazonEC2SpotFleetTaggingRole


nodes.tkg.cloud.abc.com:
  aws.iam.role.present:
  - resource_id: nodes.tkg.cloud.abc.com
  - name: nodes.tkg.cloud.abc.com
  - arn: arn:aws:iam::123456789012:role/nodes.tkg.cloud.abc.com
  - id: AROAX2FJ77DC75PUXUSTB
  - path: /
  - description: ''
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "ec2.amazonaws.com"}}], "Version": "2012-10-17"}'


nodes.tkg.cloud.abc.com-arn:aws:iam::123456789012:policy/nodes.tkg.cloud.abc.com:
  aws.iam.role_policy_attachment.present:
  - role_name: nodes.tkg.cloud.abc.com
  - policy_arn: arn:aws:iam::123456789012:policy/nodes.tkg.cloud.abc.com


nodes.tmc.cloud.abc.com:
  aws.iam.role.present:
  - resource_id: nodes.tmc.cloud.abc.com
  - name: nodes.tmc.cloud.abc.com
  - arn: arn:aws:iam::123456789012:role/nodes.tmc.cloud.abc.com
  - id: AROAX2FJ77DC6H2KFTNNS
  - path: /
  - description: ''
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "ec2.amazonaws.com"}}], "Version": "2012-10-17"}'


nodes.tmc.cloud.abc.com-arn:aws:iam::123456789012:policy/nodes.tmc.cloud.abc.com:
  aws.iam.role_policy_attachment.present:
  - role_name: nodes.tmc.cloud.abc.com
  - policy_arn: arn:aws:iam::123456789012:policy/nodes.tmc.cloud.abc.com


nodes.training.k8s.local:
  aws.iam.role.present:
  - resource_id: nodes.training.k8s.local
  - name: nodes.training.k8s.local
  - arn: arn:aws:iam::123456789012:role/nodes.training.k8s.local
  - id: AROAJQCCTG247P2YJ3Y5U
  - path: /
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "ec2.amazonaws.com"}}], "Version": "2012-10-17"}'


nodes.training.k8s.local-nodes.training.k8s.local:
  aws.iam.role_policy.present:
  - resource_id: nodes.training.k8s.local-nodes.training.k8s.local
  - role_name: nodes.training.k8s.local
  - name: nodes.training.k8s.local
  - policy_document: '{"Statement": [{"Action": ["ec2:DescribeInstances"], "Effect":
      "Allow", "Resource": ["*"], "Sid": "kopsK8sEC2NodePerms"}, {"Action": ["s3:GetBucketLocation",
      "s3:ListBucket"], "Effect": "Allow", "Resource": ["arn:aws:s3:::training.k8s.local"],
      "Sid": "kopsK8sS3GetListBucket"}, {"Action": ["s3:Get*"], "Effect": "Allow",
      "Resource": ["arn:aws:s3:::training.k8s.local/training.k8s.local/addons/*",
      "arn:aws:s3:::training.k8s.local/training.k8s.local/cluster.spec", "arn:aws:s3:::training.k8s.local/training.k8s.local/config",
      "arn:aws:s3:::training.k8s.local/training.k8s.local/instancegroup/*", "arn:aws:s3:::training.k8s.local/training.k8s.local/pki/issued/*",
      "arn:aws:s3:::training.k8s.local/training.k8s.local/pki/private/kube-proxy/*",
      "arn:aws:s3:::training.k8s.local/training.k8s.local/pki/private/kubelet/*",
      "arn:aws:s3:::training.k8s.local/training.k8s.local/pki/ssh/*", "arn:aws:s3:::training.k8s.local/training.k8s.local/secrets/dockerconfig"],
      "Sid": "kopsK8sS3NodeBucketSelectiveGet"}, {"Action": ["ecr:GetAuthorizationToken",
      "ecr:BatchCheckLayerAvailability", "ecr:GetDownloadUrlForLayer", "ecr:GetRepositoryPolicy",
      "ecr:DescribeRepositories", "ecr:ListImages", "ecr:BatchGetImage"], "Effect":
      "Allow", "Resource": ["*"], "Sid": "kopsK8sECR"}], "Version": "2012-10-17"}'


org1_cluster01_potato_dev_k8s_admins:
  aws.iam.role.present:
  - resource_id: org1_cluster01_potato_dev_k8s_admins
  - name: org1_cluster01_potato_dev_k8s_admins
  - arn: arn:aws:iam::123456789012:role/org1_cluster01_potato_dev_k8s_admins
  - id: AROAX2FJ77DCXVB3S3YEU
  - path: /
  - max_session_duration: 43200
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRoleWithWebIdentity",
      "Condition": {"ForAnyValue:StringEquals": {"org1.workspaceair.com/SAAS/auth:aud":
      "org1_cluster01_potato_dev_k8s_admins"}}, "Effect": "Allow", "Principal": {"Federated":
      "arn:aws:iam::123456789012:oidc-provider/org1.workspaceair.com/SAAS/auth"}}],
      "Version": "2012-10-17"}'


org1_cluster01_potato_dev_k8s_admins-arn:aws:iam::123456789012:policy/org1_cluster01_potato_dev_k8s_admins:
  aws.iam.role_policy_attachment.present:
  - role_name: org1_cluster01_potato_dev_k8s_admins
  - policy_arn: arn:aws:iam::123456789012:policy/org1_cluster01_potato_dev_k8s_admins


org1_cluster01_potato_dev_k8s_readonly:
  aws.iam.role.present:
  - resource_id: org1_cluster01_potato_dev_k8s_readonly
  - name: org1_cluster01_potato_dev_k8s_readonly
  - arn: arn:aws:iam::123456789012:role/org1_cluster01_potato_dev_k8s_readonly
  - id: AROAX2FJ77DCTTZZ6XA5T
  - path: /
  - max_session_duration: 43200
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRoleWithWebIdentity",
      "Condition": {"ForAnyValue:StringEquals": {"org1.workspaceair.com/SAAS/auth:aud":
      "org1_cluster01_potato_dev_k8s_readonly"}}, "Effect": "Allow", "Principal":
      {"Federated": "arn:aws:iam::123456789012:oidc-provider/org1.workspaceair.com/SAAS/auth"}}],
      "Version": "2012-10-17"}'


org1_cluster01_potato_dev_k8s_readonly-arn:aws:iam::123456789012:policy/org1_cluster01_potato_dev_org1_k8s_readonly:
  aws.iam.role_policy_attachment.present:
  - role_name: org1_cluster01_potato_dev_k8s_readonly
  - policy_arn: arn:aws:iam::123456789012:policy/org1_cluster01_potato_dev_org1_k8s_readonly


org1_idem-test_vidmGroup_all_profile_k8s_admins:
  aws.iam.role.present:
  - resource_id: org1_idem-test_vidmGroup_all_profile_k8s_admins
  - name: org1_idem-test_vidmGroup_all_profile_k8s_admins
  - arn: arn:aws:iam::123456789012:role/org1_idem-test_vidmGroup_all_profile_k8s_admins
  - id: AROAX2FJ77DCQMVVOYGPC
  - path: /
  - max_session_duration: 43200
  - tags:
    - Key: Environment
      Value: default
    - Key: Owner
      Value: org1
    - Key: KubernetesCluster
      Value: idem-test
    - Key: COGS
      Value: OPEX
    - Key: Automation
      Value: 'true'
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRoleWithWebIdentity",
      "Condition": {"ForAnyValue:StringEquals": {"org1.workspaceair.com/SAAS/auth:aud":
      "org1_idem-test_vidmGroup_all_profile_k8s_admins"}}, "Effect": "Allow", "Principal":
      {"Federated": "arn:aws:iam::my_account:oidc-provider/org1.workspaceair.com/SAAS/auth"}}],
      "Version": "2012-10-17"}'


org1_idem_test_potato_dev_k8s_admins:
  aws.iam.role.present:
  - resource_id: org1_idem_test_potato_dev_k8s_admins
  - name: org1_idem_test_potato_dev_k8s_admins
  - arn: arn:aws:iam::123456789012:role/org1_idem_test_potato_dev_k8s_admins
  - id: AROAX2FJ77DCRFA2KD4FU
  - path: /
  - max_session_duration: 43200
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRoleWithWebIdentity",
      "Condition": {"ForAnyValue:StringEquals": {"org1.workspaceair.com/SAAS/auth:aud":
      "org1_idem_test_potato_dev_k8s_admins"}}, "Effect": "Allow", "Principal": {"Federated":
      "arn:aws:iam::123456789012:oidc-provider/org1.workspaceair.com/SAAS/auth"}}],
      "Version": "2012-10-17"}'


org1_idem_test_potato_dev_k8s_admins-arn:aws:iam::123456789012:policy/org1_cluster01_potato_dev_k8s_admins:
  aws.iam.role_policy_attachment.present:
  - role_name: org1_idem_test_potato_dev_k8s_admins
  - policy_arn: arn:aws:iam::123456789012:policy/org1_cluster01_potato_dev_k8s_admins


org1_idem_test_potato_dev_k8s_admins-arn:aws:iam::123456789012:policy/org1_idem_test_potato_dev_k8s_admins:
  aws.iam.role_policy_attachment.present:
  - role_name: org1_idem_test_potato_dev_k8s_admins
  - policy_arn: arn:aws:iam::123456789012:policy/org1_idem_test_potato_dev_k8s_admins


org1_idem_test_potato_dev_k8s_readonly:
  aws.iam.role.present:
  - resource_id: org1_idem_test_potato_dev_k8s_readonly
  - name: org1_idem_test_potato_dev_k8s_readonly
  - arn: arn:aws:iam::123456789012:role/org1_idem_test_potato_dev_k8s_readonly
  - id: AROAX2FJ77DC4TGQSSUEE
  - path: /
  - max_session_duration: 43200
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRoleWithWebIdentity",
      "Condition": {"ForAnyValue:StringEquals": {"org1.workspaceair.com/SAAS/auth:aud":
      "org1_idem_test_potato_dev_k8s_readonly"}}, "Effect": "Allow", "Principal":
      {"Federated": "arn:aws:iam::123456789012:oidc-provider/org1.workspaceair.com/SAAS/auth"}}],
      "Version": "2012-10-17"}'


org1_idem_test_potato_dev_k8s_readonly-arn:aws:iam::123456789012:policy/org1_cluster01_potato_dev_org1_k8s_readonly:
  aws.iam.role_policy_attachment.present:
  - role_name: org1_idem_test_potato_dev_k8s_readonly
  - policy_arn: arn:aws:iam::123456789012:policy/org1_cluster01_potato_dev_org1_k8s_readonly


org1_idem_test_potato_dev_k8s_readonly-arn:aws:iam::123456789012:policy/org1_idem_test_potato_dev_org1_k8s_readonly:
  aws.iam.role_policy_attachment.present:
  - role_name: org1_idem_test_potato_dev_k8s_readonly
  - policy_arn: arn:aws:iam::123456789012:policy/org1_idem_test_potato_dev_org1_k8s_readonly


pashmantak@abc.com:
  aws.iam.user.present:
  - name: pashmantak@abc.com
  - resource_id: pashmantak@abc.com
  - arn: arn:aws:iam::123456789012:user/pashmantak@abc.com
  - path: /
  - tags:
    - Key: app
      Value: guardrails
    - Key: tenant
      Value: pcg
  - user_name: pashmantak@abc.com


powershell-role-gj7p5czv:
  aws.iam.role.present:
  - resource_id: powershell-role-gj7p5czv
  - name: powershell-role-gj7p5czv
  - arn: arn:aws:iam::123456789012:role/service-role/powershell-role-gj7p5czv
  - id: AROAX2FJ77DCT3WUMYDBS
  - path: /service-role/
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "lambda.amazonaws.com"}}], "Version": "2012-10-17"}'


? powershell-role-gj7p5czv-arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-7536bc56-a15a-4ff6-bdb8-b6ed04fdfbec


: aws.iam.role_policy_attachment.present:
  - role_name: powershell-role-gj7p5czv
  - policy_arn: arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-7536bc56-a15a-4ff6-bdb8-b6ed04fdfbec


prelude_test:
  aws.iam.user.present:
  - name: prelude_test
  - resource_id: prelude_test
  - arn: arn:aws:iam::123456789012:user/prelude_test
  - path: /
  - user_name: prelude_test


prelude_test-potato-AWS-Permissions:
  aws.iam.user_policy.present:
  - resource_id: prelude_test-potato-AWS-Permissions
  - user_name: prelude_test
  - name: potato-AWS-Permissions
  - policy_document: '{"Statement": [{"Action": ["ec2:DescribeAvailabilityZones",
      "ec2:DescribeInstances", "ec2:DescribeInternetGateways", "ec2:DescribeRouteTables",
      "ec2:DescribeSecurityGroups", "ec2:DescribeSubnets", "ec2:DescribeVolumes",
      "ec2:DescribeVpcs"], "Effect": "Allow", "Resource": "*"}, {"Action": ["cloudwatch:ListMetrics",
      "cloudwatch:GetMetricStatistics"], "Effect": "Allow", "Resource": "*"}, {"Action":
      ["s3:Get*", "s3:List*"], "Effect": "Allow", "Resource": "*"}], "Version": "2012-10-17"}'


prelude_test02:
  aws.iam.user.present:
  - name: prelude_test02
  - resource_id: prelude_test02
  - arn: arn:aws:iam::123456789012:user/prelude_test02
  - path: /
  - user_name: prelude_test02


prelude_test02-ensemble_primary_acc_policy-test:
  aws.iam.user_policy.present:
  - resource_id: prelude_test02-ensemble_primary_acc_policy-test
  - user_name: prelude_test02
  - name: ensemble_primary_acc_policy-test
  - policy_document: '{"Statement": [{"Action": ["iam:ListAccountAliases"], "Effect":
      "Allow", "Resource": ["*"]}, {"Action": ["ec2:Describe*"], "Effect": "Allow",
      "Resource": "*"}, {"Action": ["logs:Describe*", "logs:Get*", "logs:TestMetricFilter",
      "logs:FilterLogEvents"], "Effect": "Allow", "Resource": "*"}, {"Action": ["organizations:ListAccounts"],
      "Effect": "Allow", "Resource": "*"}, {"Action": "sts:AssumeRole", "Effect":
      "Allow", "Resource": "*"}], "Version": "2012-10-17"}'


prelude_test02-potato-AWS-Permissions:
  aws.iam.user_policy.present:
  - resource_id: prelude_test02-potato-AWS-Permissions
  - user_name: prelude_test02
  - name: potato-AWS-Permissions
  - policy_document: '{"Statement": [{"Action": ["ec2:DescribeAvailabilityZones",
      "ec2:DescribeInstances", "ec2:DescribeInternetGateways", "ec2:DescribeRouteTables",
      "ec2:DescribeSecurityGroups", "ec2:DescribeSubnets", "ec2:DescribeVolumes",
      "ec2:DescribeVpcs"], "Effect": "Allow", "Resource": "*"}, {"Action": ["cloudwatch:ListMetrics",
      "cloudwatch:GetMetricStatistics"], "Effect": "Allow", "Resource": "*"}, {"Action":
      ["s3:Get*", "s3:List*"], "Effect": "Allow", "Resource": "*"}], "Version": "2012-10-17"}'


prelude_test02-potato-AWS-Permissions1:
  aws.iam.user_policy.present:
  - resource_id: prelude_test02-potato-AWS-Permissions1
  - user_name: prelude_test02
  - name: potato-AWS-Permissions1
  - policy_document: '{"Statement": [{"Action": ["ec2:DescribeAvailabilityZones",
      "ec2:DescribeInstances", "ec2:DescribeInternetGateways", "ec2:DescribeRouteTables",
      "ec2:DescribeSecurityGroups", "ec2:DescribeSubnets", "ec2:DescribeVolumes",
      "ec2:DescribeVpcs"], "Effect": "Allow", "Resource": "*"}, {"Action": ["cloudwatch:ListMetrics",
      "cloudwatch:GetMetricStatistics"], "Effect": "Allow", "Resource": "*"}, {"Action":
      ["s3:Get*", "s3:List*"], "Effect": "Allow", "Resource": "*"}], "Version": "2012-10-17"}'


pvbhullg-dev-us-east-1-lambdaRole:
  aws.iam.role.present:
  - resource_id: pvbhullg-dev-us-east-1-lambdaRole
  - name: pvbhullg-dev-us-east-1-lambdaRole
  - arn: arn:aws:iam::123456789012:role/pvbhullg-dev-us-east-1-lambdaRole
  - id: AROAX2FJ77DCZVPSKQDVX
  - path: /
  - description: ''
  - max_session_duration: 3600
  - tags:
    - Key: STAGE
      Value: dev
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "lambda.amazonaws.com"}}], "Version": "2012-10-17"}'


pvbhullg-dev-us-east-1-lambdaRole-pvbhullg-dev-lambda:
  aws.iam.role_policy.present:
  - resource_id: pvbhullg-dev-us-east-1-lambdaRole-pvbhullg-dev-lambda
  - role_name: pvbhullg-dev-us-east-1-lambdaRole
  - name: pvbhullg-dev-lambda
  - policy_document: '{"Statement": [{"Action": ["logs:CreateLogStream", "logs:CreateLogGroup"],
      "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:/aws/lambda/pvbhullg-dev*:*"]},
      {"Action": ["logs:PutLogEvents"], "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:/aws/lambda/pvbhullg-dev*:*:*"]}],
      "Version": "2012-10-17"}'


rajeshagrawa@abc.com:
  aws.iam.user.present:
  - name: rajeshagrawa@abc.com
  - resource_id: rajeshagrawa@abc.com
  - arn: arn:aws:iam::123456789012:user/rajeshagrawa@abc.com
  - path: /
  - user_name: rajeshagrawa@abc.com


rds-monitoring-role:
  aws.iam.role.present:
  - resource_id: rds-monitoring-role
  - name: rds-monitoring-role
  - arn: arn:aws:iam::123456789012:role/rds-monitoring-role
  - id: AROAJ2YIUIG6C2RGSUPCW
  - path: /
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "monitoring.rds.amazonaws.com"}, "Sid": ""}],
      "Version": "2012-10-17"}'


rds-monitoring-role-arn:aws:iam::aws:policy/service-role/AmazonRDSEnhancedMonitoringRole:
  aws.iam.role_policy_attachment.present:
  - role_name: rds-monitoring-role
  - policy_arn: arn:aws:iam::aws:policy/service-role/AmazonRDSEnhancedMonitoringRole


salt_sqs_poc:
  aws.iam.user.present:
  - name: salt_sqs_poc
  - resource_id: salt_sqs_poc
  - arn: arn:aws:iam::123456789012:user/salt_sqs_poc
  - path: /
  - user_name: salt_sqs_poc


secure-state-event-role:
  aws.iam.role.present:
  - resource_id: secure-state-event-role
  - name: secure-state-event-role
  - arn: arn:aws:iam::123456789012:role/secure-state-event-role
  - id: AROAX2FJ77DCUMXWHC356
  - path: /
  - description: ''
  - max_session_duration: 3600
  - tags:
    - Key: ManagedBy
      Value: ConformityEngine
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "events.amazonaws.com"}}], "Version": "2012-10-17"}'


secure-state-event-role-secure-state-event-role-policy:
  aws.iam.role_policy.present:
  - resource_id: secure-state-event-role-secure-state-event-role-policy
  - role_name: secure-state-event-role
  - name: secure-state-event-role-policy
  - policy_document: '{"Statement": [{"Action": "events:PutEvents", "Effect": "Allow",
      "Resource": "arn:aws:events:*:963874769787:event-bus/default"}], "Version":
      "2012-10-17"}'


securestate-role:
  aws.iam.role.present:
  - resource_id: securestate-role
  - name: securestate-role
  - arn: arn:aws:iam::123456789012:role/securestate-role
  - id: AROAX2FJ77DCR337N3O2J
  - path: /
  - description: ''
  - max_session_duration: 3600
  - tags:
    - Key: ManagedBy
      Value: ConformityEngine
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {"StringEquals": {"sts:ExternalId": "yC7ArT3F57MC8QHlVagibYz9YMce"}}, "Effect":
      "Allow", "Principal": {"AWS": "arn:aws:iam::910887748405:root"}}], "Version":
      "2012-10-17"}'


securestate-role-arn:aws:iam::aws:policy/SecurityAudit:
  aws.iam.role_policy_attachment.present:
  - role_name: securestate-role
  - policy_arn: arn:aws:iam::aws:policy/SecurityAudit


serverless:
  aws.iam.user.present:
  - name: serverless
  - resource_id: serverless
  - arn: arn:aws:iam::123456789012:user/serverless
  - path: /
  - user_name: serverless


serverlessrepo-hello-world-helloworldRole-BJQ26NJLFUJY:
  aws.iam.role.present:
  - resource_id: serverlessrepo-hello-world-helloworldRole-BJQ26NJLFUJY
  - name: serverlessrepo-hello-world-helloworldRole-BJQ26NJLFUJY
  - arn: arn:aws:iam::123456789012:role/serverlessrepo-hello-world-helloworldRole-BJQ26NJLFUJY
  - id: AROAX2FJ77DCUDCV5ZNCI
  - path: /
  - description: ''
  - max_session_duration: 3600
  - tags:
    - Key: serverlessrepo:semanticVersion
      Value: 1.0.4
    - Key: lambda:createdBy
      Value: SAM
    - Key: serverlessrepo:applicationId
      Value: arn:aws:serverlessrepo:us-east-1:077246666028:applications/hello-world
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "lambda.amazonaws.com"}}], "Version": "2012-10-17"}'


serverlessrepo-hello-world-helloworldRole-BJQ26NJLFUJY-arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole:
  aws.iam.role_policy_attachment.present:
  - role_name: serverlessrepo-hello-world-helloworldRole-BJQ26NJLFUJY
  - policy_arn: arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole


serverlessrepo-hello-world-helloworldRole-BJQ26NJLFUJY-helloworldRolePolicy0:
  aws.iam.role_policy.present:
  - resource_id: serverlessrepo-hello-world-helloworldRole-BJQ26NJLFUJY-helloworldRolePolicy0
  - role_name: serverlessrepo-hello-world-helloworldRole-BJQ26NJLFUJY
  - name: helloworldRolePolicy0
  - policy_document: '{"Statement": [{"Action": ["ses:SendBounce"], "Effect": "Allow",
      "Resource": "arn:aws:ses:us-east-1:123456789012:identity/test_id"}]}'


service-09vpvytp-dev-us-east-1-lambdaRole:
  aws.iam.role.present:
  - resource_id: service-09vpvytp-dev-us-east-1-lambdaRole
  - name: service-09vpvytp-dev-us-east-1-lambdaRole
  - arn: arn:aws:iam::123456789012:role/service-09vpvytp-dev-us-east-1-lambdaRole
  - id: AROAX2FJ77DC5NIE5QHPY
  - path: /
  - description: ''
  - max_session_duration: 3600
  - tags:
    - Key: STAGE
      Value: dev
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "lambda.amazonaws.com"}}], "Version": "2012-10-17"}'


service-09vpvytp-dev-us-east-1-lambdaRole-service-09vpvytp-dev-lambda:
  aws.iam.role_policy.present:
  - resource_id: service-09vpvytp-dev-us-east-1-lambdaRole-service-09vpvytp-dev-lambda
  - role_name: service-09vpvytp-dev-us-east-1-lambdaRole
  - name: service-09vpvytp-dev-lambda
  - policy_document: '{"Statement": [{"Action": ["logs:CreateLogStream", "logs:CreateLogGroup"],
      "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:/aws/lambda/service-09vpvytp-dev*:*"]},
      {"Action": ["logs:PutLogEvents"], "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:/aws/lambda/service-09vpvytp-dev*:*:*"]}],
      "Version": "2012-10-17"}'


spotinst-iam-stack-33rom-SpotinstRole-1B9YKWTIB1LGK:
  aws.iam.role.present:
  - resource_id: spotinst-iam-stack-33rom-SpotinstRole-1B9YKWTIB1LGK
  - name: spotinst-iam-stack-33rom-SpotinstRole-1B9YKWTIB1LGK
  - arn: arn:aws:iam::123456789012:role/spotinst-iam-stack-33rom-SpotinstRole-1B9YKWTIB1LGK
  - id: AROAX2FJ77DC5CXAUUMSQ
  - path: /
  - description: ''
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {"StringEquals": {"sts:ExternalId": "5iOE4qbqkHcyajclWh9AXm-YhZWa5t7Qo27CFhfS7Js-"}},
      "Effect": "Allow", "Principal": {"AWS": "arn:aws:iam::922761411349:root"}}],
      "Version": "2012-10-17"}'


? spotinst-iam-stack-33rom-SpotinstRole-1B9YKWTIB1LGK-arn:aws:iam::123456789012:policy/spotinst-iam-stack-33rom-SpotinstManagedPolicy-1VGQO1DZRBAM8


: aws.iam.role_policy_attachment.present:
  - role_name: spotinst-iam-stack-33rom-SpotinstRole-1B9YKWTIB1LGK
  - policy_arn: arn:aws:iam::123456789012:policy/spotinst-iam-stack-33rom-SpotinstManagedPolicy-1VGQO1DZRBAM8


spotinst-iam-stack-33rom-SpotinstRole-1B9YKWTIB1LGK-arn:aws:iam::aws:policy/SecurityAudit:
  aws.iam.role_policy_attachment.present:
  - role_name: spotinst-iam-stack-33rom-SpotinstRole-1B9YKWTIB1LGK
  - policy_arn: arn:aws:iam::aws:policy/SecurityAudit


spotinst-iam-stack-8tt4w-SpotinstRole-9OV7TCGCIEXG:
  aws.iam.role.present:
  - resource_id: spotinst-iam-stack-8tt4w-SpotinstRole-9OV7TCGCIEXG
  - name: spotinst-iam-stack-8tt4w-SpotinstRole-9OV7TCGCIEXG
  - arn: arn:aws:iam::123456789012:role/spotinst-iam-stack-8tt4w-SpotinstRole-9OV7TCGCIEXG
  - id: AROAX2FJ77DCSPDY5NPT2
  - path: /
  - description: ''
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {"StringEquals": {"sts:ExternalId": "i7IqcLtbB11zzsfahn3lLP8hLguVmGV1jzKrwFmv6eA-"}},
      "Effect": "Allow", "Principal": {"AWS": "arn:aws:iam::922761411349:root"}}],
      "Version": "2012-10-17"}'


? spotinst-iam-stack-8tt4w-SpotinstRole-9OV7TCGCIEXG-arn:aws:iam::123456789012:policy/spotinst-iam-stack-8tt4w-SpotinstManagedPolicy-6URGPL3AFJO8


: aws.iam.role_policy_attachment.present:
  - role_name: spotinst-iam-stack-8tt4w-SpotinstRole-9OV7TCGCIEXG
  - policy_arn: arn:aws:iam::123456789012:policy/spotinst-iam-stack-8tt4w-SpotinstManagedPolicy-6URGPL3AFJO8


spotinst-iam-stack-8tt4w-SpotinstRole-9OV7TCGCIEXG-arn:aws:iam::aws:policy/SecurityAudit:
  aws.iam.role_policy_attachment.present:
  - role_name: spotinst-iam-stack-8tt4w-SpotinstRole-9OV7TCGCIEXG
  - policy_arn: arn:aws:iam::aws:policy/SecurityAudit


ssumeer@abc.com:
  aws.iam.user.present:
  - name: ssumeer@abc.com
  - resource_id: ssumeer@abc.com
  - arn: arn:aws:iam::123456789012:user/ssumeer@abc.com
  - path: /
  - tags:
    - Key: email
      Value: ssumeer@abc.com
  - user_name: ssumeer@abc.com


t1-role-cn88ujwv:
  aws.iam.role.present:
  - resource_id: t1-role-cn88ujwv
  - name: t1-role-cn88ujwv
  - arn: arn:aws:iam::123456789012:role/service-role/t1-role-cn88ujwv
  - id: AROAX2FJ77DCXRVYLLRYE
  - path: /service-role/
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "lambda.amazonaws.com"}}], "Version": "2012-10-17"}'


? t1-role-cn88ujwv-arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-c115d523-c25b-44e3-b42b-32143f4af4bf


: aws.iam.role_policy_attachment.present:
  - role_name: t1-role-cn88ujwv
  - policy_arn: arn:aws:iam::123456789012:policy/service-role/AWSLambdaBasicExecutionRole-c115d523-c25b-44e3-b42b-32143f4af4bf


t2guydv5-dev-us-east-1-lambdaRole:
  aws.iam.role.present:
  - resource_id: t2guydv5-dev-us-east-1-lambdaRole
  - name: t2guydv5-dev-us-east-1-lambdaRole
  - arn: arn:aws:iam::123456789012:role/t2guydv5-dev-us-east-1-lambdaRole
  - id: AROAX2FJ77DCTAHNNN72O
  - path: /
  - description: ''
  - max_session_duration: 3600
  - tags:
    - Key: STAGE
      Value: dev
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "lambda.amazonaws.com"}}], "Version": "2012-10-17"}'


t2guydv5-dev-us-east-1-lambdaRole-t2guydv5-dev-lambda:
  aws.iam.role_policy.present:
  - resource_id: t2guydv5-dev-us-east-1-lambdaRole-t2guydv5-dev-lambda
  - role_name: t2guydv5-dev-us-east-1-lambdaRole
  - name: t2guydv5-dev-lambda
  - policy_document: '{"Statement": [{"Action": ["logs:CreateLogStream", "logs:CreateLogGroup"],
      "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:/aws/lambda/t2guydv5-dev*:*"]},
      {"Action": ["logs:PutLogEvents"], "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:/aws/lambda/t2guydv5-dev*:*:*"]}],
      "Version": "2012-10-17"}'


test:
  aws.iam.role.present:
  - resource_id: test
  - name: test
  - arn: arn:aws:iam::123456789012:role/service-role/test
  - id: AROAX2FJ77DCZ632NHVVI
  - path: /service-role/
  - description: Proton pipeline service role
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "proton.amazonaws.com"}}], "Version": "2012-10-17"}'


test-arn:aws:iam::123456789012:policy/service-role/ProtonRolePolicy-test:
  aws.iam.role_policy_attachment.present:
  - role_name: test
  - policy_arn: arn:aws:iam::123456789012:policy/service-role/ProtonRolePolicy-test


test-xyz_fargate:
  aws.iam.role.present:
  - resource_id: test-xyz_fargate
  - name: test-xyz_fargate
  - arn: arn:aws:iam::123456789012:role/test-xyz_fargate
  - id: AROAX2FJ77DCUZU4M6BP7
  - path: /
  - description: Allows access to other AWS service resources that are required to
      run Amazon xyz pods on AWS Fargate.
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "xyz-fargate-pods.amazonaws.com"}}], "Version":
      "2012-10-17"}'


test-xyz_fargate-arn:aws:iam::aws:policy/AmazonxyzFargatePodExecutionRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: test-xyz_fargate
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzFargatePodExecutionRolePolicy


tgeorgiev-lambda-dynamodb:
  aws.iam.role.present:
  - resource_id: tgeorgiev-lambda-dynamodb
  - name: tgeorgiev-lambda-dynamodb
  - arn: arn:aws:iam::123456789012:role/tgeorgiev-lambda-dynamodb
  - id: AROAX2FJ77DC3KAWRBNJ7
  - path: /
  - description: Allows Lambda functions to call AWS services on your behalf.
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "lambda.amazonaws.com"}}], "Version": "2012-10-17"}'


tgeorgiev-lambda-dynamodb-arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess:
  aws.iam.role_policy_attachment.present:
  - role_name: tgeorgiev-lambda-dynamodb
  - policy_arn: arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess


tgeorgiev-lambda-dynamodb-arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole:
  aws.iam.role_policy_attachment.present:
  - role_name: tgeorgiev-lambda-dynamodb
  - policy_arn: arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole


tjingjing_test:
  aws.iam.user.present:
  - name: tjingjing_test
  - resource_id: tjingjing_test
  - arn: arn:aws:iam::123456789012:user/tjingjing_test
  - path: /
  - user_name: tjingjing_test


vieworgspolicies:
  aws.iam.role.present:
  - resource_id: vieworgspolicies
  - name: vieworgspolicies
  - arn: arn:aws:iam::123456789012:role/vieworgspolicies
  - id: AROAX2FJ77DCZRY2OJLXC
  - path: /
  - description: vieworgspolicies
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {}, "Effect": "Allow", "Principal": {"AWS": "arn:aws:iam::123456789012:root"}}],
      "Version": "2012-10-17"}'


vieworgspolicies-arn:aws:iam::aws:policy/AWSOrganizationsReadOnlyAccess:
  aws.iam.role_policy_attachment.present:
  - role_name: vieworgspolicies
  - policy_arn: arn:aws:iam::aws:policy/AWSOrganizationsReadOnlyAccess


vkalal@abc.com:
  aws.iam.user.present:
  - name: vkalal@abc.com
  - resource_id: vkalal@abc.com
  - arn: arn:aws:iam::123456789012:user/vkalal@abc.com
  - path: /
  - user_name: vkalal@abc.com


vmw-cloudhealth-role:
  aws.iam.role.present:
  - resource_id: vmw-cloudhealth-role
  - name: vmw-cloudhealth-role
  - arn: arn:aws:iam::123456789012:role/vmw-cloudhealth-role
  - id: AROAJDM6SWVHL3ZLVIJBU
  - path: /
  - description: Role for AWS-CloudHealth integration at abc
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {"StringEquals": {"sts:ExternalId": "81320172017"}}, "Effect": "Allow", "Principal":
      {"AWS": "arn:aws:iam::454464851268:root"}}], "Version": "2012-10-17"}'


vmw-cloudhealth-role-arn:aws:iam::123456789012:policy/vmw-cloudhealth-automation:
  aws.iam.role_policy_attachment.present:
  - role_name: vmw-cloudhealth-role
  - policy_arn: arn:aws:iam::123456789012:policy/vmw-cloudhealth-automation


vmw-cloudhealth-role-arn:aws:iam::123456789012:policy/vmw-cloudhealth-policy:
  aws.iam.role_policy_attachment.present:
  - role_name: vmw-cloudhealth-role
  - policy_arn: arn:aws:iam::123456789012:policy/vmw-cloudhealth-policy


vmws-config-role:
  aws.iam.role.present:
  - resource_id: vmws-config-role
  - name: vmws-config-role
  - arn: arn:aws:iam::123456789012:role/vmws-config-role
  - id: AROAX2FJ77DCZW4QMKR7K
  - path: /
  - description: Role for abc AWS Config - Do not delete
  - max_session_duration: 3600
  - tags:
    - Key: ManagedBy
      Value: ConformityEngine
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "config.amazonaws.com"}, "Sid": ""}], "Version":
      "2012-10-17"}'


vmws-config-role-arn:aws:iam::aws:policy/service-role/AWSConfigRole:
  aws.iam.role_policy_attachment.present:
  - role_name: vmws-config-role
  - policy_arn: arn:aws:iam::aws:policy/service-role/AWSConfigRole


xyz-cluster01-admin:
  aws.iam.role.present:
  - resource_id: xyz-cluster01-admin
  - name: xyz-cluster01-admin
  - arn: arn:aws:iam::123456789012:role/xyz-cluster01-admin
  - id: AROAX2FJ77DCS4HYWPSRF
  - path: /
  - max_session_duration: 3600
  - tags:
    - Key: Automation
      Value: 'true'
    - Key: COGS
      Value: OPEX
    - Key: Owner
      Value: org1
    - Key: Environment
      Value: default
    - Key: KubernetesCluster
      Value: cluster01
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {"ForAnyValue:StringEquals": {"aws:username": ["user1", "user2", "user3", "user4",
      "user5"]}}, "Effect": "Allow", "Principal": {"AWS": "arn:aws:iam::123456789012:root"}},
      {"Action": "sts:AssumeRole", "Effect": "Allow", "Principal": {"AWS": "arn:aws:iam::123456789012:role/xyz-cluster01-admin"}}],
      "Version": "2012-10-17"}'


xyz-cluster01-admin-arn:aws:iam::aws:policy/AdministratorAccess:
  aws.iam.role_policy_attachment.present:
  - role_name: xyz-cluster01-admin
  - policy_arn: arn:aws:iam::aws:policy/AdministratorAccess


xyz-cluster01_redlock_flow_role:
  aws.iam.role.present:
  - resource_id: xyz-cluster01_redlock_flow_role
  - name: xyz-cluster01_redlock_flow_role
  - arn: arn:aws:iam::123456789012:role/xyz-cluster01_redlock_flow_role
  - id: AROAX2FJ77DCQTHAPMDNN
  - path: /
  - max_session_duration: 3600
  - tags:
    - Key: Owner
      Value: org1
    - Key: KubernetesCluster
      Value: cluster01
    - Key: COGS
      Value: OPEX
    - Key: Automation
      Value: 'true'
    - Key: Environment
      Value: test-dev
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "vpc-flow-logs.amazonaws.com"}, "Sid": ""}],
      "Version": "2012-10-17"}'


xyz-cluster01_redlock_flow_role-xyz-cluster01_redlock_flow_policy:
  aws.iam.role_policy.present:
  - resource_id: xyz-cluster01_redlock_flow_role-xyz-cluster01_redlock_flow_policy
  - role_name: xyz-cluster01_redlock_flow_role
  - name: xyz-cluster01_redlock_flow_policy
  - policy_document: '{"Statement": [{"Action": ["logs:CreateLogGroup", "logs:CreateLogStream",
      "logs:PutLogEvents", "logs:DescribeLogGroups", "logs:DescribeLogStreams"], "Effect":
      "Allow", "Resource": "*"}], "Version": "2012-10-17"}'


xyz-cluster02-fargate-pod-execution-role:
  aws.iam.role.present:
  - resource_id: xyz-cluster02-fargate-pod-execution-role
  - name: xyz-cluster02-fargate-pod-execution-role
  - arn: arn:aws:iam::123456789012:role/xyz-cluster02-fargate-pod-execution-role
  - id: AROAX2FJ77DCXS2XYYNNQ
  - path: /
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": ["xyz-fargate-pods.amazonaws.com", "xyz.amazonaws.com"]}}],
      "Version": "2012-10-17"}'


xyz-cluster02-fargate-pod-execution-role-arn:aws:iam::aws:policy/AmazonxyzFargatePodExecutionRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: xyz-cluster02-fargate-pod-execution-role
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzFargatePodExecutionRolePolicy


xyz-idem-test-admin:
  aws.iam.role.present:
  - resource_id: xyz-idem-test-admin
  - name: xyz-idem-test-admin
  - arn: arn:aws:iam::123456789012:role/xyz-idem-test-admin
  - id: AROAX2FJ77DC33OWDECR6
  - path: /
  - max_session_duration: 3600
  - tags:
    - Key: Environment
      Value: test-dev
    - Key: Owner
      Value: org1
    - Key: Automation
      Value: 'true'
    - Key: COGS
      Value: OPEX
    - Key: KubernetesCluster
      Value: idem-test
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {"ForAnyValue:StringEquals": {"aws:username": ["user1", "user2", "user3", "user4",
      "user5"]}}, "Effect": "Allow", "Principal": {"AWS": "arn:aws:iam::123456789012:root"}},
      {"Action": "sts:AssumeRole", "Effect": "Allow", "Principal": {"Service": "xyz.amazonaws.com"}}],
      "Version": "2012-10-17"}'


xyz-idem-test-admin-arn:aws:iam::123456789012:policy/xyz-idem-test-admin:
  aws.iam.role_policy_attachment.present:
  - role_name: xyz-idem-test-admin
  - policy_arn: arn:aws:iam::123456789012:policy/xyz-idem-test-admin


xyz-idem-test-fargate-pod-execution-role:
  aws.iam.role.present:
  - resource_id: xyz-idem-test-fargate-pod-execution-role
  - name: xyz-idem-test-fargate-pod-execution-role
  - arn: arn:aws:iam::123456789012:role/xyz-idem-test-fargate-pod-execution-role
  - id: AROAX2FJ77DC6EXFK3CYE
  - path: /
  - max_session_duration: 3600
  - tags:
    - Key: Owner
      Value: org1
    - Key: KubernetesCluster
      Value: idem-test
    - Key: COGS
      Value: OPEX
    - Key: Automation
      Value: 'true'
    - Key: Environment
      Value: test
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": ["xyz.amazonaws.com", "xyz-fargate-pods.amazonaws.com"]}}],
      "Version": "2012-10-17"}'


xyz-idem-test-fargate-pod-execution-role-arn:aws:iam::aws:policy/AmazonxyzFargatePodExecutionRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: xyz-idem-test-fargate-pod-execution-role
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzFargatePodExecutionRolePolicy


xyz-idem-test-jenkins:
  aws.iam.role.present:
  - resource_id: xyz-idem-test-jenkins
  - name: xyz-idem-test-jenkins
  - arn: arn:aws:iam::123456789012:role/xyz-idem-test-jenkins
  - id: AROAX2FJ77DCQHIULDKIS
  - path: /
  - max_session_duration: 3600
  - tags:
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Automation
      Value: 'true'
    - Key: Environment
      Value: test-dev
    - Key: Owner
      Value: org1
    - Key: COGS
      Value: OPEX
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Condition":
      {"ForAnyValue:StringEquals": {"aws:username": ["user1", "user2", "user3", "user4",
      "user5"]}}, "Effect": "Allow", "Principal": {"AWS": "arn:aws:iam::123456789012:root"}},
      {"Action": "sts:AssumeRole", "Effect": "Allow", "Principal": {"AWS": "arn:aws:iam::123456789012:user/xyz/idem-test/extension-jenkins-idem-test"}}],
      "Version": "2012-10-17"}'


xyz-idem-test-jenkins-arn:aws:iam::123456789012:policy/xyz-idem-test-jenkins:
  aws.iam.role_policy_attachment.present:
  - role_name: xyz-idem-test-jenkins
  - policy_arn: arn:aws:iam::123456789012:policy/xyz-idem-test-jenkins


xyz-idem-test_redlock_flow_role:
  aws.iam.role.present:
  - resource_id: xyz-idem-test_redlock_flow_role
  - name: xyz-idem-test_redlock_flow_role
  - arn: arn:aws:iam::123456789012:role/xyz-idem-test_redlock_flow_role
  - id: AROAX2FJ77DC7NUDJJ6DH
  - path: /
  - max_session_duration: 3600
  - tags:
    - Key: Owner
      Value: org1
    - Key: Automation
      Value: 'true'
    - Key: Environment
      Value: test-dev
    - Key: KubernetesCluster
      Value: idem-test
    - Key: COGS
      Value: OPEX
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "vpc-flow-logs.amazonaws.com"}, "Sid": ""}],
      "Version": "2012-10-17"}'


xyz-idem-test_redlock_flow_role-xyz-idem-test_redlock_flow_policy:
  aws.iam.role_policy.present:
  - resource_id: xyz-idem-test_redlock_flow_role-xyz-idem-test_redlock_flow_policy
  - role_name: xyz-idem-test_redlock_flow_role
  - name: xyz-idem-test_redlock_flow_policy
  - policy_document: '{"Statement": [{"Action": ["logs:CreateLogGroup", "logs:CreateLogStream",
      "logs:PutLogEvents", "logs:DescribeLogGroups", "logs:DescribeLogStreams"], "Effect":
      "Allow", "Resource": "*"}], "Version": "2012-10-17"}'


xyzClusterRole:
  aws.iam.role.present:
  - resource_id: xyzClusterRole
  - name: xyzClusterRole
  - arn: arn:aws:iam::123456789012:role/xyzClusterRole
  - id: AROAX2FJ77DCUPA62WEVL
  - path: /
  - description: Allows access to other AWS service resources that are required to
      operate clusters managed by xyz.
  - max_session_duration: 14400
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "xyz.amazonaws.com"}}], "Version": "2012-10-17"}'


xyzClusterRole-arn:aws:iam::aws:policy/AmazonxyzClusterPolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: xyzClusterRole
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzClusterPolicy


xyzClusterRole-idem-test-temp-xyz-bootstrap-access:
  aws.iam.role_policy.present:
  - resource_id: xyzClusterRole-idem-test-temp-xyz-bootstrap-access
  - role_name: xyzClusterRole
  - name: idem-test-temp-xyz-bootstrap-access
  - policy_document: '{"Statement": [{"Action": ["ec2:CreateTags", "ec2:AssociateAddress",
      "logs:CreateLogGroup", "logs:CreateLogStream", "logs:PutLogEvents"], "Effect":
      "Allow", "Resource": "*"}], "Version": "2012-10-17"}'


xyzManageRole:
  aws.iam.role.present:
  - resource_id: xyzManageRole
  - name: xyzManageRole
  - arn: arn:aws:iam::123456789012:role/xyzManageRole
  - id: AROAI6URWWBKJCLNVOAW2
  - path: /
  - description: Allows xyz to manage clusters on your behalf.
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "xyz.amazonaws.com"}}], "Version": "2012-10-17"}'


xyzManageRole-arn:aws:iam::aws:policy/AmazonxyzClusterPolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: xyzManageRole
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzClusterPolicy


xyzManageRole-arn:aws:iam::aws:policy/AmazonxyzServicePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: xyzManageRole
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzServicePolicy


xyzcluster:
  aws.iam.role.present:
  - resource_id: xyzcluster
  - name: xyzcluster
  - arn: arn:aws:iam::123456789012:role/xyzcluster
  - id: AROAX2FJ77DCWNFKJVIOQ
  - path: /
  - description: Allows access to other AWS service resources that are required to
      operate clusters managed by xyz.
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "xyz.amazonaws.com"}}], "Version": "2012-10-17"}'


xyzcluster-arn:aws:iam::aws:policy/AmazonxyzClusterPolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: xyzcluster
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzClusterPolicy


xyzctl-pr-ssc-xyz-poc-addon-iamserviceaccoun-Role1-I6WHOL6EKKG2:
  aws.iam.role.present:
  - resource_id: xyzctl-pr-ssc-xyz-poc-addon-iamserviceaccoun-Role1-I6WHOL6EKKG2
  - name: xyzctl-pr-ssc-xyz-poc-addon-iamserviceaccoun-Role1-I6WHOL6EKKG2
  - arn: arn:aws:iam::123456789012:role/xyzctl-pr-ssc-xyz-poc-addon-iamserviceaccoun-Role1-I6WHOL6EKKG2
  - id: AROAX2FJ77DCYKAQDLEQU
  - path: /
  - description: ''
  - max_session_duration: 3600
  - tags:
    - Key: alpha.xyzctl.io/cluster-name
      Value: pr-ssc-xyz-poc
    - Key: xyzctl.cluster.k8s.io/v1alpha1/cluster-name
      Value: pr-ssc-xyz-poc
    - Key: alpha.xyzctl.io/iamserviceaccount-name
      Value: kube-system/alb-ingress-controller
    - Key: alpha.xyzctl.io/xyzctl-version
      Value: 0.77.0
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRoleWithWebIdentity",
      "Condition": {"StringEquals": {"oidc.xyz.us-east-2.amazonaws.com/id/D1EE3259ED98815196FA1D1A3E51C26A:aud":
      "sts.amazonaws.com", "oidc.xyz.us-east-2.amazonaws.com/id/D1EE3259ED98815196FA1D1A3E51C26A:sub":
      "system:serviceaccount:kube-system:alb-ingress-controller"}}, "Effect": "Allow",
      "Principal": {"Federated": "arn:aws:iam::123456789012:oidc-provider/oidc.xyz.us-east-2.amazonaws.com/id/D1EE3259ED98815196FA1D1A3E51C26A"}}],
      "Version": "2012-10-17"}'


? xyzctl-pr-ssc-xyz-poc-addon-iamserviceaccoun-Role1-I6WHOL6EKKG2-arn:aws:iam::123456789012:policy/ALBIngressControllerIAMPolicy


: aws.iam.role_policy_attachment.present:
  - role_name: xyzctl-pr-ssc-xyz-poc-addon-iamserviceaccoun-Role1-I6WHOL6EKKG2
  - policy_arn: arn:aws:iam::123456789012:policy/ALBIngressControllerIAMPolicy


xyzctl-pr-ssc-xyz-poc-addon-iamserviceaccoun-Role1-L0052FE4X7Q0:
  aws.iam.role.present:
  - resource_id: xyzctl-pr-ssc-xyz-poc-addon-iamserviceaccoun-Role1-L0052FE4X7Q0
  - name: xyzctl-pr-ssc-xyz-poc-addon-iamserviceaccoun-Role1-L0052FE4X7Q0
  - arn: arn:aws:iam::123456789012:role/xyzctl-pr-ssc-xyz-poc-addon-iamserviceaccoun-Role1-L0052FE4X7Q0
  - id: AROAX2FJ77DCWVCEYIHTX
  - path: /
  - description: ''
  - max_session_duration: 3600
  - tags:
    - Key: alpha.xyzctl.io/cluster-name
      Value: pr-ssc-xyz-poc
    - Key: xyzctl.cluster.k8s.io/v1alpha1/cluster-name
      Value: pr-ssc-xyz-poc
    - Key: alpha.xyzctl.io/iamserviceaccount-name
      Value: kube-system/aws-load-balancer-controller
    - Key: alpha.xyzctl.io/xyzctl-version
      Value: 0.77.0
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRoleWithWebIdentity",
      "Condition": {"StringEquals": {"oidc.xyz.us-east-2.amazonaws.com/id/D1EE3259ED98815196FA1D1A3E51C26A:aud":
      "sts.amazonaws.com", "oidc.xyz.us-east-2.amazonaws.com/id/D1EE3259ED98815196FA1D1A3E51C26A:sub":
      "system:serviceaccount:kube-system:aws-load-balancer-controller"}}, "Effect":
      "Allow", "Principal": {"Federated": "arn:aws:iam::123456789012:oidc-provider/oidc.xyz.us-east-2.amazonaws.com/id/D1EE3259ED98815196FA1D1A3E51C26A"}}],
      "Version": "2012-10-17"}'


? xyzctl-pr-ssc-xyz-poc-addon-iamserviceaccoun-Role1-L0052FE4X7Q0-arn:aws:iam::123456789012:policy/AWSLoadBalancerControllerAdditionalIAMPolicy


: aws.iam.role_policy_attachment.present:
  - role_name: xyzctl-pr-ssc-xyz-poc-addon-iamserviceaccoun-Role1-L0052FE4X7Q0
  - policy_arn: arn:aws:iam::123456789012:policy/AWSLoadBalancerControllerAdditionalIAMPolicy


? xyzctl-pr-ssc-xyz-poc-addon-iamserviceaccoun-Role1-L0052FE4X7Q0-arn:aws:iam::123456789012:policy/AWSLoadBalancerControllerIAMPolicy


: aws.iam.role_policy_attachment.present:
  - role_name: xyzctl-pr-ssc-xyz-poc-addon-iamserviceaccoun-Role1-L0052FE4X7Q0
  - policy_arn: arn:aws:iam::123456789012:policy/AWSLoadBalancerControllerIAMPolicy


xyzctl-pr-ssc-xyz-poc-cluster-ServiceRole-1889QFJ9Q8DJY:
  aws.iam.role.present:
  - resource_id: xyzctl-pr-ssc-xyz-poc-cluster-ServiceRole-1889QFJ9Q8DJY
  - name: xyzctl-pr-ssc-xyz-poc-cluster-ServiceRole-1889QFJ9Q8DJY
  - arn: arn:aws:iam::123456789012:role/xyzctl-pr-ssc-xyz-poc-cluster-ServiceRole-1889QFJ9Q8DJY
  - id: AROAX2FJ77DCZQCWEQHHQ
  - path: /
  - description: ''
  - max_session_duration: 3600
  - tags:
    - Key: alpha.xyzctl.io/cluster-name
      Value: pr-ssc-xyz-poc
    - Key: xyzctl.cluster.k8s.io/v1alpha1/cluster-name
      Value: pr-ssc-xyz-poc
    - Key: alpha.xyzctl.io/xyzctl-version
      Value: 0.77.0
    - Key: Name
      Value: xyzctl-pr-ssc-xyz-poc-cluster/ServiceRole
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "xyz.amazonaws.com"}}], "Version": "2012-10-17"}'


xyzctl-pr-ssc-xyz-poc-cluster-ServiceRole-1889QFJ9Q8DJY-arn:aws:iam::aws:policy/AmazonxyzClusterPolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: xyzctl-pr-ssc-xyz-poc-cluster-ServiceRole-1889QFJ9Q8DJY
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzClusterPolicy


xyzctl-pr-ssc-xyz-poc-cluster-ServiceRole-1889QFJ9Q8DJY-arn:aws:iam::aws:policy/AmazonxyzVPCResourceController:
  aws.iam.role_policy_attachment.present:
  - role_name: xyzctl-pr-ssc-xyz-poc-cluster-ServiceRole-1889QFJ9Q8DJY
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzVPCResourceController


xyzctl-pr-ssc-xyz-poc-cluster-ServiceRole-1889QFJ9Q8DJY-xyzctl-pr-ssc-xyz-poc-cluster-PolicyCloudWatchMetrics:
  aws.iam.role_policy.present:
  - resource_id: xyzctl-pr-ssc-xyz-poc-cluster-ServiceRole-1889QFJ9Q8DJY-xyzctl-pr-ssc-xyz-poc-cluster-PolicyCloudWatchMetrics
  - role_name: xyzctl-pr-ssc-xyz-poc-cluster-ServiceRole-1889QFJ9Q8DJY
  - name: xyzctl-pr-ssc-xyz-poc-cluster-PolicyCloudWatchMetrics
  - policy_document: '{"Statement": [{"Action": ["cloudwatch:PutMetricData"], "Effect":
      "Allow", "Resource": "*"}], "Version": "2012-10-17"}'


xyzctl-pr-ssc-xyz-poc-cluster-ServiceRole-1889QFJ9Q8DJY-xyzctl-pr-ssc-xyz-poc-cluster-PolicyELBPermissions:
  aws.iam.role_policy.present:
  - resource_id: xyzctl-pr-ssc-xyz-poc-cluster-ServiceRole-1889QFJ9Q8DJY-xyzctl-pr-ssc-xyz-poc-cluster-PolicyELBPermissions
  - role_name: xyzctl-pr-ssc-xyz-poc-cluster-ServiceRole-1889QFJ9Q8DJY
  - name: xyzctl-pr-ssc-xyz-poc-cluster-PolicyELBPermissions
  - policy_document: '{"Statement": [{"Action": ["ec2:DescribeAccountAttributes",
      "ec2:DescribeAddresses", "ec2:DescribeInternetGateways"], "Effect": "Allow",
      "Resource": "*"}], "Version": "2012-10-17"}'


xyzctl-pr-ssc-xyz-poc-nodegroup-n-NodeInstanceRole-1RGEJRE3KZQ50:
  aws.iam.role.present:
  - resource_id: xyzctl-pr-ssc-xyz-poc-nodegroup-n-NodeInstanceRole-1RGEJRE3KZQ50
  - name: xyzctl-pr-ssc-xyz-poc-nodegroup-n-NodeInstanceRole-1RGEJRE3KZQ50
  - arn: arn:aws:iam::123456789012:role/xyzctl-pr-ssc-xyz-poc-nodegroup-n-NodeInstanceRole-1RGEJRE3KZQ50
  - id: AROAX2FJ77DCYLCUYMEWE
  - path: /
  - description: ''
  - max_session_duration: 3600
  - tags:
    - Key: alpha.xyzctl.io/nodegroup-name
      Value: ng-027a0eb6
    - Key: alpha.xyzctl.io/cluster-name
      Value: pr-ssc-xyz-poc
    - Key: xyzctl.cluster.k8s.io/v1alpha1/cluster-name
      Value: pr-ssc-xyz-poc
    - Key: alpha.xyzctl.io/nodegroup-type
      Value: managed
    - Key: alpha.xyzctl.io/xyzctl-version
      Value: 0.77.0
    - Key: Name
      Value: xyzctl-pr-ssc-xyz-poc-nodegroup-ng-027a0eb6/NodeInstanceRole
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "ec2.amazonaws.com"}}], "Version": "2012-10-17"}'


? xyzctl-pr-ssc-xyz-poc-nodegroup-n-NodeInstanceRole-1RGEJRE3KZQ50-arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly


: aws.iam.role_policy_attachment.present:
  - role_name: xyzctl-pr-ssc-xyz-poc-nodegroup-n-NodeInstanceRole-1RGEJRE3KZQ50
  - policy_arn: arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly


xyzctl-pr-ssc-xyz-poc-nodegroup-n-NodeInstanceRole-1RGEJRE3KZQ50-arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore:
  aws.iam.role_policy_attachment.present:
  - role_name: xyzctl-pr-ssc-xyz-poc-nodegroup-n-NodeInstanceRole-1RGEJRE3KZQ50
  - policy_arn: arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore


xyzctl-pr-ssc-xyz-poc-nodegroup-n-NodeInstanceRole-1RGEJRE3KZQ50-arn:aws:iam::aws:policy/AmazonxyzWorkerNodePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: xyzctl-pr-ssc-xyz-poc-nodegroup-n-NodeInstanceRole-1RGEJRE3KZQ50
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzWorkerNodePolicy


xyzctl-pr-ssc-xyz-poc-nodegroup-n-NodeInstanceRole-1RGEJRE3KZQ50-arn:aws:iam::aws:policy/Amazonxyz_CNI_Policy:
  aws.iam.role_policy_attachment.present:
  - role_name: xyzctl-pr-ssc-xyz-poc-nodegroup-n-NodeInstanceRole-1RGEJRE3KZQ50
  - policy_arn: arn:aws:iam::aws:policy/Amazonxyz_CNI_Policy


xyzfargateprofile-role:
  aws.iam.role.present:
  - resource_id: xyzfargateprofile-role
  - name: xyzfargateprofile-role
  - arn: arn:aws:iam::123456789012:role/xyzfargateprofile-role
  - id: AROAX2FJ77DC2FQOJAREM
  - path: /
  - description: Allows access to other AWS service resources that are required to
      run Amazon xyz pods on AWS Fargate.
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "xyz-fargate-pods.amazonaws.com"}}], "Version":
      "2012-10-17"}'


xyzfargateprofile-role-arn:aws:iam::aws:policy/AmazonxyzFargatePodExecutionRolePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: xyzfargateprofile-role
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzFargatePodExecutionRolePolicy
