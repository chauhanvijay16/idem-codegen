13.37.173.220:
  aws.ec2.elastic_ip.present:
  - name: 13.37.173.220
  - resource_id: 13.37.173.220
  - allocation_id: eipalloc-001d4219447c325ca
  - domain: vpc
  - network_border_group: eu-west-3
  - public_ipv4_pool: amazon
  - tags:
    - Key: Owner
      Value: org1
    - Key: Name
      Value: idem-test-natgw-eip-1
    - Key: COGS
      Value: OPEX
    - Key: Automation
      Value: 'true'
    - Key: Environment
      Value: test-dev
    - Key: KubernetesCluster
      Value: idem-test


13.38.205.2:
  aws.ec2.elastic_ip.present:
  - name: 13.38.205.2
  - resource_id: 13.38.205.2
  - allocation_id: eipalloc-01319ee06efe14298
  - domain: vpc
  - network_border_group: eu-west-3
  - public_ipv4_pool: amazon
  - tags:
    - Key: COGS
      Value: OPEX
    - Key: Name
      Value: idem-test-natgw-eip-2
    - Key: Owner
      Value: org1
    - Key: Automation
      Value: 'true'
    - Key: Environment
      Value: test-dev
    - Key: KubernetesCluster
      Value: idem-test


15.236.223.139:
  aws.ec2.elastic_ip.present:
  - name: 15.236.223.139
  - resource_id: 15.236.223.139
  - allocation_id: eipalloc-0134ceb9112c887fd
  - domain: vpc
  - network_border_group: eu-west-3
  - public_ipv4_pool: amazon
  - tags:
    - Key: COGS
      Value: OPEX
    - Key: Name
      Value: idem-test-natgw-eip-0
    - Key: Owner
      Value: org1
    - Key: Automation
      Value: 'true'
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Environment
      Value: test-dev


dopt-00338f328b40be8f0:
  aws.ec2.dhcp_option.present:
  - name: dopt-00338f328b40be8f0
  - resource_id: dopt-00338f328b40be8f0
  - tags:
    - Key: test_name222
      Value: test-dhcp333
  - dhcp_configurations:
    - Key: domain-name
      Values:
      - testing123
    - Key: domain-name-servers
      Values:
      - 10.2.5.1
      - 10.2.5.2
    - Key: netbios-node-type
      Values:
      - '2'


dopt-00756d50a1f5125a0:
  aws.ec2.dhcp_option.present:
  - name: dopt-00756d50a1f5125a0
  - resource_id: dopt-00756d50a1f5125a0
  - tags:
    - Key: test_name222
      Value: test-dhcp333
  - dhcp_configurations:
    - Key: domain-name
      Values:
      - testing123
    - Key: domain-name-servers
      Values:
      - 10.2.5.1
      - 10.2.5.2
    - Key: netbios-node-type
      Values:
      - '2'


dopt-0131e72ab5197b3ef:
  aws.ec2.dhcp_option.present:
  - name: dopt-0131e72ab5197b3ef
  - resource_id: dopt-0131e72ab5197b3ef
  - tags:
    - Key: test_name222
      Value: test-dhcp333
  - dhcp_configurations:
    - Key: domain-name
      Values:
      - testing123
    - Key: domain-name-servers
      Values:
      - 10.2.5.1
      - 10.2.5.2
    - Key: netbios-node-type
      Values:
      - '2'


dopt-014ff111b2c222df6:
  aws.ec2.dhcp_option.present:
  - name: dopt-014ff111b2c222df6
  - resource_id: dopt-014ff111b2c222df6
  - tags:
    - Key: test_name222
      Value: test-dhcp333
  - dhcp_configurations:
    - Key: domain-name
      Values:
      - testing123
    - Key: domain-name-servers
      Values:
      - 10.2.5.1
      - 10.2.5.2
    - Key: netbios-node-type
      Values:
      - '2'


dopt-0155e2b56140c3584:
  aws.ec2.dhcp_option.present:
  - name: dopt-0155e2b56140c3584
  - resource_id: dopt-0155e2b56140c3584
  - tags:
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Name
      Value: xyz-idem-test
    - Key: Automation
      Value: 'true'
    - Key: Owner
      Value: org1
    - Key: COGS
      Value: OPEX
    - Key: Environment
      Value: test-dev
  - dhcp_configurations:
    - Key: domain-name
      Values:
      - us-west-2.compute.internal
    - Key: domain-name-servers
      Values:
      - AmazonProvidedDNS


dopt-036afdf595a5d3bb5:
  aws.ec2.dhcp_option.present:
  - name: dopt-036afdf595a5d3bb5
  - resource_id: dopt-036afdf595a5d3bb5
  - tags: []
  - dhcp_configurations:
    - Key: domain-name-servers
      Values:
      - 10.2.5.1
      - 10.2.5.2


dopt-0480c4df211668e3a:
  aws.ec2.dhcp_option.present:
  - name: dopt-0480c4df211668e3a
  - resource_id: dopt-0480c4df211668e3a
  - tags:
    - Key: Environment
      Value: test-dev
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Automation
      Value: 'true'
    - Key: COGS
      Value: OPEX
    - Key: Owner
      Value: org1
    - Key: Name
      Value: xyz-idem-test
  - dhcp_configurations:
    - Key: domain-name
      Values:
      - us-west-2.compute.internal
    - Key: domain-name-servers
      Values:
      - AmazonProvidedDNS


dopt-04be1a8d8d0826a24:
  aws.ec2.dhcp_option.present:
  - name: dopt-04be1a8d8d0826a24
  - resource_id: dopt-04be1a8d8d0826a24
  - tags:
    - Key: test_name222
      Value: test-dhcp333
  - dhcp_configurations:
    - Key: domain-name
      Values:
      - testing123
    - Key: domain-name-servers
      Values:
      - 10.2.5.1
      - 10.2.5.2
    - Key: netbios-node-type
      Values:
      - '2'


dopt-052512c363a0a800b:
  aws.ec2.dhcp_option.present:
  - name: dopt-052512c363a0a800b
  - resource_id: dopt-052512c363a0a800b
  - tags:
    - Key: COGS
      Value: OPEX
    - Key: Owner
      Value: org1
    - Key: Name
      Value: xyz-idem-test
    - Key: Environment
      Value: test-dev
    - Key: Automation
      Value: 'true'
    - Key: KubernetesCluster
      Value: idem-test
  - dhcp_configurations:
    - Key: domain-name
      Values:
      - us-west-2.compute.internal
    - Key: domain-name-servers
      Values:
      - AmazonProvidedDNS


dopt-0656233c5899a635a:
  aws.ec2.dhcp_option.present:
  - name: dopt-0656233c5899a635a
  - resource_id: dopt-0656233c5899a635a
  - tags: []
  - dhcp_configurations:
    - Key: domain-name-servers
      Values:
      - 10.2.5.1
      - 10.2.5.2


dopt-06763700a2b49ea57:
  aws.ec2.dhcp_option.present:
  - name: dopt-06763700a2b49ea57
  - resource_id: dopt-06763700a2b49ea57
  - tags:
    - Key: test_name222
      Value: test-dhcp333
  - dhcp_configurations:
    - Key: domain-name
      Values:
      - testing123
    - Key: domain-name-servers
      Values:
      - 10.2.5.1
      - 10.2.5.2
    - Key: netbios-node-type
      Values:
      - '2'


dopt-078d9c1e9e0cd4bdb:
  aws.ec2.dhcp_option.present:
  - name: dopt-078d9c1e9e0cd4bdb
  - resource_id: dopt-078d9c1e9e0cd4bdb
  - tags:
    - Key: test_name222
      Value: test-dhcp333
  - dhcp_configurations:
    - Key: domain-name
      Values:
      - testing123
    - Key: domain-name-servers
      Values:
      - 10.2.5.1
      - 10.2.5.2
    - Key: netbios-node-type
      Values:
      - '2'


dopt-0882a9c4d825793ee:
  aws.ec2.dhcp_option.present:
  - name: dopt-0882a9c4d825793ee
  - resource_id: dopt-0882a9c4d825793ee
  - tags:
    - Key: COGS
      Value: OPEX
    - Key: Name
      Value: xyz-idem-test
    - Key: Environment
      Value: test-dev
    - Key: Automation
      Value: 'true'
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Owner
      Value: org1
  - dhcp_configurations:
    - Key: domain-name
      Values:
      - us-west-2.compute.internal
    - Key: domain-name-servers
      Values:
      - AmazonProvidedDNS


dopt-0ae8c394c563d5ee8:
  aws.ec2.dhcp_option.present:
  - name: dopt-0ae8c394c563d5ee8
  - resource_id: dopt-0ae8c394c563d5ee8
  - tags:
    - Key: test_name222
      Value: test-dhcp333
  - dhcp_configurations:
    - Key: domain-name
      Values:
      - testing123
    - Key: domain-name-servers
      Values:
      - 10.2.5.1
      - 10.2.5.2
    - Key: netbios-node-type
      Values:
      - '2'


dopt-0b8e3e4566083cc5e:
  aws.ec2.dhcp_option.present:
  - name: dopt-0b8e3e4566083cc5e
  - resource_id: dopt-0b8e3e4566083cc5e
  - tags:
    - Key: test_name222
      Value: test-dhcp333
  - dhcp_configurations:
    - Key: domain-name
      Values:
      - testing123
    - Key: domain-name-servers
      Values:
      - 10.2.5.1
      - 10.2.5.2
    - Key: netbios-node-type
      Values:
      - '2'


dopt-0c038d5d17f706f51:
  aws.ec2.dhcp_option.present:
  - name: dopt-0c038d5d17f706f51
  - resource_id: dopt-0c038d5d17f706f51
  - tags:
    - Key: test_name222
      Value: test-dhcp333
  - dhcp_configurations:
    - Key: domain-name
      Values:
      - testing123
    - Key: domain-name-servers
      Values:
      - 10.2.5.1
      - 10.2.5.2
    - Key: netbios-node-type
      Values:
      - '2'


dopt-0f26285d884de8f6b:
  aws.ec2.dhcp_option.present:
  - name: dopt-0f26285d884de8f6b
  - resource_id: dopt-0f26285d884de8f6b
  - tags:
    - Key: Owner
      Value: org1
    - Key: COGS
      Value: OPEX
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Environment
      Value: test-dev
    - Key: Automation
      Value: 'true'
    - Key: Name
      Value: xyz-idem-test
  - dhcp_configurations:
    - Key: domain-name
      Values:
      - us-west-2.compute.internal
    - Key: domain-name-servers
      Values:
      - AmazonProvidedDNS


dopt-68ef1501:
  aws.ec2.dhcp_option.present:
  - name: dopt-68ef1501
  - resource_id: dopt-68ef1501
  - tags: []
  - dhcp_configurations:
    - Key: domain-name
      Values:
      - eu-west-3.compute.internal
    - Key: domain-name-servers
      Values:
      - AmazonProvidedDNS


fl-02e639031d2f37592:
  aws.ec2.flow_log.present:
  - resource_ids:
    - vpc-02e366ce9fa15dd56
  - resource_type: VPC
  - resource_id: fl-02e639031d2f37592
  - iam_role: arn:aws:iam::123456789012:role/xyz-idem-test_redlock_flow_role
  - log_group_name: xyz-idem-test_redlock_flow_log_group
  - traffic_type: ALL
  - log_destination_type: cloud-watch-logs
  - log_destination: arn:aws:logs:eu-west-3:123456789012:log-group:xyz-idem-test_redlock_flow_log_group:*
  - log_format: ${version} ${account-id} ${interface-id} ${srcaddr} ${dstaddr} ${srcport}
      ${dstport} ${protocol} ${packets} ${bytes} ${start} ${end} ${action} ${log-status}
  - max_aggregation_interval: 600
  - tags: []


fl-0604c326e4d398406:
  aws.ec2.flow_log.present:
  - resource_ids:
    - vpc-0c06512aed6737787
  - resource_type: VPC
  - resource_id: fl-0604c326e4d398406
  - iam_role: arn:aws:iam::123456789012:role/xyz-idem-test_redlock_flow_role
  - log_group_name: xyz-idem-test_redlock_flow_log_group
  - traffic_type: ALL
  - log_destination_type: cloud-watch-logs
  - log_destination: arn:aws:logs:eu-west-3:123456789012:log-group:xyz-idem-test_redlock_flow_log_group:*
  - log_format: ${version} ${account-id} ${interface-id} ${srcaddr} ${dstaddr} ${srcport}
      ${dstport} ${protocol} ${packets} ${bytes} ${start} ${end} ${action} ${log-status}
  - max_aggregation_interval: 600
  - tags: []


fl-0761ee74ec872ab13:
  aws.ec2.flow_log.present:
  - resource_ids:
    - vpc-079e00e8877b676f8
  - resource_type: VPC
  - resource_id: fl-0761ee74ec872ab13
  - iam_role: arn:aws:iam::123456789012:role/xyz-idem-test_redlock_flow_role
  - log_group_name: xyz-idem-test_redlock_flow_log_group
  - traffic_type: ALL
  - log_destination_type: cloud-watch-logs
  - log_destination: arn:aws:logs:eu-west-3:123456789012:log-group:xyz-idem-test_redlock_flow_log_group:*
  - log_format: ${version} ${account-id} ${interface-id} ${srcaddr} ${dstaddr} ${srcport}
      ${dstport} ${protocol} ${packets} ${bytes} ${start} ${end} ${action} ${log-status}
  - max_aggregation_interval: 600
  - tags: []


fl-0d8347dbe88a0027a:
  aws.ec2.flow_log.present:
  - resource_ids:
    - vpc-0738f2a523f4735bd
  - resource_type: VPC
  - resource_id: fl-0d8347dbe88a0027a
  - iam_role: arn:aws:iam::123456789012:role/xyz-idem-test_redlock_flow_role
  - log_group_name: xyz-idem-test_redlock_flow_log_group
  - traffic_type: ALL
  - log_destination_type: cloud-watch-logs
  - log_destination: arn:aws:logs:eu-west-3:123456789012:log-group:xyz-idem-test_redlock_flow_log_group
  - log_format: ${version} ${account-id} ${interface-id} ${srcaddr} ${dstaddr} ${srcport}
      ${dstport} ${protocol} ${packets} ${bytes} ${start} ${end} ${action} ${log-status}
  - max_aggregation_interval: 600
  - tags: []


igw-0eee9bba485b312a8:
  aws.ec2.internet_gateway.present:
  - attachments:
    - State: available
      VpcId: vpc-0738f2a523f4735bd
    name: igw-0eee9bba485b312a8
    resource_id: igw-0eee9bba485b312a8
    tags:
    - Key: Name
      Value: idem-test-temp-xyz
    - Key: COGS
      Value: OPEX
    - Key: Automation
      Value: 'true'
    - Key: Owner
      Value: org1
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Environment
      Value: test-dev
    vpc_id:
    - vpc-0738f2a523f4735bd


igw-89798ae0:
  aws.ec2.internet_gateway.present:
  - attachments:
    - State: available
      VpcId: vpc-dcae57b5
    name: igw-89798ae0
    resource_id: igw-89798ae0
    vpc_id:
    - vpc-dcae57b5


nat-076cd14a28acd21b4:
  aws.ec2.nat_gateway.present:
  - name: nat-076cd14a28acd21b4
  - resource_id: nat-076cd14a28acd21b4
  - subnet_id: subnet-0d68d61b1ab708d42
  - connectivity_type: public
  - tags:
    - Key: Environment
      Value: test-dev
    - Key: Automation
      Value: 'true'
    - Key: Owner
      Value: org1
    - Key: KubernetesCluster
      Value: idem-test
    - Key: COGS
      Value: OPEX
    - Key: Name
      Value: idem-test-natgw-2
  - state: available
  - allocation_id: eipalloc-01319ee06efe14298


nat-0a49a65a4bb87370a:
  aws.ec2.nat_gateway.present:
  - name: nat-0a49a65a4bb87370a
  - resource_id: nat-0a49a65a4bb87370a
  - subnet_id: subnet-09cecc8c853637d3b
  - connectivity_type: public
  - tags:
    - Key: Environment
      Value: test-dev
    - Key: Automation
      Value: 'true'
    - Key: Owner
      Value: org1
    - Key: KubernetesCluster
      Value: idem-test
    - Key: COGS
      Value: OPEX
    - Key: Name
      Value: idem-test-natgw-0
  - state: available
  - allocation_id: eipalloc-0134ceb9112c887fd


nat-0c02a1f1d590b5534:
  aws.ec2.nat_gateway.present:
  - name: nat-0c02a1f1d590b5534
  - resource_id: nat-0c02a1f1d590b5534
  - subnet_id: subnet-0094b72dfb7ce6131
  - connectivity_type: public
  - tags:
    - Key: Environment
      Value: test-dev
    - Key: Automation
      Value: 'true'
    - Key: Owner
      Value: org1
    - Key: KubernetesCluster
      Value: idem-test
    - Key: COGS
      Value: OPEX
    - Key: Name
      Value: idem-test-natgw-1
  - state: available
  - allocation_id: eipalloc-001d4219447c325ca


rtb-01e542a8c56c9511f:
  aws.ec2.route_table.present:
  - associations:
    - AssociationState:
        State: associated
      Main: false
      RouteTableAssociationId: rtbassoc-0f2b91e5f78d7af47
      RouteTableId: rtb-01e542a8c56c9511f
      SubnetId: subnet-09cecc8c853637d3b
    name: rtb-01e542a8c56c9511f
    propagating_vgws: []
    resource_id: rtb-01e542a8c56c9511f
    routes:
    - DestinationCidrBlock: 10.170.0.0/16
      GatewayId: local
      Origin: CreateRouteTable
      State: active
    - DestinationCidrBlock: 0.0.0.0/0
      GatewayId: igw-0eee9bba485b312a8
      Origin: CreateRoute
      State: active
    tags:
    - Key: Environment
      Value: test-dev
    - Key: Name
      Value: idem-test-xyz-public-0
    - Key: Automation
      Value: 'true'
    - Key: COGS
      Value: OPEX
    - Key: Owner
      Value: org1
    - Key: KubernetesCluster
      Value: idem-test
    vpc_id: vpc-0738f2a523f4735bd


rtb-0445912793473da66:
  aws.ec2.route_table.present:
  - associations:
    - AssociationState:
        State: associated
      Main: false
      RouteTableAssociationId: rtbassoc-05b12a843fea97f87
      RouteTableId: rtb-0445912793473da66
      SubnetId: subnet-0094b72dfb7ce6131
    name: rtb-0445912793473da66
    propagating_vgws: []
    resource_id: rtb-0445912793473da66
    routes:
    - DestinationCidrBlock: 10.170.0.0/16
      GatewayId: local
      Origin: CreateRouteTable
      State: active
    - DestinationCidrBlock: 0.0.0.0/0
      GatewayId: igw-0eee9bba485b312a8
      Origin: CreateRoute
      State: active
    tags:
    - Key: Name
      Value: idem-test-xyz-public-1
    - Key: COGS
      Value: OPEX
    - Key: Automation
      Value: 'true'
    - Key: Environment
      Value: test-dev
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Owner
      Value: org1
    vpc_id: vpc-0738f2a523f4735bd


rtb-0516e0e06d933d9f4:
  aws.ec2.route_table.present:
  - associations:
    - AssociationState:
        State: associated
      Main: false
      RouteTableAssociationId: rtbassoc-080a96c3caa4d67bc
      RouteTableId: rtb-0516e0e06d933d9f4
      SubnetId: subnet-050732fa4616470d9
    name: rtb-0516e0e06d933d9f4
    propagating_vgws: []
    resource_id: rtb-0516e0e06d933d9f4
    routes:
    - DestinationCidrBlock: 10.170.0.0/16
      GatewayId: local
      Origin: CreateRouteTable
      State: active
    - DestinationCidrBlock: 0.0.0.0/0
      NatGatewayId: nat-0a49a65a4bb87370a
      Origin: CreateRoute
      State: active
    tags:
    - Key: COGS
      Value: OPEX
    - Key: Environment
      Value: test-dev
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Name
      Value: idem-test-xyz-private-0
    - Key: Owner
      Value: org1
    - Key: Automation
      Value: 'true'
    vpc_id: vpc-0738f2a523f4735bd


rtb-05bd8f7251c25d82c:
  aws.ec2.route_table.present:
  - associations:
    - AssociationState:
        State: associated
      Main: false
      RouteTableAssociationId: rtbassoc-0e7f80eb1863284da
      RouteTableId: rtb-05bd8f7251c25d82c
      SubnetId: subnet-0d68d61b1ab708d42
    name: rtb-05bd8f7251c25d82c
    propagating_vgws: []
    resource_id: rtb-05bd8f7251c25d82c
    routes:
    - DestinationCidrBlock: 10.170.0.0/16
      GatewayId: local
      Origin: CreateRouteTable
      State: active
    - DestinationCidrBlock: 0.0.0.0/0
      GatewayId: igw-0eee9bba485b312a8
      Origin: CreateRoute
      State: active
    tags:
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Owner
      Value: org1
    - Key: COGS
      Value: OPEX
    - Key: Automation
      Value: 'true'
    - Key: Name
      Value: idem-test-xyz-public-2
    - Key: Environment
      Value: test-dev
    vpc_id: vpc-0738f2a523f4735bd


rtb-0752eb6244ce03cff:
  aws.ec2.route_table.present:
  - associations: []
    name: rtb-0752eb6244ce03cff
    propagating_vgws: []
    resource_id: rtb-0752eb6244ce03cff
    routes:
    - DestinationCidrBlock: 10.0.0.0/16
      GatewayId: local
      Origin: CreateRouteTable
      State: active
    tags:
    - Key: Name
      Value: vpc-0dbee6437661777d4
    vpc_id: vpc-0dbee6437661777d4


rtb-0b0a3c628c59ac049:
  aws.ec2.route_table.present:
  - associations:
    - AssociationState:
        State: associated
      Main: false
      RouteTableAssociationId: rtbassoc-0ef8af8654a7c1fb3
      RouteTableId: rtb-0b0a3c628c59ac049
      SubnetId: subnet-039e53122e038d38c
    name: rtb-0b0a3c628c59ac049
    propagating_vgws: []
    resource_id: rtb-0b0a3c628c59ac049
    routes:
    - DestinationCidrBlock: 10.170.0.0/16
      GatewayId: local
      Origin: CreateRouteTable
      State: active
    - DestinationCidrBlock: 0.0.0.0/0
      NatGatewayId: nat-076cd14a28acd21b4
      Origin: CreateRoute
      State: active
    tags:
    - Key: COGS
      Value: OPEX
    - Key: Owner
      Value: org1
    - Key: Name
      Value: idem-test-xyz-private-2
    - Key: Environment
      Value: test-dev
    - Key: Automation
      Value: 'true'
    - Key: KubernetesCluster
      Value: idem-test
    vpc_id: vpc-0738f2a523f4735bd


rtb-0e8c5df719166603e:
  aws.ec2.route_table.present:
  - associations: []
    name: rtb-0e8c5df719166603e
    propagating_vgws: []
    resource_id: rtb-0e8c5df719166603e
    routes:
    - DestinationCidrBlock: 10.170.0.0/16
      GatewayId: local
      Origin: CreateRouteTable
      State: active
    tags: []
    vpc_id: vpc-0738f2a523f4735bd


rtb-0ee77d1deb1a1d86a:
  aws.ec2.route_table.present:
  - associations:
    - AssociationState:
        State: associated
      Main: false
      RouteTableAssociationId: rtbassoc-0832ce3bb146df047
      RouteTableId: rtb-0ee77d1deb1a1d86a
      SubnetId: subnet-05dfaa0d01a337199
    name: rtb-0ee77d1deb1a1d86a
    propagating_vgws: []
    resource_id: rtb-0ee77d1deb1a1d86a
    routes:
    - DestinationCidrBlock: 10.170.0.0/16
      GatewayId: local
      Origin: CreateRouteTable
      State: active
    - DestinationCidrBlock: 0.0.0.0/0
      NatGatewayId: nat-0c02a1f1d590b5534
      Origin: CreateRoute
      State: active
    tags:
    - Key: Owner
      Value: org1
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Name
      Value: idem-test-xyz-private-1
    - Key: Environment
      Value: test-dev
    - Key: COGS
      Value: OPEX
    - Key: Automation
      Value: 'true'
    vpc_id: vpc-0738f2a523f4735bd


rtb-0f840e96646e44c53:
  aws.ec2.route_table.present:
  - associations: []
    name: rtb-0f840e96646e44c53
    propagating_vgws: []
    resource_id: rtb-0f840e96646e44c53
    routes:
    - DestinationCidrBlock: 10.1.150.0/28
      GatewayId: local
      Origin: CreateRouteTable
      State: active
    tags: []
    vpc_id: vpc-08f76fe175071e969


rtb-6bcc0d02:
  aws.ec2.route_table.present:
  - associations: []
    name: rtb-6bcc0d02
    propagating_vgws: []
    resource_id: rtb-6bcc0d02
    routes:
    - DestinationCidrBlock: 172.31.0.0/16
      GatewayId: local
      Origin: CreateRouteTable
      State: active
    - DestinationCidrBlock: 0.0.0.0/0
      GatewayId: igw-89798ae0
      Origin: CreateRoute
      State: active
    tags:
    - Key: Name
      Value: idem-test
    vpc_id: vpc-dcae57b5


sg-01a41f68:
  aws.ec2.security_group.present:
  - resource_id: sg-01a41f68
  - name: rds-launch-wizard-2
  - vpc_id: vpc-dcae57b5
  - tags:
    - Key: AutomaticCleanExpirationTime
      Value: '2020-07-10T09:57:33.882Z'
  - description: 'Created from the RDS Management Console: 2018/01/28 19:09:27'


sg-02b4fa0698eaa06cf:
  aws.ec2.security_group.present:
  - resource_id: sg-02b4fa0698eaa06cf
  - name: idem-test-temp-xyz-cluster-node
  - vpc_id: vpc-0738f2a523f4735bd
  - tags:
    - Key: cluster
      Value: xyz
    - Key: Environment
      Value: test-dev
    - Key: Name
      Value: idem-test-temp-xyz-cluster-node
    - Key: KubernetesCluster
      Value: idem-test
    - Key: role
      Value: xyz-worker
    - Key: kubernetes.io/cluster/idem-test
      Value: owned
    - Key: Owner
      Value: org1
    - Key: Automation
      Value: 'true'
    - Key: COGS
      Value: OPEX
  - description: Security group for all nodes in the cluster


sg-04fa08f7b284cd1e9:
  aws.ec2.security_group.present:
  - resource_id: sg-04fa08f7b284cd1e9
  - name: default
  - vpc_id: vpc-0738f2a523f4735bd
  - description: default VPC security group


sg-06552329287f9b206:
  aws.ec2.security_group.present:
  - resource_id: sg-06552329287f9b206
  - name: temp-20220505111435890800000003
  - vpc_id: vpc-0738f2a523f4735bd
  - tags:
    - Key: Name
      Value: idem-test-nessus_vuln_scanner
  - description: nessus scanner


sg-070a797a4b433814b:
  aws.ec2.security_group.present:
  - resource_id: sg-070a797a4b433814b
  - name: idem-test-temp-xyz-cluster
  - vpc_id: vpc-0738f2a523f4735bd
  - tags:
    - Key: Environment
      Value: test-dev
    - Key: Automation
      Value: 'true'
    - Key: KubernetesCluster
      Value: idem-test
    - Key: COGS
      Value: OPEX
    - Key: Name
      Value: idem-test-temp-xyz
    - Key: Owner
      Value: org1
  - description: Cluster communication with worker nodes


sg-07400e9684a4b7a48:
  aws.ec2.security_group.present:
  - resource_id: sg-07400e9684a4b7a48
  - name: idem-fixture-security-group-eb54958c-7199-4034-8f0f-83a62bcf1dc5
  - vpc_id: vpc-0dbee6437661777d4
  - tags:
    - Key: Name
      Value: idem-fixture-security-group-eb54958c-7199-4034-8f0f-83a62bcf1dc5
  - description: Created for Idem integration test.


sg-0d129413d7cea1757:
  aws.ec2.security_group.present:
  - resource_id: sg-0d129413d7cea1757
  - name: default
  - vpc_id: vpc-0dbee6437661777d4
  - description: default VPC security group


sg-0edb77cb85ac5f73e:
  aws.ec2.security_group.present:
  - resource_id: sg-0edb77cb85ac5f73e
  - name: default
  - vpc_id: vpc-08f76fe175071e969
  - description: default VPC security group


sg-0f9910c81ca733164:
  aws.ec2.security_group.present:
  - resource_id: sg-0f9910c81ca733164
  - name: xyz-cluster-sg-idem-test-7532260
  - vpc_id: vpc-0738f2a523f4735bd
  - tags:
    - Key: kubernetes.io/cluster/idem-test
      Value: owned
    - Key: aws:xyz:cluster-name
      Value: idem-test
    - Key: Name
      Value: xyz-cluster-sg-idem-test-7532260
  - description: xyz created security group applied to ENI that is attached to xyz
      Control Plane master nodes, as well as any managed workloads.


sg-0fc412cebfb987038:
  aws.ec2.security_group.present:
  - resource_id: sg-0fc412cebfb987038
  - name: photon-model-sg
  - vpc_id: vpc-dcae57b5
  - description: abc Photon model security group


sg-d68355bf:
  aws.ec2.security_group.present:
  - resource_id: sg-d68355bf
  - name: default
  - vpc_id: vpc-dcae57b5
  - description: default VPC security group


sgr-00140d3e51aad2e43:
  aws.ec2.security_group_rule.present:
  - name: sgr-00140d3e51aad2e43
  - resource_id: sgr-00140d3e51aad2e43
  - group_id: sg-04fa08f7b284cd1e9
  - is_egress: false
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - tags: []
  - referenced_group_info:
      GroupId: sg-04fa08f7b284cd1e9
      UserId: '123456789012'


sgr-00ed93ac23eed55f6:
  aws.ec2.security_group_rule.present:
  - name: sgr-00ed93ac23eed55f6
  - resource_id: sgr-00ed93ac23eed55f6
  - group_id: sg-04fa08f7b284cd1e9
  - is_egress: true
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


sgr-01f124742859acf30:
  aws.ec2.security_group_rule.present:
  - name: sgr-01f124742859acf30
  - resource_id: sgr-01f124742859acf30
  - group_id: sg-0fc412cebfb987038
  - is_egress: false
  - ip_protocol: tcp
  - from_port: 8080
  - to_port: 8080
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


sgr-02dcf5ca5bb9218b9:
  aws.ec2.security_group_rule.present:
  - name: sgr-02dcf5ca5bb9218b9
  - resource_id: sgr-02dcf5ca5bb9218b9
  - group_id: sg-0fc412cebfb987038
  - is_egress: false
  - ip_protocol: tcp
  - from_port: 1
  - to_port: 65535
  - cidr_ipv4: 172.31.0.0/16
  - tags: []


sgr-0336bcc760f16097a:
  aws.ec2.security_group_rule.present:
  - name: sgr-0336bcc760f16097a
  - resource_id: sgr-0336bcc760f16097a
  - group_id: sg-0d129413d7cea1757
  - is_egress: false
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - tags: []
  - referenced_group_info:
      GroupId: sg-0d129413d7cea1757
      UserId: '123456789012'


sgr-04299d060ac3ebe2a:
  aws.ec2.security_group_rule.present:
  - name: sgr-04299d060ac3ebe2a
  - resource_id: sgr-04299d060ac3ebe2a
  - group_id: sg-0fc412cebfb987038
  - is_egress: false
  - ip_protocol: tcp
  - from_port: 2376
  - to_port: 2376
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


sgr-0470200c0811d69df:
  aws.ec2.security_group_rule.present:
  - name: sgr-0470200c0811d69df
  - resource_id: sgr-0470200c0811d69df
  - group_id: sg-0edb77cb85ac5f73e
  - is_egress: false
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - tags: []
  - referenced_group_info:
      GroupId: sg-0edb77cb85ac5f73e
      UserId: '123456789012'


sgr-04baf6a7aeec7027c:
  aws.ec2.security_group_rule.present:
  - name: sgr-04baf6a7aeec7027c
  - resource_id: sgr-04baf6a7aeec7027c
  - group_id: sg-0d129413d7cea1757
  - is_egress: true
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


sgr-05e5771dece9c1ed9:
  aws.ec2.security_group_rule.present:
  - name: sgr-05e5771dece9c1ed9
  - resource_id: sgr-05e5771dece9c1ed9
  - group_id: sg-0fc412cebfb987038
  - is_egress: false
  - ip_protocol: tcp
  - from_port: 443
  - to_port: 443
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


sgr-05e6ceaaebacf5dc1:
  aws.ec2.security_group_rule.present:
  - name: sgr-05e6ceaaebacf5dc1
  - resource_id: sgr-05e6ceaaebacf5dc1
  - group_id: sg-d68355bf
  - is_egress: false
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - tags: []
  - referenced_group_info:
      GroupId: sg-d68355bf
      UserId: '123456789012'


sgr-06960cb41fe34b17f:
  aws.ec2.security_group_rule.present:
  - name: sgr-06960cb41fe34b17f
  - resource_id: sgr-06960cb41fe34b17f
  - group_id: sg-0f9910c81ca733164
  - is_egress: true
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


sgr-06de8bb9a233fe279:
  aws.ec2.security_group_rule.present:
  - name: sgr-06de8bb9a233fe279
  - resource_id: sgr-06de8bb9a233fe279
  - group_id: sg-0edb77cb85ac5f73e
  - is_egress: true
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


sgr-06fa8980bf9bffea7:
  aws.ec2.security_group_rule.present:
  - name: sgr-06fa8980bf9bffea7
  - resource_id: sgr-06fa8980bf9bffea7
  - group_id: sg-02b4fa0698eaa06cf
  - is_egress: false
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - tags: []
  - description: Allow worker Kubelets and pods to receive communication from the
      nessus scanner
  - referenced_group_info:
      GroupId: sg-06552329287f9b206
      UserId: '123456789012'


sgr-0746837a711b2f632:
  aws.ec2.security_group_rule.present:
  - name: sgr-0746837a711b2f632
  - resource_id: sgr-0746837a711b2f632
  - group_id: sg-02b4fa0698eaa06cf
  - is_egress: false
  - ip_protocol: tcp
  - from_port: 1025
  - to_port: 65535
  - tags: []
  - description: Allow worker Kubelets and pods to receive communication from the
      cluster control plane
  - referenced_group_info:
      GroupId: sg-070a797a4b433814b
      UserId: '123456789012'


sgr-0748e2625a1c6b9b1:
  aws.ec2.security_group_rule.present:
  - name: sgr-0748e2625a1c6b9b1
  - resource_id: sgr-0748e2625a1c6b9b1
  - group_id: sg-0f9910c81ca733164
  - is_egress: false
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - tags: []
  - referenced_group_info:
      GroupId: sg-0f9910c81ca733164
      UserId: '123456789012'


sgr-077c2651cc2eb9b1f:
  aws.ec2.security_group_rule.present:
  - name: sgr-077c2651cc2eb9b1f
  - resource_id: sgr-077c2651cc2eb9b1f
  - group_id: sg-070a797a4b433814b
  - is_egress: false
  - ip_protocol: tcp
  - from_port: 443
  - to_port: 443
  - tags: []
  - description: Allow pods to communicate with the cluster API Server
  - referenced_group_info:
      GroupId: sg-02b4fa0698eaa06cf
      UserId: '123456789012'


sgr-08163ca9869d9fce3:
  aws.ec2.security_group_rule.present:
  - name: sgr-08163ca9869d9fce3
  - resource_id: sgr-08163ca9869d9fce3
  - group_id: sg-01a41f68
  - is_egress: false
  - ip_protocol: tcp
  - from_port: 5432
  - to_port: 5432
  - cidr_ipv4: 24.7.88.198/32
  - tags: []


sgr-0895dc3282b1068e5:
  aws.ec2.security_group_rule.present:
  - name: sgr-0895dc3282b1068e5
  - resource_id: sgr-0895dc3282b1068e5
  - group_id: sg-06552329287f9b206
  - is_egress: true
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


sgr-09a323e0c1f512b46:
  aws.ec2.security_group_rule.present:
  - name: sgr-09a323e0c1f512b46
  - resource_id: sgr-09a323e0c1f512b46
  - group_id: sg-0fc412cebfb987038
  - is_egress: false
  - ip_protocol: tcp
  - from_port: 22
  - to_port: 22
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


sgr-0a1c13bf84aef05b3:
  aws.ec2.security_group_rule.present:
  - name: sgr-0a1c13bf84aef05b3
  - resource_id: sgr-0a1c13bf84aef05b3
  - group_id: sg-0fc412cebfb987038
  - is_egress: true
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


sgr-0a35370111c0c05fe:
  aws.ec2.security_group_rule.present:
  - name: sgr-0a35370111c0c05fe
  - resource_id: sgr-0a35370111c0c05fe
  - group_id: sg-0fc412cebfb987038
  - is_egress: false
  - ip_protocol: tcp
  - from_port: 2375
  - to_port: 2375
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


sgr-0a410f396195a1a29:
  aws.ec2.security_group_rule.present:
  - name: sgr-0a410f396195a1a29
  - resource_id: sgr-0a410f396195a1a29
  - group_id: sg-02b4fa0698eaa06cf
  - is_egress: true
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


sgr-0a4ef3a78cdce5042:
  aws.ec2.security_group_rule.present:
  - name: sgr-0a4ef3a78cdce5042
  - resource_id: sgr-0a4ef3a78cdce5042
  - group_id: sg-070a797a4b433814b
  - is_egress: true
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


sgr-0b6d618ed84f17b83:
  aws.ec2.security_group_rule.present:
  - name: sgr-0b6d618ed84f17b83
  - resource_id: sgr-0b6d618ed84f17b83
  - group_id: sg-02b4fa0698eaa06cf
  - is_egress: false
  - ip_protocol: tcp
  - from_port: 22
  - to_port: 22
  - cidr_ipv4: 10.170.0.0/16
  - tags: []
  - description: Allow bastion to communicate with worker nodes


sgr-0ca3ad84b6ac0506e:
  aws.ec2.security_group_rule.present:
  - name: sgr-0ca3ad84b6ac0506e
  - resource_id: sgr-0ca3ad84b6ac0506e
  - group_id: sg-01a41f68
  - is_egress: true
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


sgr-0d08ef367d62e82f3:
  aws.ec2.security_group_rule.present:
  - name: sgr-0d08ef367d62e82f3
  - resource_id: sgr-0d08ef367d62e82f3
  - group_id: sg-02b4fa0698eaa06cf
  - is_egress: false
  - ip_protocol: tcp
  - from_port: 443
  - to_port: 443
  - tags: []
  - description: Allow worker Kubelets and pods to receive communication from the
      cluster control plane
  - referenced_group_info:
      GroupId: sg-070a797a4b433814b
      UserId: '123456789012'


sgr-0e211bb288c7a0d96:
  aws.ec2.security_group_rule.present:
  - name: sgr-0e211bb288c7a0d96
  - resource_id: sgr-0e211bb288c7a0d96
  - group_id: sg-d68355bf
  - is_egress: true
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


sgr-0e9f9a226e5e1aec6:
  aws.ec2.security_group_rule.present:
  - name: sgr-0e9f9a226e5e1aec6
  - resource_id: sgr-0e9f9a226e5e1aec6
  - group_id: sg-07400e9684a4b7a48
  - is_egress: true
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


sgr-0eb7727a9d7a24f7d:
  aws.ec2.security_group_rule.present:
  - name: sgr-0eb7727a9d7a24f7d
  - resource_id: sgr-0eb7727a9d7a24f7d
  - group_id: sg-0fc412cebfb987038
  - is_egress: false
  - ip_protocol: tcp
  - from_port: 80
  - to_port: 80
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


sgr-0ffb22f7ca7a0f16c:
  aws.ec2.security_group_rule.present:
  - name: sgr-0ffb22f7ca7a0f16c
  - resource_id: sgr-0ffb22f7ca7a0f16c
  - group_id: sg-02b4fa0698eaa06cf
  - is_egress: false
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - tags: []
  - description: Allow node to communicate with each other
  - referenced_group_info:
      GroupId: sg-02b4fa0698eaa06cf
      UserId: '123456789012'


subnet-0094b72dfb7ce6131:
  aws.ec2.subnet.present:
  - name: subnet-0094b72dfb7ce6131
  - resource_id: subnet-0094b72dfb7ce6131
  - vpc_id: vpc-0738f2a523f4735bd
  - cidr_block: 10.170.208.0/20
  - availability_zone: eu-west-3b
  - tags:
    - Key: KubernetesCluster
      Value: idem-test
    - Key: kubernetes.io/cluster/idem-test
      Value: shared
    - Key: kubernetes.io/role/elb
      Value: '1'
    - Key: Automation
      Value: 'true'
    - Key: Owner
      Value: org1
    - Key: COGS
      Value: OPEX
    - Key: Environment
      Value: test-dev
    - Key: Name
      Value: idem-test-temp-xyz-control-public


subnet-039e53122e038d38c:
  aws.ec2.subnet.present:
  - name: subnet-039e53122e038d38c
  - resource_id: subnet-039e53122e038d38c
  - vpc_id: vpc-0738f2a523f4735bd
  - cidr_block: 10.170.128.0/18
  - availability_zone: eu-west-3c
  - tags:
    - Key: Automation
      Value: 'true'
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Environment
      Value: test-dev
    - Key: kubernetes.io/cluster/idem-test
      Value: shared
    - Key: kubernetes.io/role/internal-elb
      Value: '1'
    - Key: COGS
      Value: OPEX
    - Key: Name
      Value: idem-test-temp-xyz-node-private
    - Key: Owner
      Value: org1


subnet-050732fa4616470d9:
  aws.ec2.subnet.present:
  - name: subnet-050732fa4616470d9
  - resource_id: subnet-050732fa4616470d9
  - vpc_id: vpc-0738f2a523f4735bd
  - cidr_block: 10.170.0.0/18
  - availability_zone: eu-west-3a
  - tags:
    - Key: COGS
      Value: OPEX
    - Key: Name
      Value: idem-test-temp-xyz-node-private
    - Key: kubernetes.io/role/internal-elb
      Value: '1'
    - Key: Environment
      Value: test-dev
    - Key: Automation
      Value: 'true'
    - Key: KubernetesCluster
      Value: idem-test
    - Key: kubernetes.io/cluster/idem-test
      Value: shared
    - Key: Owner
      Value: org1


subnet-05a0fbce3b40dcd05:
  aws.ec2.subnet.present:
  - name: subnet-05a0fbce3b40dcd05
  - resource_id: subnet-05a0fbce3b40dcd05
  - vpc_id: vpc-0dbee6437661777d4
  - cidr_block: 10.0.107.0/24
  - availability_zone: eu-west-3b
  - tags:
    - Key: Name
      Value: aws_ec2_subnet_for_xyz


subnet-05dfaa0d01a337199:
  aws.ec2.subnet.present:
  - name: subnet-05dfaa0d01a337199
  - resource_id: subnet-05dfaa0d01a337199
  - vpc_id: vpc-0738f2a523f4735bd
  - cidr_block: 10.170.64.0/18
  - availability_zone: eu-west-3b
  - tags:
    - Key: Automation
      Value: 'true'
    - Key: kubernetes.io/cluster/idem-test
      Value: shared
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Environment
      Value: test-dev
    - Key: COGS
      Value: OPEX
    - Key: kubernetes.io/role/internal-elb
      Value: '1'
    - Key: Owner
      Value: org1
    - Key: Name
      Value: idem-test-temp-xyz-node-private


subnet-085c807f9dcd3bccd:
  aws.ec2.subnet.present:
  - name: subnet-085c807f9dcd3bccd
  - resource_id: subnet-085c807f9dcd3bccd
  - vpc_id: vpc-0dbee6437661777d4
  - cidr_block: 10.0.78.0/24
  - availability_zone: eu-west-3a
  - tags:
    - Key: Name
      Value: aws_ec2_subnet_for_xyz_worker


subnet-09cecc8c853637d3b:
  aws.ec2.subnet.present:
  - name: subnet-09cecc8c853637d3b
  - resource_id: subnet-09cecc8c853637d3b
  - vpc_id: vpc-0738f2a523f4735bd
  - cidr_block: 10.170.192.0/20
  - availability_zone: eu-west-3a
  - tags:
    - Key: kubernetes.io/cluster/idem-test
      Value: shared
    - Key: Name
      Value: idem-test-temp-xyz-control-public
    - Key: Environment
      Value: test-dev
    - Key: kubernetes.io/role/elb
      Value: '1'
    - Key: Automation
      Value: 'true'
    - Key: KubernetesCluster
      Value: idem-test
    - Key: COGS
      Value: OPEX
    - Key: Owner
      Value: org1


subnet-0d68d61b1ab708d42:
  aws.ec2.subnet.present:
  - name: subnet-0d68d61b1ab708d42
  - resource_id: subnet-0d68d61b1ab708d42
  - vpc_id: vpc-0738f2a523f4735bd
  - cidr_block: 10.170.224.0/20
  - availability_zone: eu-west-3c
  - tags:
    - Key: Owner
      Value: org1
    - Key: kubernetes.io/role/elb
      Value: '1'
    - Key: COGS
      Value: OPEX
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Automation
      Value: 'true'
    - Key: Environment
      Value: test-dev
    - Key: Name
      Value: idem-test-temp-xyz-control-public
    - Key: kubernetes.io/cluster/idem-test
      Value: shared


subnet-5f1c0227:
  aws.ec2.subnet.present:
  - name: subnet-5f1c0227
  - resource_id: subnet-5f1c0227
  - vpc_id: vpc-dcae57b5
  - cidr_block: 172.31.16.0/20
  - availability_zone: eu-west-3b
  - tags:
    - Key: AutomaticCleanExpirationTime
      Value: '2020-07-10T09:57:33.882Z'


vpc-0738f2a523f4735bd:
  aws.ec2.vpc.present:
  - name: vpc-0738f2a523f4735bd
  - resource_id: vpc-0738f2a523f4735bd
  - instance_tenancy: default
  - tags:
    - Key: COGS
      Value: OPEX
    - Key: Automation
      Value: 'true'
    - Key: Owner
      Value: org1
    - Key: kubernetes.io/cluster/idem-test
      Value: shared
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Environment
      Value: test-dev
    - Key: Name
      Value: idem-test-temp-xyz-cluster-node
  - cidr_block_association_set:
    - AssociationId: vpc-cidr-assoc-02ab4e7064a606d2b
      CidrBlock: 10.170.0.0/16
      CidrBlockState:
        State: associated
  - enable_dns_hostnames: true
  - enable_dns_support: true


vpc-08f76fe175071e969:
  aws.ec2.vpc.present:
  - name: vpc-08f76fe175071e969
  - resource_id: vpc-08f76fe175071e969
  - instance_tenancy: default
  - tags:
    - Key: Name3
      Value: vijay-vpc-test3
  - cidr_block_association_set:
    - AssociationId: vpc-cidr-assoc-054ac0d8549bbd88b
      CidrBlock: 10.1.150.0/28
      CidrBlockState:
        State: associated
  - enable_dns_hostnames: false
  - enable_dns_support: true


vpc-0dbee6437661777d4:
  aws.ec2.vpc.present:
  - name: vpc-0dbee6437661777d4
  - resource_id: vpc-0dbee6437661777d4
  - instance_tenancy: default
  - tags:
    - Key: Name
      Value: idem-fixture-vpc-61ddff14-631a-4ee9-bab6-daa191a7da66
  - cidr_block_association_set:
    - AssociationId: vpc-cidr-assoc-054669469e1afac05
      CidrBlock: 10.0.0.0/16
      CidrBlockState:
        State: associated
  - enable_dns_hostnames: false
  - enable_dns_support: true


vpc-dcae57b5:
  aws.ec2.vpc.present:
  - name: vpc-dcae57b5
  - resource_id: vpc-dcae57b5
  - instance_tenancy: default
  - tags:
    - Key: Name
      Value: default
  - cidr_block_association_set:
    - AssociationId: vpc-cidr-assoc-5e854037
      CidrBlock: 172.31.0.0/16
      CidrBlockState:
        State: associated
  - enable_dns_hostnames: true
  - enable_dns_support: true
