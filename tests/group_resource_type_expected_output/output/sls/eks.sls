idem-test:
  aws.eks.cluster.present:
  - name: idem-test
  - resource_id: idem-test
  - role_arn: arn:aws:iam::123456789012:role/idem-test-temp-xyz-cluster
  - arn: arn:aws:xyz:eu-west-3:123456789012:cluster/idem-test
  - status: ACTIVE
  - version: '1.20'
  - resources_vpc_config:
      clusterSecurityGroupId: sg-0f9910c81ca733164
      endpointPrivateAccess: false
      endpointPublicAccess: true
      publicAccessCidrs:
      - 0.0.0.0/0
      securityGroupIds:
      - sg-070a797a4b433814b
      subnetIds:
      - subnet-05dfaa0d01a337199
      - subnet-0094b72dfb7ce6131
      - subnet-050732fa4616470d9
      - subnet-0d68d61b1ab708d42
      - subnet-039e53122e038d38c
      - subnet-09cecc8c853637d3b
      vpcId: vpc-0738f2a523f4735bd
  - kubernetes_network_config:
      ipFamily: ipv4
      serviceIpv4Cidr: 172.20.0.0/16
  - logging:
      clusterLogging:
      - enabled: true
        types:
        - api
        - audit
        - authenticator
        - controllerManager
        - scheduler
  - tags:
      Automation: 'true'
      COGS: OPEX
      Environment: test-dev
      KubernetesCluster: idem-test
      Owner: org1
