aws_iam_user.extension-jenkins:
  aws.iam.user.present:
  - name: extension-jenkins-{{ params["clusterName"] }}
  - resource_id: {{ params.get("aws_iam_user.extension-jenkins")}}
  - path: /xyz/{{ params["clusterName"] }}/
  - tags: {{ params["local_tags"] }}
  - user_name: extension-jenkins-idem-test

aws_iam_user_policy.extension-jenkins:
  aws.iam.user_policy.present:
  - resource_id: {{ params.get("aws_iam_user_policy.extension-jenkins")}}
  - user_name: ${aws.iam.user:aws_iam_user.extension-jenkins:resource_id}
  - name: extension-jenkins-{{ params["clusterName"] }}
  - policy_document:
      {
          "Statement": [
              {
                  "Action": [
                      "xyz:DescribeCluster"
                  ],
                  "Effect": "Allow",
                  "Resource": "${aws.eks.cluster:aws_eks_cluster.cluster:arn}"
              },
              {
                  "Action": [
                      "sts:AssumeRole"
                  ],
                  "Effect": "Allow",
                  "Resource": [
                      "${aws.iam.role:aws_iam_role.xyz-jenkins:arn}"
                  ],
                  "Sid": ""
              },
              {
                  "Action": "s3:*",
                  "Effect": "Allow",
                  "Resource": [
                      "arn:aws:s3:::ssm-ansible-test-dev",
                      "arn:aws:s3:::ssm-ansible-test-dev/*"
                  ],
                  "Sid": ""
              },
              {
                  "Action": [
                      "ssm:StartSession"
                  ],
                  "Condition": {
                      "StringLike": {
                          "ssm:resourceTag/KubernetesCluster": [
                              "${aws.eks.cluster:aws_eks_cluster.cluster:resource_id}"
                          ]
                      }
                  },
                  "Effect": "Allow",
                  "Resource": "*"
              },
              {
                  "Action": [
                      "ssm:TerminateSession"
                  ],
                  "Effect": "Allow",
                  "Resource": "*"
              }
          ],
          "Version": "2012-10-17"
      }

{% if params["enable-jenkins-rolling-upgrade-policy"]  %}
aws_iam_user_policy.extension-jenkins-rolling-upgrade-0:
  aws.iam.user_policy.present:
  - resource_id: {{ params.get("aws_iam_user_policy.extension-jenkins-rolling-upgrade-0")}}
  - user_name: ${aws.iam.user:aws_iam_user.extension-jenkins:resource_id}
  - name: extension-jenkins-rolling-upgrade-{{ params["clusterName"] }}
  - policy_document:
      {
          "Statement": [
              {
                  "Action": "ec2:Describe*",
                  "Effect": "Allow",
                  "Resource": "*"
              },
              {
                  "Action": [
                      "sts:AssumeRole"
                  ],
                  "Effect": "Allow",
                  "Resource": [
                      "${aws.iam.role:aws_iam_role.xyz-jenkins:arn}"
                  ],
                  "Sid": ""
              },
              {
                  "Action": [
                      "autoscaling:DeleteTags",
                      "autoscaling:ResumeProcesses",
                      "autoscaling:CreateOrUpdateTags",
                      "autoscaling:UpdateAutoScalingGroup",
                      "autoscaling:SuspendProcesses",
                      "autoscaling:TerminateInstanceInAutoScalingGroup"
                  ],
                  "Condition": {
                      "StringEquals": {
                          "autoscaling:ResourceTag/KubernetesCluster": "${aws.eks.cluster:aws_eks_cluster.cluster:resource_id}"
                      }
                  },
                  "Effect": "Allow",
                  "Resource": "*",
                  "Sid": ""
              },
              {
                  "Action": [
                      "xyz:UpdateClusterVersion",
                      "ec2:DescribeInstances",
                      "ec2:RebootInstances",
                      "autoscaling:DescribeAutoScalingGroups",
                      "xyz:DescribeUpdate",
                      "xyz:DescribeCluster",
                      "xyz:ListClusters",
                      "xyz:CreateCluster"
                  ],
                  "Effect": "Allow",
                  "Resource": "*",
                  "Sid": ""
              },
              {
                  "Action": "ec2:DescribeInstances",
                  "Effect": "Allow",
                  "Resource": "*",
                  "Sid": ""
              }
          ],
          "Version": "2012-10-17"
      }
{% endif %}

{% if params["enable-jenkins-rolling-upgrade-policy"]  %}
aws_iam_role.xyz-jenkins:
  aws.iam.role.present:
  - resource_id: {{ params.get("aws_iam_role.xyz-jenkins")}}
  - name: xyz-{{ params["clusterName"] }}-jenkins
  - max_session_duration: 3600
  - tags: {{ params["local_tags"] }}
  - assume_role_policy_document:
      {
          "Statement": [
              {
                  "Action": "sts:AssumeRole",
                  "Condition": {
                      "ForAnyValue:StringEquals": {
                          "aws:username": "{{ params[\"admin_users\"] }}"
                      }
                  },
                  "Effect": "Allow",
                  "Principal": {
                      "AWS": "arn:aws:iam::123456789012:root"
                  }
              },
              {
                  "Action": "sts:AssumeRole",
                  "Effect": "Allow",
                  "Principal": {
                      "AWS": "${aws.iam.user:aws_iam_user.extension-jenkins:arn}"
                  }
              }
          ],
          "Version": "2012-10-17"
      }
{% endif %}

{% if params["enable-jenkins-rolling-upgrade-policy"]  %}
aws_iam_policy.xyz-jenkins:
  aws.iam.policy.present:
  - policy_document:
      {
          "Statement": [
              {
                  "Action": "xyz:DescribeCluster",
                  "Effect": "Allow",
                  "Resource": "${aws.eks.cluster:aws_eks_cluster.cluster:arn}"
              }
          ],
          "Version": "2012-10-17"
      }
  - tags: []
  - name: xyz-{{ params["clusterName"] }}-jenkins
  - resource_id: {{ params.get("aws_iam_policy.xyz-jenkins")}}
  - path: /
{% endif %}

{% if params["enable-jenkins-rolling-upgrade-policy"]  %}
aws_iam_role_policy_attachment.xyz-jenkins:
  aws.iam.role_policy_attachment.present:
  - role_name: ${aws.iam.role:aws_iam_role.xyz-jenkins:resource_id}
  - policy_arn: ${aws.iam.policy:aws_iam_policy.xyz-jenkins:resource_id}
{% endif %}
