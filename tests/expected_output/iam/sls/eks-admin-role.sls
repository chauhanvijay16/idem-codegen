aws_iam_role.xyz-admin:
  aws.iam.role.present:
  - resource_id: {{ params.get("aws_iam_role.xyz-admin")}}
  - name: xyz-{{ params["clusterName"] }}-admin
  - max_session_duration: 3600
  - tags: {{ params["local_tags"] }}
  - assume_role_policy_document:
      {
          "Statement": [
              {
                  "Action": "sts:AssumeRole",
                  "Condition": {
                      "ForAnyValue:StringEquals": {
                          "aws:username": "{{ params[\"admin_users\"] }}"
                      }
                  },
                  "Effect": "Allow",
                  "Principal": {
                      "AWS": "arn:aws:iam::123456789012:root"
                  }
              },
              {
                  "Action": "sts:AssumeRole",
                  "Effect": "Allow",
                  "Principal": {
                      "Service": "xyz.amazonaws.com"
                  }
              }
          ],
          "Version": "2012-10-17"
      }

aws_iam_policy.xyz-admin:
  aws.iam.policy.present:
  - policy_document:
      {
          "Statement": [
              {
                  "Action": [
                      "xyz:*"
                  ],
                  "Effect": "Allow",
                  "Resource": "*"
              },
              {
                  "Action": [
                      "iam:PassRole"
                  ],
                  "Effect": "Allow",
                  "Resource": [
                      "${aws.iam.role:aws_iam_role.cluster:arn}"
                  ]
              },
              {
                  "Action": [
                      "kms:Create*",
                      "kms:Describe*",
                      "kms:Enable*",
                      "kms:List*",
                      "kms:Put*",
                      "kms:Update*",
                      "kms:Revoke*",
                      "kms:Disable*",
                      "kms:Get*",
                      "kms:Delete*",
                      "kms:TagResource",
                      "kms:UntagResource",
                      "kms:ScheduleKeyDeletion",
                      "kms:CancelKeyDeletion"
                  ],
                  "Effect": "Allow",
                  "Resource": "arn:aws:kms:eu-west-3:123456789012:key/8ac5f341-fd1c-4e9d-9596-8f844dba5cc8"
              }
          ],
          "Version": "2012-10-17"
      }
  - tags: []
  - name: xyz-{{ params["clusterName"] }}-admin
  - resource_id: {{ params.get("aws_iam_policy.xyz-admin")}}
  - path: /

aws_iam_role_policy_attachment.xyz-admin:
  aws.iam.role_policy_attachment.present:
  - role_name: ${aws.iam.role:aws_iam_role.xyz-admin:resource_id}
  - policy_arn: ${aws.iam.policy:aws_iam_policy.xyz-admin:resource_id}
