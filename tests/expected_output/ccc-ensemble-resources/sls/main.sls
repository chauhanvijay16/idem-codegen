aws_s3_bucket.ccc:
  aws.s3.bucket.present:
  - name: ccc-{{ params["clusterName"] }}
  - resource_id: {{ params.get("aws_s3_bucket.ccc")}}
  - create_bucket_configuration:
      LocationConstraint: us-west-2
  - grant_full_control: FULL_CONTROL
  - acl: {{ params["acl"] }}
  - tags: {% set x = params["local_tags"].copy() %}
          {% do x.update({"Service": "ccc", "Precious": "true"}) %}
          {{x}}

aws_s3_bucket_lifecycle.ccc-0:
  aws.s3.bucket_lifecycle.present:
  - name: ccc-staging-us-2-lifecycle
  - resource_id: {{ params.get("aws_s3_bucket_lifecycle.ccc-0")}}
  - bucket: ccc-staging-us-2
  - lifecycle_configuration:
      Rules:
      - Expiration:
          Days: 30
        Filter:
          And:
            Prefix: Test
            Tags:
            - Key: Tmp
              Value: true
        ID: Delete
        Status: Enabled

aws_s3_bucket_encryption.ccc-1:
  aws.s3.bucket_encryption.present:
  - name: ccc-staging-us-2-encryption
  - resource_id: {{ params.get("aws_s3_bucket_encryption.ccc-1")}}
  - bucket: ccc-staging-us-2
  - server_side_encryption_configuration:
      Rules:
      - ApplyServerSideEncryptionByDefault:
          SSEAlgorithm: AES256
        BucketKeyEnabled: false
