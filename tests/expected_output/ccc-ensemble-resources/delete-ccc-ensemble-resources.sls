aws_s3_bucket.ccc:
  aws.s3.bucket.absent:
  - name: ccc-staging-us-2
aws_s3_bucket_encryption.ccc-1:
  aws.s3.bucket_encryption.absent:
  - name: ccc-staging-us-2-encryption
aws_s3_bucket_lifecycle.ccc-0:
  aws.s3.bucket_lifecycle.absent:
  - name: ccc-staging-us-2-lifecycle
