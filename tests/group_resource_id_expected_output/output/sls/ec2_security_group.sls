sg-01a41f68:
  aws.ec2.security_group.present:
  - resource_id: sg-01a41f68
  - name: rds-launch-wizard-2
  - vpc_id: vpc-dcae57b5
  - tags:
    - Key: AutomaticCleanExpirationTime
      Value: '2020-07-10T09:57:33.882Z'
  - description: 'Created from the RDS Management Console: 2018/01/28 19:09:27'


sg-02b4fa0698eaa06cf:
  aws.ec2.security_group.present:
  - resource_id: sg-02b4fa0698eaa06cf
  - name: idem-test-temp-xyz-cluster-node
  - vpc_id: vpc-0738f2a523f4735bd
  - tags:
    - Key: cluster
      Value: xyz
    - Key: Environment
      Value: test-dev
    - Key: Name
      Value: idem-test-temp-xyz-cluster-node
    - Key: KubernetesCluster
      Value: idem-test
    - Key: role
      Value: xyz-worker
    - Key: kubernetes.io/cluster/idem-test
      Value: owned
    - Key: Owner
      Value: org1
    - Key: Automation
      Value: 'true'
    - Key: COGS
      Value: OPEX
  - description: Security group for all nodes in the cluster


sg-04fa08f7b284cd1e9:
  aws.ec2.security_group.present:
  - resource_id: sg-04fa08f7b284cd1e9
  - name: default
  - vpc_id: vpc-0738f2a523f4735bd
  - description: default VPC security group


sg-06552329287f9b206:
  aws.ec2.security_group.present:
  - resource_id: sg-06552329287f9b206
  - name: temp-20220505111435890800000003
  - vpc_id: vpc-0738f2a523f4735bd
  - tags:
    - Key: Name
      Value: idem-test-nessus_vuln_scanner
  - description: nessus scanner


sg-070a797a4b433814b:
  aws.ec2.security_group.present:
  - resource_id: sg-070a797a4b433814b
  - name: idem-test-temp-xyz-cluster
  - vpc_id: vpc-0738f2a523f4735bd
  - tags:
    - Key: Environment
      Value: test-dev
    - Key: Automation
      Value: 'true'
    - Key: KubernetesCluster
      Value: idem-test
    - Key: COGS
      Value: OPEX
    - Key: Name
      Value: idem-test-temp-xyz
    - Key: Owner
      Value: org1
  - description: Cluster communication with worker nodes


sg-07400e9684a4b7a48:
  aws.ec2.security_group.present:
  - resource_id: sg-07400e9684a4b7a48
  - name: idem-fixture-security-group-eb54958c-7199-4034-8f0f-83a62bcf1dc5
  - vpc_id: vpc-0dbee6437661777d4
  - tags:
    - Key: Name
      Value: idem-fixture-security-group-eb54958c-7199-4034-8f0f-83a62bcf1dc5
  - description: Created for Idem integration test.


sg-0d129413d7cea1757:
  aws.ec2.security_group.present:
  - resource_id: sg-0d129413d7cea1757
  - name: default
  - vpc_id: vpc-0dbee6437661777d4
  - description: default VPC security group


sg-0edb77cb85ac5f73e:
  aws.ec2.security_group.present:
  - resource_id: sg-0edb77cb85ac5f73e
  - name: default
  - vpc_id: vpc-08f76fe175071e969
  - description: default VPC security group


sg-0f9910c81ca733164:
  aws.ec2.security_group.present:
  - resource_id: sg-0f9910c81ca733164
  - name: xyz-cluster-sg-idem-test-7532260
  - vpc_id: vpc-0738f2a523f4735bd
  - tags:
    - Key: kubernetes.io/cluster/idem-test
      Value: owned
    - Key: aws:xyz:cluster-name
      Value: idem-test
    - Key: Name
      Value: xyz-cluster-sg-idem-test-7532260
  - description: xyz created security group applied to ENI that is attached to xyz
      Control Plane master nodes, as well as any managed workloads.


sg-0fc412cebfb987038:
  aws.ec2.security_group.present:
  - resource_id: sg-0fc412cebfb987038
  - name: photon-model-sg
  - vpc_id: vpc-dcae57b5
  - description: abc Photon model security group


sg-d68355bf:
  aws.ec2.security_group.present:
  - resource_id: sg-d68355bf
  - name: default
  - vpc_id: vpc-dcae57b5
  - description: default VPC security group
