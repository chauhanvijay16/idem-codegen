db-subnet-group-idem-test:
  aws.rds.db_subnet_group.present:
  - db_subnet_group_arn: arn:aws:rds:eu-west-3:123456789012:subgrp:db-subnet-group-idem-test
    db_subnet_group_description: For Aurora rds
    name: db-subnet-group-idem-test
    resource_id: db-subnet-group-idem-test
    subnets:
    - subnet-039e53122e038d38c
    - subnet-05dfaa0d01a337199
    - subnet-050732fa4616470d9
    tags:
    - Key: Automation
      Value: 'true'
    - Key: Owner
      Value: org1
    - Key: COGS
      Value: OPEX
    - Key: Environment
      Value: test-dev
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Name
      Value: idem-test-db-subnet-group


default:
  aws.rds.db_subnet_group.present:
  - db_subnet_group_arn: arn:aws:rds:eu-west-3:123456789012:subgrp:default
    db_subnet_group_description: default
    name: default
    resource_id: default
    subnets:
    - subnet-01a4eaab9dfdc026a
    - subnet-5f1c0227
